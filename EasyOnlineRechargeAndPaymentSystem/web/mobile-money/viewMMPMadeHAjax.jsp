<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMmPaymentMadeInfoList !=null">
    <s:if test="singleMmPaymentMadeInfoList.size() !=0">
        <s:iterator value="singleMmPaymentMadeInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Mobile Money Payment Made ID # <s:property value="mmTransactionHistoryId"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN ALERTS PORTLET-->
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    Payment Made Details
                                </div>
                            </div>
                            <div class="portlet-body">
<!--                                <div class="alert alert-success">
                                    <strong>Success!</strong>
                                    Transaction ID is - 
                                </div>
                                <form method="post" class="form-inline" role="form" action="#">
                                    <div class="form-group">
                                        <label class="sr-only" for="trid">TrID</label>
                                        <input class="form-control" id="trid" name="trid" value="" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-success">Manual&nbsp;Confirm</button>
                                </form>-->
                                <h4>Details</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 30%">Payment Made ID</th>
                                                <th><s:property value="mmTransactionHistoryId"/></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>From</td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                            </tr>
                                            <tr>
                                                <td>Current Balance</td>
                                                <td><s:property value="0"/></td>
                                            </tr>
                                            <tr>
                                                <td>To</td>
                                                <td><s:property value="userInfo.userId"/></td>
                                            </tr>
                                            <tr>
                                                <td>Balance Type</td>
                                                <td>Mobile Money</td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><s:property value="addBalance"/></td>
                                            </tr>
<!--                                            <tr>
                                                <td>Actual Amount</td>
                                                <td>Default</td>
                                            </tr>-->
                                            <!-- Added New Start -->
<!--                                            <tr>
                                                <td>Reseller 4 Old Balance</td>
                                                <td>Default</td>
                                            </tr>
                                            <tr>
                                                <td>Reseller 4 New Balance</td>
                                                <td>Default</td>
                                            </tr>-->
                                            <!--        Added New   -->
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <s:if test="type=='Y'">
                                                        Refund
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #009966;">
                                                            Waiting
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
<!--                                            <tr>
                                                <td>Modem</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>

                                                </td>
                                            </tr>-->
                                            <tr>
                                                <td>Request Time</td>
                                                <td><s:property value="balanceGivenDate"/></td>
                                            </tr>
<!--                                            <tr>
                                                <td>Send Time</td>
                                                <td>Default</td>
                                            </tr>
                                            <tr>
                                                <td>Success Time</td>
                                                <td>Default</td>
                                            </tr>
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>IP</td>
                                                <td>Default</td>
                                            </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Orders For this number</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Date</th>
                                                <!--<th>Status</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><s:property value="mmTransactionHistoryId"/></td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                                <td><s:property value="userInfo.userId"/></td>
                                                <td><s:property value="balanceGivenDate"/></td>
<!--                                                <td>
                                                    <span class="label label-sm label-success">

                                                    </span>
                                                </td>-->
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
<!--                                <h4>Related SMS</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>SMS</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                        </div>
                        <!-- END ALERTS PORTLET-->
                    </div>
                </div>
            </div>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>