<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile Money <small>Send Mobile Money Confirm</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-6 col-sm-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption text-info">
                                        You are sending <span style="font-size:20px; color:#C00"> Mobile Money </span> Taka <span style="font-size:20px; color:#C00"> <s:property value="amount"/> </span>  to <span style="font-size:20px; color:#C00"> <s:property value="phone"/> </span>
                                        <br/><br/>
                                        Your Fee <span style="font-size:20px; color:#C00"> <s:property value="fee"/> </span>
                                        Total Amount will be deducted <span style="font-size:20px; color:#C00"> <s:property value="totalAmount"/> </span>
                                    </div>

                                </div>
                                <form role="form">
                                    <div class="form-group has-success">
                                        <label><strong>Enter PIN to Confirm</strong></label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="icon-key"></i>
                                            </span>
                                            <input type="password" id="pin" name="pin" class="form-control input-lg" autocomplete="off"  style="font-size:20px; color:#C00">
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="hidden" id="phone" name="phone" value="<s:property value="phone"/>">
                                        <input type="hidden" id="amount" amount="amount" value="<s:property value="amount"/>">
                                        <input type="hidden" id="type" name="type" value="<s:property value="type"/>">
                                        <input type="hidden" id="mmOperator" name="mmOperator" value="<s:property value="mmOperator"/>">
                                        <input type="hidden" id="operator" name="operator" value="<s:property value="operator"/>">
                                        <input type="hidden" id="totalAmount" name="totalAmount" value="<s:property value="totalAmount"/>">
                                        <input type="hidden" id="fee" name="fee" value="<s:property value="fee"/>">
                                        <button type="button" onclick="sendMobileMoney();" class="btn blue">Confirm</button>
                                        <a href="SendMobileMoney" class="btn red">Cancel</a>
                                        <img id="createLoadingImage" src="" alt="" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/sendMMValidation.js" type="text/javascript"></script>

        <script type="text/javascript">

                                            jQuery(document).ready(function () {
                                                // initiate layout and plugins
                                                Metronic.init(); // init metronic core components
                                                Layout.init(); // init current layout
                                                QuickSidebar.init() // init quick sidebar
                                                //        UIIdleTimeout.init();
                                            });

        </script>

        <script type="text/javascript">

            function sendMobileMoney() {

                var pin = document.getElementById("pin").value;

                var phone = document.getElementById("phone").value;

                var amount = document.getElementById("amount").value;

                var type = document.getElementById('type').value;

                var mmOperator = document.getElementById("mmOperator").value;

                var operator = document.getElementById("operator").value;

                var totalAmount = document.getElementById("totalAmount").value;

                var fee = document.getElementById("fee").value;

                var message = confirm("You are sending Mobile Money Tk." + amount + " to mobile number " + phone + " your fee " + fee + " and total amount will be deducted " + totalAmount + ". Please Make sure- Phone Number, Amount and Type is Correct. Click OK to send or Cancel to Cancel");

                if (message == true) {

                    document.getElementById('createLoadingImage').src = 'images/loading.gif';

                    var dataString = 'phone=' + phone;
                    dataString += '&amount=' + amount;
                    dataString += '&type=' + type;
                    dataString += '&pin=' + pin;
                    dataString += '&mmOperator=' + mmOperator;
                    dataString += '&operator=' + operator;
                    dataString += '&totalAmount=' + totalAmount;

                    document.getElementById('createLoadingImage').src = 'images/loading.gif';

                    window.location = 'SubmitMobileMoney?' + dataString;

                    document.getElementById('createLoadingImage').src = '';
                }
            }
        </script>

    </body>
</html>