<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>

        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/datepicker.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            .modal-scrollable {
                overflow: auto !important;
            }
        </style>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Balance&nbsp;Request&nbsp;<small class="fv">Mobile Recharge, Mobile Money</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <span class="caption-subject bold uppercase">Balance&nbsp;Request</span>
                                        <span class="caption-helper">History...</span>
                                    </div>
                                    <div class="actions">
                                        <div class="tools">
                                            <a href="Payments" class="btn btn-circle purple-medium btn-sm">
                                                <i class="icon-plus"></i>&nbsp;Send&nbsp;Balance
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                            <select class="table-group-action-input form-control input-inline input-small input-sm">
                                                <option value="">Select...</option>
                                                <option value="refund">Refund</option>
                                                <option value="resend">Resend</option>
                                                <option value="confirm">Manual Confirm</option>
                                            </select>
                                            <button class="btn btn-sm yellow table-group-action-submit"><i class="icon-check"></i> Submit</button>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                            <thead>
                                                <tr role="row" class="heading">
                                                    <th width="2%"><input type="checkbox" class="group-checkable"></th>
                                                    <th width="5%">Request ID</th>
                                                    <th width="12%">Request Time</th>
                                                    <th width="8%">Request Sender</th>
                                                    <th width="8%">Request Receiver</th>
                                                    <th width="8%">Balance Type</th>
                                                    <th width="6%">Amount</th>
                                                    <th width="7%">Request Status</th>
                                                    <th width="10%">TrID</th>
                                                    <th width="10%">Success Time</th>
                                                    <th width="10%">Response By</th>
                                                    <th width="6%">Actions</th>
                                                </tr>
                                                <tr role="row" class="filter">
                                                    <td></td>
                                                    <td><input type="text" id="order_id" name="order_id" class="form-control form-filter input-sm"></td>
                                                    <td>
                                                        <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
                                                            <input type="text" id="sdate" name="sdate" style="width: 90px;" value="" class="form-control form-filter input-sm" readonly placeholder="From">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="icon-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
                                                            <input type="text" id="tdate" name="tdate" style="width: 90px;" value="" class="form-control form-filter input-sm" readonly placeholder="To">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-sm default" type="button"><i class="icon-calendar"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td><input type="text" id="sender" name="sender" class="form-control form-filter input-sm"></td>
                                                    <td><input type="text" id="receiver" name="receiver" class="form-control form-filter input-sm"></td>
                                                    <td>
                                                        <select id="type" name="type" style="width: 145px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="MR">Mobile Recharge</option>
                                                            <option value="MM">Mobile Money</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <input type="text" id="famount" name="famount" class="form-control form-filter input-sm margin-bottom-5 clearfix" placeholder="From"/>
                                                        </div>
                                                        <input type="text" id="tamount" name="tamount" class="form-control form-filter input-sm" placeholder="To"/>
                                                    </td>
                                                    <td>
                                                        <select id="status" name="status" style="width: 107px;" class="form-control form-filter input-sm">
                                                            <option value="">Select...</option>
                                                            <option value="Y">Success</option>
                                                            <option value="N">Pending</option>
                                                            <option value="P">Processing</option>
                                                            <option value="W">Waiting</option>
                                                            <option value="F">Failed</option>
                                                        </select>
                                                    </td>
                                                    <td><input type="text" id="trid" name="trid" style="width: 70px;" class="form-control form-filter input-sm"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <div class="margin-bottom-5">
                                                            <button class="btn btn-sm yellow filter-submit1 margin-bottom"><i class="icon-check"></i> Search</button>
                                                        </div>
                                                        <button class="btn btn-sm red filter-cancel"><i class="icon-close"></i> Reset</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody_balRReceiveH">
                                                <s:if test="balanceRequestReceiveInfoList !=null">
                                                    <s:if test="balanceRequestReceiveInfoList.size() !=0">
                                                        <s:iterator value="balanceRequestReceiveInfoList">
                                                            <tr>
                                                                <td><input type="checkbox" id="chkb" name="chkb" value="<s:property value="balanceRequestId"/>" class="form-filter"></td>
                                                                <td align="center" class="sorting_1">
                                                                    <s:property value="balanceRequestId"/>
                                                                    <br/>
                                                                    <a href="javascript:void(0);" id="<s:property value="balanceRequestId"/>" class="btn fview btn-circle btn-xs blue">
                                                                        <i class="icon-list"></i>
                                                                        View
                                                                    </a>
                                                                </td>
                                                                <td align="center"><s:property value="requestTime"/></td>
                                                                <td><s:property value="requestSender"/></td>
                                                                <td><s:property value="requestReceiver"/></td>
                                                                <td>
                                                                    <s:if test='balanceType=="MR"'>
                                                                        Mobile Recharge
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Mobile Money
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td class="amount"><s:property value="amount"/></td>
                                                                <td align="center">
                                                                    <s:if test="requestStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="requestStatus=='W'">
                                                                        <span style="color: #1590f2;">
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="requestStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="requestStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td><s:property value="trid"/></td>
                                                                <td><s:property value="requestSuccessTime"/></td>
                                                                <td><s:property value="requestResponseBy"/></td>
                                                                <td>
                                                                    <s:if test='balanceType=="MR"'>
                                                                        <% if ((PM03CM01 && PERMISSION_MR)) {%>
                                                                        <s:if test="requestStatus!='Y'">
                                                                            <a href="RequestForPayment?resellerId=<s:property value="requestSender"/>&balReqID=<s:property value="trid"/>&amount=<s:property value="amount"/>" class="send_balance btn btn-circle blue btn-sm">
                                                                                <i class="icon-plus"></i>
                                                                                Send Balance
                                                                            </a>
                                                                        </s:if>
                                                                        <% }%>
                                                                    </s:if>

                                                                    <s:if test='balanceType=="MM"'>
                                                                        <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                        <s:if test="requestStatus!='Y'">
                                                                            <a href="RequestForPayment?resellerId=<s:property value="requestSender"/>&loadType=<s:property value="balanceType"/>&balReqID=<s:property value="trid"/>&amount=<s:property value="amount"/>" id="<s:property value="balanceRequestId"/>" class="send_balance btn btn-circle purple-medium btn-sm">
                                                                                <i class="icon-plus"></i>
                                                                                Send Balance
                                                                            </a>
                                                                        </s:if>
                                                                        <% }%>
                                                                    </s:if>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="6" style="text-align:right">Total:</th>
                                                    <th colspan="6" style="text-align: left"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery-migrate-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery-ui-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_005.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_003.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_004.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-switch.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/select2.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-datepicker.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/datatable.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/balRequestRHModalView.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(6)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(6, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(6).footer()).html(
                                'Page Total ' + pageTotal + ' [ All Total ' + total + ' ]'
                                );
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                //initiate layout and plugins
                Metronic.init();
                Layout.init();
                QuickSidebar.init();
                EcommerceOrders.init();
            });
        </script>

    </body>
</html>