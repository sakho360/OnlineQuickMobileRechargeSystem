<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/font-awesome.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Support <small>Contact with Support</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="portlet light">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- Google Map -->
                                    <div class="row margin-bottom-20">
                                        <div class="col-xs-6">

                                            <div class="space20"></div>

                                            <h3 class="form-section">Contacts</h3>

                                            <p>
                                                Contact with your Parent User for Support
                                            </p>

                                            <div class="well">

                                                <h4>Parent User Details</h4>

                                                <%--<s:if test="profileInfoList !=null">--%>
                                                <%--<s:if test="profileInfoList.size() !=0">--%>
                                                <%--<s:iterator value="profileInfoList">--%>

                                                <address>
                                                    <strong>Administrator</strong><br>
                                                    Parent User Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<s:property value="emailAddress"/><br>
                                                    Parent User ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;<s:property value="parentId"/><br>
                                                    <abbr title="Phone">Parent User Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;</abbr>rezaul@brandbangla.com
                                                </address>

                                                <address>
                                                    <strong>Parent User Email</strong><br>
                                                    <a target="_blank" href="http://brandbanglait.com/">rezaul@brandbangla.com</a>
                                                </address>

                                                <%--</s:iterator>--%>
                                                <%--</s:if>--%>
                                                <%--</s:if>--%>

                                                <h4>Contact with your Parent User</h4>

                                                <ul class="social-icons margin-bottom-10">
                                                    <li>
                                                        <a href="https://www.facebook.com/Brand-Bangla-IT-572802589586723/?fref=ts" target="_blank" data-original-title="facebook" class="facebook"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="github" class="github"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="Goole Plus" class="googleplus"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="linkedin" class="linkedin"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="rss" class="rss"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="skype" class="skype"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="twitter" class="twitter"></a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-original-title="youtube" class="youtube"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col-xs-6">

                                            <div class="space20"></div>
                                            <!-- BEGIN FORM-->
                                            <form action="#" method="post">

                                                <h3 class="form-section">Support Request</h3>

                                                <p>
                                                    Please include Order ID for any Transaction Related Problem.
                                                </p>

                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-check"></i>
                                                        <input type="text" id="subject" name="subject" placeholder="Subject" value="" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-user"></i>
                                                        <input type="text" id="name" name="name" placeholder="Name" value="" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="input-icon">
                                                        <i class="fa fa-envelope"></i>
                                                        <input type="text" id="email" name="email" placeholder="Email"  value="" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <textarea id="message" name="message" rows="3=6" placeholder="Your Message" class="form-control"></textarea>
                                                </div>

                                                <button type="submit" class="btn green">
                                                    <i class="fa fa-check"></i>
                                                    Send
                                                </button>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>