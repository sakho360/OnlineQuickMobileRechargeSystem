<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function updateResellerProfile(resellerId) {

                var resellerId = resellerId;

                var fullName = document.getElementById("fullName").value;

                var email = document.getElementById("email").value;

                var phone = document.getElementById("phone").value;

                var otpStatus = document.getElementById("otpY");

                var pinEnabledStatus = document.getElementById('pinEnabledY');

                var loadPin = document.getElementById("loadPin").value;

                var pinConfirmStatus = '';

                var curPassword = document.getElementById("curPass").value;

                var apiStatus = document.getElementById("apiY");
                //
                if ((!notEmpty(fullName, "fullName can't be empty !")) & (!forFullName(fullName, "Invalid fullName Format !")) & (!lengthRestriction(fullName, 3, 20, "your fullName"))) {
                    return false;
                }
                //
                if (email == "") {
                    email = 'N';
                    flag = true;
                } else if (!emailValidator(email, "Invalid Email Format !")) {
                    return false;
                }
                //
                if (phone == "") {
                    phone = 'N';
                    flag = true;
                } else if ((!isNumeric(phone, "Invalid Phone Number Format !")) & (!lengthRestriction(phone, 11, 11, "your phone"))) {
                    return false;
                }
                //
                if (otpStatus.checked) {
                    otpStatus = document.getElementById("otpY").value;
                    flag = true;
                } else {
                    otpStatus = document.getElementById("otpN").value;
                    flag = true;
                }
                //
                if (pinEnabledStatus.checked) {
                    if ((!notEmpty(loadPin, "Pin can't be empty !")) & (!isAlphanumeric(loadPin, "Invalid Pin Format !")) & (!lengthRestriction(loadPin, 6, 10, "your pin"))) {
                        return false;
                    }
                    pinConfirmStatus = loadPin;
                    pinEnabledStatus = document.getElementById('pinEnabledY').value;
                } else {
                    pinConfirmStatus = '';
                    pinEnabledStatus = document.getElementById('pinEnabledN').value;
                    flag = true;
                }
                //
                if (apiStatus.checked) {
                    apiStatus = document.getElementById("apiY").value;
                    flag = true;
                } else {
                    apiStatus = document.getElementById("apiN").value;
                    flag = true;
                }
                //
                var message = confirm("Do you want to update your profile ?");

                if (message == true) {

                    var flag = true;

                    if (flag) {

                        var dataString = 'resellerId=' + resellerId;
                        dataString += '&fullName=' + fullName;
                        dataString += '&email=' + email;
                        dataString += '&phone=' + phone;
                        dataString += '&otpStatus=' + otpStatus;
                        dataString += '&pinEnabledStatus=' + pinEnabledStatus;
                        dataString += '&apiStatus=' + apiStatus;
                        dataString += '&curPassword=' + curPassword;
                        dataString += '&pinConfirmStatus=' + pinConfirmStatus;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.ajax({
                            type: "POST",
                            url: 'UpdateResellerProfile',
                            data: dataString,
                            success: function (data) {
                                document.getElementById('createLoadingImage').src = '';
                                $("#message").html(data);
                            }
                        });
                    }
                }
            }
            //            
            function forFullName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_ ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function isAlphanumeric(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function emailValidator(id, helperMsg) {

                var emailExp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (id.match(emailExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function isNumeric(id, helperMsg) {
                var numericExpression = /^[0-9]+$/;
                if (id.match(numericExpression)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Profile <small>edit profile info</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row profile">
                        <div class=" col-md-12  ">
                            <!--BEGIN TABS-->
                            <div class="tabbable tabbable-custom tabbable-full-width">

                                <s:property value="messageString"/>

                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab"> Edit Profile </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">

                                        <form class="form-horizontal">
                                            <div class="form-body">

                                                <s:if test="profileInfoList !=null">
                                                    <s:iterator value="profileInfoList">

                                                        <div class="form-group">
                                                            <label class=" col-md-3   control-label">Type</label>
                                                            <div class=" col-md-4  ">
                                                                <p class="form-control-static">
                                                                    Reseller&nbsp;<s:property value="userGroupInfo.groupName"/>
                                                                </p>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-md-3   control-label">User ID*</label>
                                                            <div class=" col-md-4  ">
                                                                <input type="text" id="userId" name="userId" value="<s:property value="userId"/>" disabled class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-md-3   control-label">User Fullname*</label>
                                                            <div class=" col-md-4  ">

                                                                <input type="text" id="fullName" name="fullName" value="<s:property value="userName"/>" class="form-control">

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-md-3   control-label">Email Address</label>
                                                            <div class=" col-md-4  ">

                                                                <input type="text" id="email" name="email" value="<s:property value="emailAddress"/>" class="form-control">

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-md-3   control-label">Phone Number</label>
                                                            <div class=" col-md-4  ">

                                                                <input type="text" id="phone" name="phone" value="<s:property value="contactNumber1"/>" class="form-control">

                                                                <span class="help-block"> Enter phone number with country code </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Enable OTP ?</label>
                                                            <div class="col-md-4">
                                                                <div class="radio-list">

                                                                    <s:if test="otpStatus=='Y'">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="otpY" name="otp" value="Y" checked>&nbsp;Yes
                                                                        </label>

                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="otpN" name="otp" value="N">&nbsp;No
                                                                        </label>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="otpY" name="otp" value="Y">&nbsp;Yes
                                                                        </label>

                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="otpN" name="otp" value="N" checked>&nbsp;No
                                                                        </label>
                                                                    </s:else>

                                                                </div>
                                                                <span class="help-block"> Enable OTP for better security, You must use a valid email address for OTP </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Enable PIN ?</label>
                                                            <div class="col-md-4">
                                                                <div class="radio-list">
                                                                    <s:if test='enablePinStatus=="N"'>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="pinEnabledY" name="pinEnabled" value="Y">&nbsp;Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="pinEnabledN" name="pinEnabled" value="N" checked>&nbsp;No
                                                                        </label>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="pinEnabledY" name="pinEnabled" value="Y" checked>&nbsp;Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="pinEnabledN" name="pinEnabled" value="N">&nbsp;No
                                                                        </label>
                                                                        <input type="hidden" id="ipin" value="Y">
                                                                    </s:else>
                                                                </div>
                                                                <span class="help-block"> You can enable transactional PIN. If enabled PIN will be required for transactions </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group" id="pin">
                                                            <label class="col-md-3 control-label">PIN</label>
                                                            <div class="col-xs-4">
                                                                <s:if test='enablePinStatus=="N"'>
                                                                    <input type="password" id="loadPin"  name="loadPin" value="" autocomplete="off" class="form-control">
                                                                </s:if>
                                                                <s:else>
                                                                    <input type="password" id="loadPin"  name="loadPin" value="" autocomplete="off" class="form-control">
                                                                </s:else>
                                                                <span class="help-block"> Keep Blank to do not change current PIN </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Enable API Access ?</label>
                                                            <div class="col-md-4">
                                                                <div class="radio-list">
                                                                    <s:if test="apiAccessStatus=='Y'">
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="apiY" name="api" value="Y" checked>&nbsp;Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="apiN" name="api" value="N">&nbsp;No
                                                                        </label>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="apiY" name="api" value="Y">&nbsp;Yes
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" id="apiN" name="api" value="N" checked>&nbsp;No
                                                                        </label>
                                                                    </s:else>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group" id="cpin">
                                                            <label class="col-md-3 control-label">Current Password</label>
                                                            <div class="col-md-4">
                                                                <input type="password" id="curPass" name="curPass" class="form-control">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-actions fluid">
                                                        <div class=" col-md-offset-3 col-md-9">
                                                            <button type="button" onclick="updateResellerProfile('<s:property value="userId"/>');" class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Save&nbsp;Changes
                                                            </button>
                                                            <button type="reset" class="btn default">Cancel</button>
                                                            <img id="createLoadingImage" src="" alt="" />
                                                        </div>
                                                    </div>
                                                </s:iterator>
                                            </s:if>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
                                                                jQuery(document).ready(function () {
                                                                    // initiate layout and plugins
                                                                    Metronic.init(); // init metronic core components
                                                                    Layout.init(); // init current layout
                                                                    QuickSidebar.init() // init quick sidebar
                                                                    //        UIIdleTimeout.init();

                                                                    var i_pin = $("#ipin").val();
                                                                    if (i_pin == "Y") {
                                                                        $("#pin").show();
                                                                    } else {
                                                                        $("#pin").hide();
                                                                    }

                                                                    $("input[name$='pinEnabled']").click(function () {
                                                                        var pin_enabled = $(this).val();
                                                                        if (pin_enabled == "Y") {
                                                                            $("#pin").show();
                                                                        } else {
                                                                            $("#pin").hide();
                                                                        }
                                                                    });
                                                                });
        </script>

    </body>
</html>