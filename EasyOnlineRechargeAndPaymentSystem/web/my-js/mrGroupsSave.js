$(document).ready(function () {

    var item_remove = $('#item-remove');
    item_remove.hide();

    item_remove.on('click', function () {
        $("#invoice_items tr.info").fadeOut(300, function () {
            $(this).remove();
        });
    });

    $('#blank-add').on('click', function () {
        $("#invoice_items").find('tbody')
                .append('<tr><td class="count"></td><td><input type="text" id="phone" name="phone" class="form-control input-medium item_name" value=""></td><td><input type="text" id="type" name="type" class="form-control input-small" value=""></td><td><input type="text" id="amount" name="amount" class="form-control input-small" value=""></td><td><input type="text" id="modem" name="modem" class="form-control input-small" value="1"></td></tr>');
    });

    $('#invoice_items').on('click', '.item_name', function () {
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");
        item_remove.show();
    });

    /*****************************************************/

//    $("#emsg").hide();
//
//    $("#submit").click(function (e) {
//        e.preventDefault();
//        $('#ibox_form').block({message: null});
//
//        var _url = $("#_url").val();
//
//        $.post(_url + 'groups/post/', $('#ib_form').serialize(), function (data) {
//
//            setTimeout(function () {
//
//                var sbutton = $("#submit");
//
//                var _url = $("#_url").val();
//                if ($.isNumeric(data)) {
//
//                    window.location = _url + 'groups/edit/' + data + '/';
//                } else {
//                    $('#ibox_form').unblock();
//                    var body = $("html, body");
//                    body.animate({scrollTop: 0}, '1000', 'swing');
//                    $("#emsgbody").html(data);
//                    $("#emsg").show("slow");
//                }
//            }, 2000);
//        });
//    });
});