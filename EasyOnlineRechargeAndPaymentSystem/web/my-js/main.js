(function($){

	$(document).ready(function(){

	var bgSliderWrap = $("#bgSliderWrap");

	bgSliderWrap.owlCarousel({
		items : 1,
		itemsDesktop : [1000,1],
		itemsDesktopSmall : [900,1],
		itemsTablet: [600,1],
		itemsMobile : false,
		autoPlay : true
	});



	var brandSlider = $("#brandSlider");

	brandSlider.owlCarousel({
		items : 9,
		itemsDesktop : [1000,7],
		itemsDesktopSmall : [900,6],
		itemsTablet: [600,4],
		itemsMobile : false,
		pagination : false,
		addClassActive : true
	});


	var brandData = brandSlider.data('owlCarousel');


	$('.brand-slider-inner').find('.bs-ctrl').on('click', function(e){
		e.preventDefault();

		if($(this).hasClass('bs-left')){
			brandData.prev();
		} else {
			brandData.next();
		}
	});



	$('#webTicker').webTicker();



	if (jQuery('#newsSlider').length > 0) {
	    jQuery('#newsSlider').cycle({
	        slides: 'li',
	        fx : 'fade',
	        next : '#neswNext',
	        prev : '#neswPrev'
	    });
	};




	// data-cycle-carousel-visible="3"
	// data-cycle-prev="#neswPrev"
	// data-cycle-next="#neswNext"




	}); // end document ready



})(jQuery);
