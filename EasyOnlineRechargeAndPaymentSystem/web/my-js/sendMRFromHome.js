function phoneTest(a) {
    var b = /^(\+88|88|)(\d{11})$/;
    return b.test(a)
}

function amountTest(a) {
    var b = /^[1-9][0-9]+$/;
    return b.test(a)
}

function showError(a, b) {
    alert(b);
    a.focus();
    return false;
}

function validateMRInHome(a, b, c) {

    var d = document.getElementById("phone").value;

    var reTypePhone = document.getElementById("reTypePhone").value;

    if (document.getElementById('prepaid').checked) {
        var e = 'Prepaid';
    } else {
        var e = 'Postpaid';
    }

    if (d == "") {
        return showError(a, "Mobile No cannot be empty");
    }

    var h = phoneTest(d);

    if (h == true) {

        var phone = d;

        if (d.length > 11) {
            d = d.substring(d.length - 11);
        }

        var i = d.substring(0, 3);

        if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {

            if (phone != reTypePhone) {
                alert("Mobile number do not match !");
                return false;
            }

            var g = document.getElementById("amount").value;

            if (g == "") {
                return showError(b, "Mobile Recharge amount cannot be empty");
            }

            if (g < 10) {
                return showError(b, "Amount cannot be less than 10.");
            }

            var am = amountTest(g);

            if (am == true) {

                if (e == 'Prepaid' && g > 1e3) {
                    return showError(b, "Prepaid mobile recharge amount cannot be greater than 1000.");
                }
            } else {
                return showError(b, "Invalid amount");
            }
            return mrActionConfirmation("You are sending Tk." + g + " to mobile number " + d + ". Please Make sure- Phone Number, Amount and Type is Correct. Click OK to send or Cancel to Cancel");
        } else {
            return showError(a, "Invalid Mobile No");
        }
    } else {
        return showError(a, "Invalid Mobile No")
    }
}

function mrActionConfirmation(a) {

    var b = confirm(a);

    if (b == true) {
        sendMobileRecharge();
        return true;
    } else {
        return false;
    }
}

function showLastMRHistoryInHome() {

    $.ajax({
        type: "POST",
        url: 'ShowLastMRHInHome',
        success: function (data) {
            $("#lastMRHHomeTable").html(data);
        }
    });
}

function sendMobileRecharge() {

    var phoneNo = document.getElementById("phone").value;

    if (phoneNo.length > 11) {
        phoneNo = phoneNo.substring(phoneNo.length - 11);
    }
    var i = phoneNo.substring(0, 3);

    if (i == "019") {
        i = "Banglalink";
    } else if (i == "018") {
        i = "Robi";
    } else if (i == "017") {
        i = "Grameen Phone";
    } else if (i == "016") {
        i = "Airtel";
    } else if (i == "015") {
        i = "Teletalk";
    } else {
        return showError(a, "Invalid mobile operator");
    }

    var phone = document.getElementById("phone").value;

    var amount = document.getElementById("amount").value;

    var pin = document.getElementById("pin").value;

    var type = "";

    if (document.getElementById('prepaid').checked) {
        type = '0';
    } else {
        type = '1';
    }

    var dataString = 'phone=' + phone;
    dataString += '&amount=' + amount;
    dataString += '&type=' + type;
    dataString += '&pin=' + pin;
    dataString += '&operator=' + i;

    document.getElementById('mrSendLoadingImage').src = 'images/loading.gif';

    $.ajax({
        type: "POST",
        url: 'MobileRechargeFromHome',
        data: dataString,
        success: function (data) {
            document.getElementById('mrSendLoadingImage').src = '';
            $('#modal_flexiload').modal('hide');
            $("#message").html(data);
            showLastMRHistoryInHome();
        }
    });
}