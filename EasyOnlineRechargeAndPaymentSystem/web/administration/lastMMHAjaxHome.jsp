<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="lastMMoneyHInfoList !=null">
    <s:if test="lastMMoneyHInfoList.size() !=0">
        <s:iterator value="lastMMoneyHInfoList" var="lastMMHInfo">
            <tr>
                <td>${lastMMHInfo.mobileMoneyId}</td>
                <td>${lastMMHInfo.sender}</td>
                <td>${lastMMHInfo.receiver}</td>
                <td>${lastMMHInfo.type}</td>
                <td>${lastMMHInfo.givenBalance}</td>
                <td>
                    <s:if test="activeStatus=='Y'">
                        <span>
                            Success
                        </span>
                    </s:if>
                    <s:elseif test="activeStatus=='N'">
                        <span>
                            Pending
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='P'">
                        <span>
                            Processing
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='W'">
                        <span>
                            Waiting
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='F'">
                        <span>
                            Failed
                        </span>
                    </s:elseif>
                </td>
                <td>${lastMMHInfo.trid}</td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>