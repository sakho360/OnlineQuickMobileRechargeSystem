<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="paymentInfoList !=null">
    <s:if test="paymentInfoList.size() !=0">
        <% int id = 0; %>
        <s:iterator value="paymentInfoList">
            <% id++;%>
            <tr>
                <td class="active">
                    <%=id%>
                </td>
                <td>
                    <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');">
                        <s:property value="userId"/>
                    </a>
                </td>
                <td class="success">
                    <s:property value="userName"/>
                </td>
                <td class="warning">
                    <s:if test="contactNumber1 !=null">
                        <s:property value="contactNumber1"/>
                    </s:if>
                    <s:else>
                        No Phone Number
                    </s:else>
                </td>
                <td class="info">
                    Tk. <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                </td>
                <td class="danger">
                    Tk. <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>
