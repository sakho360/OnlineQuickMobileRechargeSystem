<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="mrBalanceInfoList !=null">
    <s:if test="mrBalanceInfoList.size() !=0">
        <s:iterator value="mrBalanceInfoList">
            <s:property value="mrCurrentBalance"/>
        </s:iterator>
    </s:if>
</s:if>

<s:if test="mmBalanceInfoList !=null">
    <s:if test="mmBalanceInfoList.size() !=0">
        <s:iterator value="mmBalanceInfoList">
            <s:property value="mmCurrentBalance"/>
        </s:iterator>
    </s:if>
</s:if>