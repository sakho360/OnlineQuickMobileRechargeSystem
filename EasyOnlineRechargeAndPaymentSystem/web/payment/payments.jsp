<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <!--FOR COMBOBOX-->
        <link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function viewResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "ViewResellerButtonClicked?resellerId=" + dataString;
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Payments&nbsp;<small>Add&nbsp;/&nbsp;Return Fund</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">
                                <div class="portlet-title">

                                    <div class="caption">
                                        Add&nbsp;Fund
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body form">

                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class=" col-md-3  control-label">Add Fund to</label>
                                                <div class=" col-md-4  ">
                                                    <select id="cid" name="userList" data-placeholder="Select User" class="form-control input-large">
                                                        <option value=""></option>
                                                        <s:if test="paymentInfoList !=null">
                                                            <s:if test="paymentInfoList.size() !=0">
                                                                <s:iterator value="paymentInfoList">
                                                                    <s:if test="resellerId !=null">
                                                                        <s:iterator value="resellerId">
                                                                            <option <s:if test="userId==resellerId">selected="selected"</s:if> value="<s:property value="userId"/>"><s:property value="userId"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <option value="<s:property value="userId"/>"><s:property value="userId"/></option>
                                                                    </s:else>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                    <img id="createLoadingImage" src="" alt="" />
                                                </div>
                                            </div>

                                            <div id="typeParentDiv">

                                            </div>

                                            <div class="form-group">
                                                <label class=" col-md-3   control-label">For </label>
                                                <div class=" col-md-4  ">
                                                    <div class="radio-list">
                                                        <s:if test='loadType=="MM"'>
                                                            <% if (PM03CM01 && PERMISSION_MR) {%>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType1" value="MR"/>
                                                                Mobile Recharge
                                                            </label>
                                                            <% }%>

                                                            <% if (PM04CM01 && PERMISSION_MM) {%>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType2" value="MM" checked/>
                                                                Mobile Money
                                                            </label>
                                                            <% }%>
                                                        </s:if>
                                                        <s:else>
                                                            <% if ((PM03CM01 && PERMISSION_MR) && (PM04CM01 && PERMISSION_MM)) {%>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType1" value="MR" checked/>
                                                                Mobile Recharge
                                                            </label>

                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType2" value="MM"/>
                                                                Mobile Money
                                                            </label>
                                                            <% } else if (PM03CM01 && PERMISSION_MR) { %>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType1" value="MR" checked/>
                                                                Mobile Recharge
                                                            </label>
                                                            <% } else if (PM04CM01 && PERMISSION_MM) {%>
                                                            <label class="radio-inline">
                                                                <input type="radio" name="loadType" id="loadType2" value="MM" checked/>
                                                                Mobile Money
                                                            </label>
                                                            <% }%>
                                                        </s:else>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label class=" col-md-3   control-label"><span id="cbaltxt">Current Balance</span></label>
                                                <div class=" col-md-4  ">
                                                    <div id="curBalDiv" class="input-group">
                                                        <input type="text" id="cBal" name="cBal" value="" disabled class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label class=" col-md-3   control-label">Amount</label>
                                                <div class=" col-md-4  ">
                                                    <div class="input-group">
                                                        <input type="text" id="amount" name="amount" autocomplete="off" value="" class="form-control" maxlength="7">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class=" col-md-3   control-label">Type </label>
                                                <div class=" col-md-4  ">
                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="amountType" id="amountType1" value="ADD" class="amount_type" checked>
                                                            Add
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="amountType" id="amountType2" value="RETURN" class="amount_type">
                                                            Return
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group last has-success">
                                                <label class=" col-md-3   control-label">Memo&nbsp;/&nbsp;Description</label>
                                                <div class=" col-md-4  ">
                                                    <div class="input-group">
                                                        <input type="text" id="description" name="description" value="" class="form-control  input-xlarge" maxlength="100">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group has-success">
                                                <label class=" col-md-3   control-label">PIN</label>
                                                <div class=" col-md-4  ">
                                                    <div class="input-group">
                                                        <input type="password" id="pin" name="pin" value="" class="form-control  input-xlarge" maxlength="20">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-actions fluid">
                                            <div class=" col-md-offset-3 col-md-9">

                                                <% if (PM03CM01 || PM04CM01) {%>
                                                <% if (PERMISSION_MR || PERMISSION_MM) {%>
                                                <s:if test="pinStatus !=null">
                                                    <s:if test='pinStatus=="N"'>
                                                        <button type="button" id="btnSubmit" disabled class="btn blue">
                                                            <i class="icon-check"></i>
                                                            Submit
                                                        </button>
                                                    </s:if>
                                                    <s:else>
                                                        <button type="button" id="btnSubmit" class="btn blue">
                                                            <i class="icon-check"></i>
                                                            Submit
                                                        </button>
                                                    </s:else>
                                                </s:if>
                                                <% } else {%>
                                                <button type="button" id="btnSubmit" disabled class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Submit
                                                </button>
                                                <% }%>
                                                <% } else {%>
                                                <button type="button" id="btnSubmit" disabled class="btn blue">
                                                    <i class="icon-check"></i>
                                                    Submit
                                                </button>
                                                <% }%>

                                                <button type="reset" class="btn default">&nbsp;Cancel</button>
                                                <img id="btnCreateLoadingImage" src="" alt="" />
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        All Reseller's Created by <s:property value="messageString"/>
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div style="padding-top: 16px;" class="table-scrollable">
                                        <table id="datatable_orders" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Serial</th>
                                                    <th>User&nbsp;ID</th>
                                                    <th>Full&nbsp;Name</th>
                                                    <th>Phone</th>
                                                    <th>MR&nbsp;Bal</th>
                                                    <th>MM&nbsp;Bal</th>
                                                </tr>
                                            </thead>
                                            <tbody id="payments_tbl">
                                                <s:if test="paymentInfoList !=null">
                                                    <s:if test="paymentInfoList.size() !=0">
                                                        <% int id = 0; %>
                                                        <s:iterator value="paymentInfoList">
                                                            <% id++;%>
                                                            <tr>
                                                                <td class="active">
                                                                    <%=id%>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');">
                                                                        <s:property value="userId"/>
                                                                    </a>
                                                                </td>
                                                                <td class="success">
                                                                    <s:property value="userName"/>
                                                                </td>
                                                                <td class="warning">
                                                                    <s:if test="contactNumber1 !=null">
                                                                        <s:property value="contactNumber1"/>
                                                                    </s:if>
                                                                    <s:else>
                                                                        No Phone Number
                                                                    </s:else>
                                                                </td>
                                                                <td class="info">
                                                                    Tk. <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                                </td>
                                                                <td class="danger">
                                                                    Tk. <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                </td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <!--FOR RELOAD-->
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <!--FOR COMBOBOX-->
        <script src="<%= request.getContextPath()%>/my-js/select2.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/payments.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                        $(document).ready(function () {
                                                                            $('#datatable_orders').DataTable({
                                                                                "order": [[1, "asc"]],
                                                                                "orderCellsTop": true,
                                                                                "pagingType": "full_numbers"
                                                                            });
                                                                        });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

    </body>
</html>