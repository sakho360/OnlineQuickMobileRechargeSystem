<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMrPaymentMadeInfoList !=null">
    <s:if test="singleMrPaymentMadeInfoList.size() !=0">
        <s:iterator value="singleMrPaymentMadeInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Mobile Recharge Payment Made ID # <s:property value="mrTransactionHistoryId"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN ALERTS PORTLET-->
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    Payment Made Details
                                </div>
                            </div>
                            <div class="portlet-body">
<!--                                <div class="alert alert-success">
                                    <strong>Success!</strong> Transaction ID is - 
                                </div>                    
                                <form method="post" class="form-inline" role="form" action="#">
                                    <div class="form-group">
                                        <label class="sr-only" for="trid">TrID</label>
                                        <input class="form-control" autocomplete="off" id="trid" name="trid" value="Success" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-success">Manual Confirm</button>
                                </form>
                                <div class="clearfix">
                                    <br>
                                </div>
                                <form method="post" class="form-inline" role="form" action="#">
                                    <div class="form-group">
                                        <label class="sr-only" for="trid">TrID</label>
                                        <input autocomplete="off" placeholder="Failed Reason..." class="form-control" id="trid" name="trid" value="Success" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-danger">Refund</button>
                                </form>
                                <div class="clearfix">
                                    <br>
                                                                    <a href="" class="btn red">Refund</a>
                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>
                                </div>-->
                                <h4>Details</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 30%">Payment Made ID</th>
                                                <th><s:property value="mrTransactionHistoryId"/></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>From</td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                            </tr>
                                            <tr>
                                                <td>Current Balance</td>
                                                <td><s:property value="0"/></td>
                                            </tr>
                                            <tr>
                                                <td>To</td>
                                                <td><s:property value="userInfo.userId"/></td>
                                            </tr>
                                            <tr>
                                                <td>Balance Type</td>
                                                <td>Mobile Recharge</td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><s:property value="addBalance"/></td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <s:if test="type=='Y'">
                                                        Refund
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #009966;">
                                                            Waiting
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
<!--                                            <tr>
                                                <td>Status</td>
                                                <td>
                                            </tr>-->
                                            <tr>
                                                <td>Request Time</td>
                                                <td><s:property value="balanceGivenDate"/></td>
                                            </tr>
<!--                                            <tr>
                                                <td>Send Time</td>
                                                <td>Default</td>
                                            </tr>
                                            <tr>
                                                <td>Success Time</td>
                                                <td>Default</td>
                                            </tr>
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>IP</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>A</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>R</td>
                                                <td></td>
                                            </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Orders For this number</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Sender</th>
                                                <th>Phone</th>
                                                <th>Date</th>
                                                <!--<th>Status</th>-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><s:property value="mrTransactionHistoryId"/></td>
                                                <td><s:property value="balanceGivenBy"/></td>
                                                <td><s:property value="userInfo.userId"/></td>
                                                <td><s:property value="balanceGivenDate"/></td>
<!--                                                <td>
                                                    <span class="label label-sm label-success">

                                                    </span>
                                                </td>-->
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
<!--                                <h4>Related SMS</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Time</th>
                                                <th>Status</th>
                                                <th>SMS</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>-->
                            </div>
                        </div>
                        <!-- END ALERTS PORTLET-->
                    </div>
                </div>
            </div>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>