<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<div class="alert alert-<s:property value="messageColor"/> fade in">
    <button class="close" data-dismiss="alert">
        ×
    </button>
    <i class="fa-fw fa fa-times"></i>
    <s:property value="messageString"/>
</div>
