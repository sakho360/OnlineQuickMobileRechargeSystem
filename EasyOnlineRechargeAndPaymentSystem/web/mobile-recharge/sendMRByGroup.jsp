<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png"  rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/componentsModal.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            table {
                counter-reset:section;
            }
            .count:before {
                counter-increment:section;
                content:counter(section);
            }
        </style>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Recharge&nbsp;<small>Send&nbsp;Mobile&nbsp;Recharge&nbsp;By&nbsp;Group</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box green" id="ibox_form">
                                <div class="portlet-body">
                                    <form method="post" id="ib_form">
                                        <s:if test="singleMRGroupInfoList !=null">
                                            <s:if test="singleMRGroupInfoList.size() !=0">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Group&nbsp;Name</label>
                                                    <s:iterator value="singleMRGroupInfoList" status="gn">
                                                        <s:if test="#gn.last==true">
                                                            <input type="text" id="groupName" name="groupName" value="<s:property value="mrGroupName"/>" readonly class="form-control">
                                                        </s:if>
                                                    </s:iterator>
                                                </div>
                                                <div class="table-scrollable">
                                                    <table class="table table-condensed table-hover invoice-table" id="invoice_items">
                                                        <thead>
                                                            <tr>
                                                                <th>Serial</th>
                                                                <th>Phone*<br>[e.g. 01XXXXXXXXX ]</th>
                                                                <th>Type*<br>[Prepaid = 0 &amp; Postpaid = 1]</th>
                                                                <th>Amount*</th>
                                                                <th>Modem*</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="groupTBody">
                                                            <s:iterator value="singleMRGroupInfoList">
                                                                <tr>
                                                                    <td class="count">

                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egPhone" name="egPhone" class="form-control input-medium item_name pPhone" value="<s:property value="mrGroupInfo.mobileNumber"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egType" name="egType" class="form-control input-small pType" value="<s:property value="mrGroupInfo.type"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egAmount" name="egAmount" class="form-control input-small pAmount" value="<s:property value="mrGroupInfo.amount"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egModem" name="egModem" class="form-control input-small pModem" value="<s:property value="mrGroupInfo.modemNumber"/>">
                                                                    </td>
                                                                </tr>
                                                            </s:iterator>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="form-group has-success">
                                                    <label>
                                                        <strong class="control-label">PIN</strong>
                                                    </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <i class="icon-key"></i>
                                                        </span>

                                                        <input type="password" id="pin" name="pin" class="form-control input-lg" autocomplete="off" style="width: 250px;font-size:20px; color:#C00">
                                                    </div>
                                                </div>
                                                <button type="submit" id="btn_send_mr" class="btn btn-success">Send Mobile Recharge</button>
                                                <img id="createLoadingImage" src="" alt="" />
                                                <hr>
                                            </s:if>
                                        </s:if>
                                    </form>
                                </div>
                            </div>
                            <!-- END CONDENSED TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>


        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <!--<script src="<%= request.getContextPath()%>/my-js/mrGroupsEdit.js" type="text/javascript"></script>-->

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();
            });
        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                $("#btn_send_mr").click(function (e) {

                    e.preventDefault();

                    var message = confirm("Do you want to send mobile recharge by group ?");

                    if (message == true) {

                        document.getElementById('createLoadingImage').src = 'images/loading.gif';

                        $.post('SendMobileRechargeByGroup', $('#ib_form').serialize(), function (data) {

                            setTimeout(function () {

                                if (data.trim() == 1) {
                                    document.getElementById('createLoadingImage').src = '';
                                    window.location = 'ConfirmSendMobileRechargeByGroup?messageString=' + "1";
                                } else if (data.trim() == 2) {
                                    document.getElementById('createLoadingImage').src = '';
                                    window.location = 'ConfirmSendMobileRechargeByGroup?messageString=' + "2";
                                } else {
                                    document.getElementById('createLoadingImage').src = '';
                                    window.location = 'ConfirmSendMobileRechargeByGroup?messageString=' + "faild";
                                }
                            }, 2000);
                        });
                    }
                });
            });
        </script>

    </body>
</html>