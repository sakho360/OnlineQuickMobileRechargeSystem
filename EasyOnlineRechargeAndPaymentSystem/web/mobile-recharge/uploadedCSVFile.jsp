<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/componentsModal.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function phoneTest(a) {
                var b = /^(\+88|88|)(\d{11})$/;
                return b.test(a)
            }
            //
            function amountTest(a) {
                var b = /^[1-9][0-9]+$/;
                return b.test(a)
            }
            //
            function showError(a, b) {
                alert(b);
//                a.focus();
                return false;
            }
            //
            function validate(a, b, c) {

                var d = '';
                var type = '';
                var g = '';
                var modem = '';

                var ph = document.getElementsByClassName("inputPhone");
                for (i = 0; i < ph.length; i++) {
                    d = ph[i].value;
                }

                var ty = document.getElementsByClassName("inputType");
                for (i = 0; i < ty.length; i++) {
                    type = ty[i].value;
                }

                var amo = document.getElementsByClassName("inputAmount");
                for (i = 0; i < amo.length; i++) {
                    g = amo[i].value;
                }

                var mod = document.getElementsByClassName("inputModem");
                for (i = 0; i < mod.length; i++) {
                    modem = mod[i].value;
                }

                if (d == null || d == "") {
                    return showError(a, "Mobile No cannot be empty");
                }

                var h = phoneTest(d);

                if (h == true) {

                    if (d.length > 11) {
                        d = d.substring(d.length - 11);
                    }

                    var i = d.substring(0, 3);

                    if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {

                        if (type == "" || type == null) {
                            return showError(b, "Mobile Recharge type cannot be empty");
                        }

                        if (type == 0 || type == 1) {
                            var am = amountTest(g);
                        } else {
                            return showError(b, "Type should be 0 or 1");
                        }

                        if (g == "" || g == null) {
                            return showError(b, "Mobile Recharge amount cannot be empty");
                        }

                        if (am == true) {

                            if (type == 0 && g > 1e3) {
                                return showError(b, "Prepaid mobile recharge amount cannot be greater than 1000.");
                            }
                        } else {
                            return showError(b, "Invalid amount");
                        }

                        if (g < 10) {
                            return showError(b, "Amount cannot be less than 10.");
                        }

                        if (modem == "" || modem == null) {
                            return showError(b, "Modem cannot be empty");
                        }

                        if (modem != 1) {
                            return showError(b, "Modem should be 1");
                        }

                        return actionConfirmation("Do you want to send mobile recharge by CSV file");
                    } else {
                        return showError(a, "Invalid Mobile No");
                    }
                } else {
                    return showError(a, "Invalid Mobile No")
                }
            }
            //
            function actionConfirmation(a) {

                var b = confirm(a);

                if (b == true) {
                    return true;
                } else {
                    return false;
                }
            }
            //
//            function showLastMRHistory() {
//
//                $.ajax({
//                    type: "POST",
//                    url: 'ShowLastMRHistory',
//                    success: function (data) {
//                        $("#lastMRHTable").html(data);
//                    }
//                });
//            }
            //
//            function sendMobileRecharge() {
//
//                var phoneNo = document.getElementById("phone").value;
//
//                if (phoneNo.length > 11) {
//                    phoneNo = phoneNo.substring(phoneNo.length - 11);
//                }
//                var i = phoneNo.substring(0, 3);
//
//                if (i == "019") {
//                    i = "Banglalink";
//                } else if (i == "018") {
//                    i = "Robi";
//                } else if (i == "017") {
//                    i = "Grameen Phone";
//                } else if (i == "016") {
//                    i = "Airtel";
//                } else if (i == "015") {
//                    i = "Teletalk";
//                } else {
//                    return showError(a, "Invalid mobile operator");
//                }
//
//                var amount = document.getElementById("amount").value;
//
//                var pin = document.getElementById("pin").value;
//
//                var type = "";
//
//                if (document.getElementById('prepaid').checked) {
//                    type = '0';
//                } else {
//                    type = '1';
//                }
//
//                var phone = phoneNo.substring(phoneNo.length - 10);
//
//                var dataString = 'phone=' + phone;
//                dataString += '&amount=' + amount;
//                dataString += '&type=' + type;
//                dataString += '&pin=' + pin;
//                dataString += '&operator=' + i;
//
//                alert(dataString);
//                //
//                document.getElementById('createLoadingImage').src = 'images/loading.gif';
//                //
//                $.ajax({
//                    type: "POST",
//                    url: 'SubmitMobileRecharge',
//                    data: dataString,
//                    success: function (data) {
//                        document.getElementById('createLoadingImage').src = '';
//                        $("#message").html(data);
//                        showLastMRHistory();
//                    }
//                });
//            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                CSV <small>uploaded</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <s:property value="messageString"/>
                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <!--SendMRByCsvFile-->
                            <!-- BEGIN FORM-->
                            <form onsubmit="return validate();" method="post" action="SendMRByCsvFile">
                                <div class="portlet box green">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-picture"></i>
                                            Bulk Mobile Recharge
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="table-scrollable">
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Serial</th>
                                                        <th>Phone*<br>[e.g. 01XXXXXXXXX ]</th>
                                                        <th>Type*<br>[Prepaid = 0 &amp; Postpaid = 1]</th>
                                                        <th>Amount*</th>
                                                        <th>Modem*</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <% int sId = 0;%>

                                                    <% int i = 0;%>

                                                    <s:if test="csvFileInfoList !=null">
                                                        <s:iterator value="csvFileInfoList">
                                                            <% sId++;%>
                                                            <tr>
                                                                <td><%= sId%></td>
                                                                <td>
                                                                    <input type="text" id="phone" name="csvFileInfoList[<%= i%>].mobileNumber" class="form-control input-medium inputPhone" value="<s:property value="mobileNumber"/>">
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="type" name="csvFileInfoList[<%= i%>].type" class="form-control input-small inputType" value="<s:property value="type"/>">
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="amount" name="csvFileInfoList[<%= i%>].amount" class="form-control input-small inputAmount" value="<s:property value="amount"/>">
                                                                </td>
                                                                <td>
                                                                    <input type="text" id="modem" name="csvFileInfoList[<%= i%>].modemNumber" class="form-control input-small inputModem" value="<s:property value="modemNumber"/>">
                                                                </td>
                                                            </tr>
                                                            <% i++;%>
                                                        </s:iterator>
                                                    </s:if>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td style="text-align: right;">
                                                            <strong>Total Amount: </strong>
                                                        </td>
                                                        <td>Default</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="icon-key"></i>
                                                                </span>
                                                                <input type="password" id="pin" name="pin" class="form-control input-lg" placeholder="Pin to confirm" autocomplete="off" style="width: 202px; font-size:20px; color:#C00">
                                                            </div>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <button type="submit" class="btn green">Confirm</button>
                                                            <a href="SendMRFromCsvFile" class="btn red">Cancel</a>
                                                        </td>
                                                        <td style="text-align: right;">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                        <!-- END PORTLET-->
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <script>
                                jQuery(document).ready(function () {
                                    // initiate layout and plugins
                                    Metronic.init(); // init metronic core components
                                    Layout.init(); // init current layout
                                    QuickSidebar.init() // init quick sidebar
                                    //        UIIdleTimeout.init();

                                    EcommerceOrders.init();
                                });
        </script>

    </body>
</html>