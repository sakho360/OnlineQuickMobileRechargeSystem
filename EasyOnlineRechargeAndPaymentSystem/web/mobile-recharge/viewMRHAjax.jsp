<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleMRHInfoList !=null">
    <s:if test="singleMRHInfoList.size() !=0">
        <s:iterator value="singleMRHInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Transaction ID&nbsp;&nbsp;-&nbsp;&nbsp;<s:property value="trid"/></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN ALERTS PORTLET-->
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    Order Details
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="alert alert-success">
                                    <strong>
                                        <s:if test="activeStatus=='W'">
                                            <span>
                                                Waiting
                                            </span>
                                        </s:if>
                                        <s:elseif test="activeStatus=='S'">
                                            <span>
                                                Sent
                                            </span>
                                        </s:elseif>
                                        <s:elseif test="activeStatus=='Y'">
                                            <span>
                                                Success
                                            </span>
                                        </s:elseif>
                                        &nbsp;!&nbsp;&nbsp;
                                    </strong>
                                    TnxID ID is&nbsp;&nbsp;-&nbsp;&nbsp;<s:property value="tnxid"/>
                                </div>                    
                                <!--                                <form method="post" class="form-inline" role="form" action="#">
                                                                    <div class="form-group">
                                                                        <label class="sr-only" for="trid">TrID</label>
                                                                        <input class="form-control" autocomplete="off" id="trid" name="trid" value="Success" type="text">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-success">Manual Confirm</button>
                                                                </form>-->
                                <!--                                <div class="clearfix">
                                                                    <br>
                                                                </div>-->
                                <!--                                <form method="post" class="form-inline" role="form" action="#">
                                                                    <div class="form-group">
                                                                        <label class="sr-only" for="trid">TrID</label>
                                                                        <input autocomplete="off" placeholder="Failed Reason..." class="form-control" id="trid" name="trid" value="Success" type="text">
                                                                    </div>
                                                                    <button type="submit" class="btn btn-danger">Refund</button>
                                                                </form>-->
                                <!--                                <div class="clearfix">
                                                                    <br>
                                                                                                    <a href="" class="btn red">Refund</a>
                                                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>
                                                                </div>-->
                                <h4>Details</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 30%">Order ID</th>
                                                <th><s:property value="mobileRechargeId"/></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Sender</td>
                                                <td><s:property value="sender"/></td>
                                            </tr>
                                            <tr>
                                                <td>Your Balance</td>
                                                <td><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></td>
                                            </tr>
                                            <tr>
                                                <td>Receiver</td>
                                                <td><s:property value="receiver"/></td>
                                            </tr>
                                            <tr>
                                                <td>Operator</td>
                                                <td><s:property value="operator"/></td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td><s:property value="givenBalance"/></td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td>
                                                    <s:if test="type==0">
                                                        Prepaid
                                                    </s:if>
                                                    <s:else>
                                                        <span style="color: #009966;">
                                                            Postpaid
                                                        </span>
                                                    </s:else>
                                                </td>
                                            </tr>
                                            <!--                                            <tr>
                                                                                            <td>Modem</td>
                                                                                            <td>
                                            <s:if test='originator=="1"'>
                                                Modem
                                            </s:if>
                                            <s:else>
                                                Web
                                            </s:else>
                                        </td>
                                    </tr>-->
                                            <tr>
                                                <td>Status</td>
                                                <td>
                                                    <s:if test="activeStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:if>
                                                    <s:elseif test="activeStatus=='S'">
                                                        <span>
                                                            Sent
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='Y'">
                                                        <span>
                                                            Success
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='R'">
                                                        <span>
                                                            Refunded
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='P'">
                                                        <span>
                                                            Processing
                                                        </span>
                                                    </s:elseif>

                                                    <s:elseif test="activeStatus=='F'">
                                                        <span>
                                                            Failed
                                                        </span>
                                                    </s:elseif>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Request Time</td>
                                                <td><s:property value="purchasedOn"/></td>
                                            </tr>
                                            <!--                                            <tr>
                                                                                            <td>Send Time</td>
                                                                                            <td>Default</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Success Time</td>
                                                                                            <td>Default</td>
                                                                                        </tr>-->
                                            <tr>
                                                <td>Transaction ID</td>
                                                <td><s:property value="trid"/></td>
                                            </tr>
                                            <!--                                            <tr>
                                                                                            <td>IP</td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>A</td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>R</td>
                                                                                            <td></td>
                                                                                        </tr>-->
                                        </tbody>
                                    </table>
                                </div>
                                <h4>Orders For this number</h4>
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Sender</th>
                                                <th>Phone</th>
                                                <th>Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><s:property value="mobileRechargeId"/></td>
                                                <td><s:property value="sender"/></td>
                                                <td><s:property value="receiver"/></td>
                                                <td><s:property value="purchasedOn"/></td>
                                                <td>
                                                    <s:if test="activeStatus=='Y'">
                                                        <span>
                                                            Success
                                                        </span>
                                                    </s:if>
                                                    <s:elseif test="activeStatus=='N'">
                                                        <span>
                                                            Pending
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='P'">
                                                        <span>
                                                            Processing
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='W'">
                                                        <span>
                                                            Waiting
                                                        </span>
                                                    </s:elseif>
                                                    <s:elseif test="activeStatus=='F'">
                                                        <span>
                                                            Failed
                                                        </span>
                                                    </s:elseif>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--                                <h4>Related SMS</h4>
                                                                <div class="table-scrollable">
                                                                    <table class="table table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>ID</th>
                                                                                <th>Time</th>
                                                                                <th>Status</th>
                                                                                <th>SMS</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                
                                                                        </tbody>
                                                                    </table>
                                                                </div>-->
                            </div>
                        </div>
                        <!-- END ALERTS PORTLET-->
                    </div>
                </div>
            </div>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>