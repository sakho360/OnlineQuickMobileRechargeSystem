<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>

        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/mobileRechargeImage.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!-- PAGE HEADER -->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!-- SIDE MENU -->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Malaysian  Mobile&nbsp;Recharge&nbsp;<small>recharge&nbsp;Digi,&nbsp;Celcom,&nbsp;UMobile,&nbsp;Tron&nbsp;etc.</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!-- END DASHBOARD -->

                    <!-- return message div -->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>
                            <h4>Quick&nbsp;Access</h4>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a href="MalaysiaMobileRecharge" class="icon-btn">
                                <i class="icon-screen-smartphone"></i>
                                <div>Malaysia MR</div>
                            </a>
                            <a href="SendMobileRecharge" class="icon-btn">
                                <i class="icon-paper-plane"></i>
                                <div>BD MR</div>
                            </a>
                            <a href="MobileRechargeHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MR&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>

                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a href="SendMobileMoney" class="icon-btn">
                                <i class="icon-wallet"></i>
                                <div>Send&nbsp;MM</div>
                            </a>
                            <a href="MobileMoneyHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MM&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>
                            <% }%>
                            <% }%>

                            <% if (groupId < 5) {%>
                            <a href="Payments" class="icon-btn">
                                <i class="icon-credit-card"></i>
                                <div>Payments</div>
                            </a>
                            <a href="ShowAllResellers" class="icon-btn">
                                <i class="icon-users"></i>
                                <div>Resellers</div>
                            </a>

                            <% if (groupId == 0) {%>
                            <a href="AddResellerSubAdmin" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 1) {%>
                            <a href="AddReseller4" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 2) {%>
                            <a href="AddReseller3" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 3) {%>
                            <a href="AddReseller2" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 4) {%>
                            <a href="AddReseller1" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>
                    <hr style="margin-bottom: 10px; padding-bottom: 0px;"/>
                    <div class="row">
                        <div style="margin-bottom: 0px; padding-bottom: 0px;" class="col-md-12">
                            <h4>
                                <strong>
                                    Your Mobile Recharge Account Balance : 
                                    <s:if test="currentMrBalInfoList !=null">
                                        <s:if test="currentMrBalInfoList.size() !=0">
                                            <s:iterator value="currentMrBalInfoList">
                                                <s:property value="mrCurrentBalance"/>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                    BDT
                                </strong>
                            </h4>
                            <hr style="margin-top: 17px; padding-top: 0px; margin-bottom: 15px; padding-bottom: 0px;"/>

                            <div style="padding-bottom: 0px; width: 100%; margin-left: 20px;" class="panel operator">
                                <h4>Select Operator</h4>
                                <div class="panelcontent">
                                    <ul>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Digi" class="operators" required checked>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_digi.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Celcom Xpax" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_celcom.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Hotlink Speed" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_hotlink.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Umobile" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_umobile.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Tunetalk" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_tunetalk.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="XOX" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_xox_mobile.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Merchantrade" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_merchantrade.gif" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="rm15" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_rm15.gif" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="speakout" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_speakout.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Tron" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_tron.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="Altel" class="operators" required>
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/logo_altel.png" alt=""/>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div style="padding-bottom: 0px; width: 100%; margin-left: 20px;" class="panel ammount">
                                <h4>Select Amount</h4>
                                <div class="panelcontent">
                                    <ul class="rate-list">

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="5" required checked>
                                            </div>
                                            <div class="ty">
                                                RM 5
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="10" required>
                                            </div>
                                            <div class="ty">
                                                RM 10
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="15" required>
                                            </div>
                                            <div class="ty">
                                                RM 15
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="20" required>
                                            </div>
                                            <div class="ty">
                                                RM 20
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="30" required>
                                            </div>
                                            <div class="ty">
                                                RM 30
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="50" required>
                                            </div>
                                            <div class="ty">
                                                RM 50
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="60" required>
                                            </div>
                                            <div class="ty">
                                                RM 60
                                            </div>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" name="amount" value="100" required>
                                            </div>
                                            <div class="ty">
                                                RM 100
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-4">
                            <div style="padding-top: 0px; margin-top: 0px;" class="portlet light">
                                <div class="portlet-body form">
                                    <form id="mrf" role="form">
                                        <div style="padding-top: 0px; margin-top: 0px;" class="form-body">
                                            <!--                                            <div style="width: 108%" class="panel ammount">
                                                                                            <h4>Select Type</h4>
                                                                                            <div class="panelcontent">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <div class="selectarea radiogroup">
                                                                                                            <input type="radio" id="prepaid" name="type" value="Prepaid" checked="checked">
                                                                                                        </div>
                                                                                                        <div class="ty">
                                                                                                            Prepaid
                                                                                                        </div>
                                                                                                    </li>
                                            
                                                                                                    <li>
                                                                                                        <div class="selectarea radiogroup">
                                                                                                            <input type="radio" id="postpaid" name="type" value="Postpaid">
                                                                                                        </div>
                                                                                                        <div class="ty">
                                                                                                            Postpaid
                                                                                                        </div>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>-->

                                            <div class="panel">
                                                <h4>Mobile Number</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-arrow-right"></i>
                                                            </span>
                                                            <span id="spanPhone">
                                                                <input type="text" id="phone" name="phone" class="form-control input-lg" style="font-size:20px; color:#C00"> 
                                                            </span>
                                                        </div>
                                                        <span class="help-block"><a href="javascript:void(0);" id="my_contacts">Click Here to Add / Manage Contacts</a></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <h4>Re-type Mobile Number</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-arrow-right"></i>
                                                            </span>
                                                            <input type="text" id="reTypePhone" name="reTypePhone" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--                                            <div class="panel">
                                                                                            <h4>Amount</h4>
                                                                                            <div class="panelcontent">
                                                                                                <div class="form-group has-success mr_input">
                                                                                                    <div class="input-group input-small">
                                                                                                        <span class="input-group-addon">
                                                                                                            <i class="icon-credit-card"></i>
                                                                                                        </span>
                                                                                                        <input type="text" id="amount" name="amount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>-->

                                            <div class="panel">
                                                <h4>PIN</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-key"></i>
                                                            </span>
                                                            <input type="password" id="pin" name="pin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="padding-left: 0px;" class="panelcontent">
                                                <div class="form-actions">
                                                    <s:if test="pinStatus !=null">
                                                        <s:if test='pinStatus=="N"'>
                                                            <button id="bt" type="button" onclick="sendMRvalidate();" disabled class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Send
                                                            </button>
                                                        </s:if>
                                                        <s:else>
                                                            <button id="bt" type="button" onclick="sendMRvalidate();" class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Send
                                                            </button>
                                                        </s:else>
                                                    </s:if>
                                                    <button type="reset" class="btn default">Cancel</button>
                                                    <img id="createLoadingImage" src="" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div style="padding-top: 0px; margin-top: 0px;" class="portlet light">
                                <div class="portlet-title">
                                    <div style="font-size: 17px; padding-top: 11px; margin-top: 0px;" class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Recharge&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>TrID</th>
                                                    <th>Sent By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Operator</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMRHTable">
                                                <s:if test="lastMRechargeHInfoList !=null">
                                                    <s:if test="lastMRechargeHInfoList.size() !=0">
                                                        <s:iterator value="lastMRechargeHInfoList" var="lastMRHInfo">
                                                            <tr>
                                                                <td>${lastMRHInfo.trid}</td>
                                                                <td>${lastMRHInfo.sender}</td>
                                                                <td>${lastMRHInfo.receiver}</td>
                                                                <td>
                                                                    <s:if test="type==0">
                                                                        Prepaid
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Postpaid
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td>${lastMRHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMRHInfo.operator}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">My Contacts</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN ALERTS PORTLET -->
                                    <div class="portlet success">
                                        <div class="portlet-body">
                                            <form class="form-horizontal" role="form" id="contact_manage_form">
                                                <div id="contact_resp"></div>
                                                <div class="form-group">
                                                    <label for="inputEmail1" class="col-md-4 control-label">Name</label>
                                                    <div class="col-md-8">
                                                        <input type="text" id="contact_name" name="contact_name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPassword12" class="col-md-4 control-label">Phone Number</label>
                                                    <div class="col-md-8">
                                                        <input type="text" id="contact_number" name="contact_number" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-4 col-md-8">
                                                        <button type="submit" class="btn blue contact_save">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                            <p class="render_my_contact">
                                            <div style="max-height: 230px;" class="table-scrollable">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Serial</th>
                                                            <th>Name</th>
                                                            <th>Phone Number</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb">

                                                    </tbody>
                                                </table>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END ALERTS PORTLET -->
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn red" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <script src="<%= request.getContextPath()%>/my-js/jqueryLibrary.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery-migrate-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery-ui-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_005.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_003.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_004.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-switch.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/contacts.js" type="text/javascript"></script>

        <!--<script src="<%= request.getContextPath()%>/my-js/sendMRValidation.js" type="text/javascript"></script>-->

        <script type="text/javascript">
                                                                var operators = $('.operators');

                                                                operators.change(function () {

                                                                    var keyword = $(this).val();

                                                                    var data = '';

                                                                    if (keyword == "Digi" || keyword == "Celcom Xpax" || keyword == "Umobile"
                                                                            || keyword == "Tunetalk" || keyword == "XOX"
                                                                            || keyword == "Merchantrade") {
                                                                        data = [5, 10, 15, 20, 30, 50, 60, 100];
                                                                    } else if (keyword == "Hotlink Speed") {
                                                                        data = [5, 10, 20, 30, 50, 60, 100];
                                                                    } else if (keyword == "rm15") {
                                                                        data = [15];
                                                                    } else if (keyword == "speakout") {
                                                                        data = [5, 10, 18, 30, 40, 50];
                                                                    } else if (keyword == "Tron") {
                                                                        data = [10];
                                                                    } else if (keyword == "Altel") {
                                                                        data = [5, 10, 20, 30];
                                                                    }

                                                                    var rateList = $('.rate-list');

                                                                    rateList.empty();

                                                                    $.each(data, function (index, value) {

                                                                        var checked = "";

                                                                        if (index === 0) {
                                                                            checked = "checked";
                                                                        }
                                                                        rateList.append("<li> <div class=\"selectarea radiogroup\"> <input " + checked + " type=\"radio\" value=\"" + value + "\" required name=\"amount\"> </div> RM " + value + " </li>");
                                                                    });
                                                                });
        </script>

        <script type="text/javascript">

            function phoneTest(a) {
                var b = /^[0-9]+$/;
                return b.test(a)
            }

//            function amountTest(a) {
//                var b = /^[1-9][0-9]+$/;
//                return b.test(a)
//            }

            function showError(a, b) {
                alert(b);
                a.focus();
                return false;
            }

            function sendMRvalidate(a, b, c) {

                var d = document.getElementById("phone").value;

                var reTypePhone = document.getElementById("reTypePhone").value;

//                var selectedOperator = $('input[name=operator]:radio:checked').val();

                var selectedAmount = $('input[name=amount]:radio:checked').val();

                if (d == "") {
                    return showError(a, "Mobile No cannot be empty");
                }

                var h = phoneTest(d);

                if (h == true) {

//                    var phone = d;
//                    if (d.length > 11) {
//                        d = d.substring(d.length - 11);
//                    }
//
//                    var i = d.substring(0, 3);

//                    if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {

//                        if ((selectedOperator == "Digi" && i == "017") || (selectedOperator == "Celcom" && i == "016") || (selectedOperator == "Hotlink" && i == "019")
//                                || (selectedOperator == "Umobile" && i == "018") || (selectedOperator == "Tunetalk" && i == "015") || (selectedOperator == "XOXMobile" && i == "015")
//                                || (selectedOperator == "Merchantrade" && i == "015") || (selectedOperator == "rm15" && i == "015") || (selectedOperator == "speakout" && i == "015")
//                                || (selectedOperator == "Tron" && i == "015") || (selectedOperator == "Altel" && i == "015")) {

                    if (d != reTypePhone) {
                        alert("Mobile number do not match !");
                        return false;
                    }

//                            var g = document.getElementById("amount").value;
//
//                            if (g == "") {
//                                return showError(b, "Mobile Recharge amount cannot be empty");
//                            }
//
//                            if (g < 10) {
//                                return showError(b, "Amount cannot be less than 10.");
//                            }
//
//                            var am = amountTest(g);
//
//                            if (am == true) {
//
//                                if (e == 'Prepaid' && g > 1e3) {
//                                    return showError(b, "Prepaid mobile recharge amount cannot be greater than 1000.");
//                                }
//                            } else {
//                                return showError(b, "Invalid amount");
//                            }
                    return actionConfirmation("You are sending Tk." + selectedAmount + " to mobile number " + d + ". Please Make sure- Phone Number, Amount and Type is Correct. Click OK to send or Cancel to Cancel");
//                        } else {
//                            return showError(a, "Selected operator and input mobile number do not match");
//                        }
//                    } else {
//                        return showError(a, "Invalid Mobile No");
//                    }
                } else {
                    return showError(a, "Invalid Mobile No")
                }
            }

            function actionConfirmation(a) {

                var b = confirm(a);

                if (b == true) {
                    sendMobileRecharge();
                    return true;
                } else {
                    return false;
                }
            }

            function showLastMRHistory() {

                $.ajax({
                    type: "POST",
                    url: 'ShowLastMRHistory',
                    success: function (data) {
                        $("#lastMRHTable").html(data);
                    }
                });
            }

            function sendMobileRecharge() {

//                var phoneNo = document.getElementById("phone").value;
//
//                if (phoneNo.length > 11) {
//                    phoneNo = phoneNo.substring(phoneNo.length - 11);
//                }
//                var i = phoneNo.substring(0, 3);
//
//                if (i == "019") {
//                    i = "Banglalink";
//                } else if (i == "018") {
//                    i = "Robi";
//                } else if (i == "017") {
//                    i = "Grameen Phone";
//                } else if (i == "016") {
//                    i = "Airtel";
//                } else if (i == "015") {
//                    i = "Teletalk";
//                } else {
//                    return showError(a, "Invalid mobile operator");
//                }

                var selectedOperator = $('input[name=operator]:radio:checked').val();

                var phone = document.getElementById("phone").value;

//                var amount = document.getElementById("amount").value;
                var amount = $('input[name=amount]:radio:checked').val();

                var pin = document.getElementById("pin").value;

//                var type = "";
//
//                if (document.getElementById('prepaid').checked) {
//                    type = '0';
//                } else {
//                    type = '1';
//                }

                var dataString = 'phone=' + phone;
                dataString += '&amount=' + amount;
                dataString += '&pin=' + pin;
                dataString += '&operator=' + selectedOperator;

                document.getElementById('createLoadingImage').src = 'images/loading.gif';

                $.ajax({
                    type: "POST",
                    url: 'SendMalaysianMobileRecharge',
                    data: dataString,
                    success: function (data) {
                        document.getElementById('createLoadingImage').src = '';
                        $("#message").html(data);
                        showLastMRHistory();
                    }
                });
            }

        </script>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar     
            });
        </script>

        <!--       
        <script type="text/javascript">
                    var operators = $('.operators');
        
                    operators.change(function () {
                        var keyword = $(this).val();
                        $.get("http://mycashmy.com/operator-rates?keyword=" + keyword, function (data)
                        {
                            var rateList = $('.rate-list');
                            rateList.empty();
                            $.each(data, function (index, value)
                            {
                                var checked = "";
                                if (index === 0)
                                {
                                    checked = "checked";
                                }
        
                                rateList.append("<li> <div class=\"selectarea radiogroup\"> <input " + checked + " type=\"radio\" value=\"" + value + "\" required name=\"amount\"> </div> RM " + value + " </li>");
                            });
                        });
        
                    });
        
                </script>
        -->

    </body>
</html>