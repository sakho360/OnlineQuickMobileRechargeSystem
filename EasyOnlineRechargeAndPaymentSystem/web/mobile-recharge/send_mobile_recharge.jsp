<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>

        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/mobileRechargeImage.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!-- PAGE HEADER -->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!-- SIDE MENU -->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Bangladeshi Mobile&nbsp;Recharge&nbsp;<small>recharge&nbsp;BL,&nbsp;GP,&nbsp;TL,&nbsp;ROBI&nbsp;etc.</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!-- END DASHBOARD -->

                    <!-- return message div -->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 5px;">
                            <% if (PM03CM01 || PM04CM01) {%>
                            <% if (PERMISSION_MR || PERMISSION_MM) {%>
                            <h4>Quick&nbsp;Access</h4>
                            <% if (PM03CM01) {%>
                            <% if (PERMISSION_MR) {%>
                            <a href="MalaysiaMobileRecharge" class="icon-btn">
                                <i class="icon-screen-smartphone"></i>
                                <div>Malaysia MR</div>
                            </a>
                            <a href="SendMobileRecharge" class="icon-btn">
                                <i class="icon-paper-plane"></i>
                                <div>BD MR</div>
                            </a>
                            <a href="MobileRechargeHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MR&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>

                            <% if (PM04CM01) {%>
                            <% if (PERMISSION_MM) {%>
                            <a href="SendMobileMoney" class="icon-btn">
                                <i class="icon-wallet"></i>
                                <div>Send&nbsp;MM</div>
                            </a>
                            <a href="MobileMoneyHistory" class="icon-btn">
                                <i class="icon-list"></i>
                                <div>MM&nbsp;History</div>
                            </a>
                            <% }%>
                            <% }%>
                            <% }%>
                            <% }%>

                            <% if (groupId < 5) {%>
                            <a href="Payments" class="icon-btn">
                                <i class="icon-credit-card"></i>
                                <div>Payments</div>
                            </a>
                            <a href="ShowAllResellers" class="icon-btn">
                                <i class="icon-users"></i>
                                <div>Resellers</div>
                            </a>

                            <% if (groupId == 0) {%>
                            <a href="AddResellerSubAdmin" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 1) {%>
                            <a href="AddReseller4" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 2) {%>
                            <a href="AddReseller3" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 3) {%>
                            <a href="AddReseller2" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% if (groupId == 4) {%>
                            <a href="AddReseller1" class="icon-btn">
                                <i class="icon-user-follow"></i>
                                <div>Add&nbsp;Reseller</div>
                            </a>
                            <% }%>
                            <% }%>
                        </div>
                    </div>
                    <hr style="margin-bottom: 10px; padding-bottom: 0px;"/>
                    <div class="row">
                        <div style="margin-bottom: 0px; padding-bottom: 0px;" class="col-md-12">
                            <h4>
                                <strong>
                                    Your Mobile Recharge Account Balance : 
                                    <s:if test="currentMrBalInfoList !=null">
                                        <s:if test="currentMrBalInfoList.size() !=0">
                                            <s:iterator value="currentMrBalInfoList">
                                                <s:property value="mrCurrentBalance"/>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                    BDT
                                </strong>
                            </h4>
                            <hr/>
                            <!--                            <a style="margin-left: 24%" class="btn green" href="#phone">
                                                            Bangladeshi Mobile Recharge&nbsp;&nbsp;
                                                            <i style="margin-top: 5px;" class="m-icon-swapdown m-icon-white"></i>
                                                        </a>-->

                            <!--                            <a style="margin-left: 50px;" class="btn yellow" href="MalaysiaMobileRecharge">
                                                            Malaysian Mobile Recharge&nbsp;&nbsp;
                                                            <i style="margin-top: 5px;" class="m-icon-swapright m-icon-white"></i>
                                                        </a>-->
                            <!--<hr style="margin-top: 17px; padding-top: 0px; margin-bottom: 15px; padding-bottom: 0px;"/>-->

                            <div style="padding-bottom: 0px; width: 80%; margin-left: 20px;" class="panel operator">
                                <h4>Select Operator</h4>
                                <div class="panelcontent">
                                    <ul>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="grameenphone" required checked="checked">
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/gp_logo.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="airtel">
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/at_logo.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="banglalink">
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/bl_logo.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="robi">
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/robi_logo.png" alt=""/>
                                        </li>

                                        <li>
                                            <div class="selectarea radiogroup">
                                                <input type="radio" id="operator" name="operator" value="teletalk">
                                            </div>
                                            <img src="<%= request.getContextPath()%>/images/tl_logo.png" alt=""/>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div style="padding-top: 0px; margin-top: 0px;" class="portlet light">
                                <div class="portlet-body form">
                                    <form id="mrf" role="form">
                                        <div style="padding-top: 0px; margin-top: 0px;" class="form-body">
                                            <div style="width: 108%" class="panel ammount">
                                                <h4>Select Type</h4>
                                                <div class="panelcontent">
                                                    <ul>
                                                        <li>
                                                            <div class="selectarea radiogroup">
                                                                <input type="radio" id="prepaid" name="type" value="Prepaid" checked="checked">
                                                            </div>
                                                            <div class="ty">
                                                                Prepaid
                                                            </div>
                                                        </li>

                                                        <li>
                                                            <div class="selectarea radiogroup">
                                                                <input type="radio" id="postpaid" name="type" value="Postpaid">
                                                            </div>
                                                            <div class="ty">
                                                                Postpaid
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <h4>Mobile Number</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-arrow-right"></i>
                                                            </span>
                                                            <span id="spanPhone">
                                                                <input type="text" id="phone" name="phone" class="form-control input-lg" style="font-size:20px; color:#C00"> 
                                                            </span>
                                                        </div>
                                                        <span class="help-block"><a href="javascript:void(0);" id="my_contacts">Click Here to Add / Manage Contacts</a></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <h4>Re-type Mobile Number</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-arrow-right"></i>
                                                            </span>
                                                            <input type="text" id="reTypePhone" name="reTypePhone" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <h4>Amount</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group input-small">
                                                            <span class="input-group-addon">
                                                                <i class="icon-credit-card"></i>
                                                            </span>
                                                            <input type="text" id="amount" name="amount" class="form-control input-lg" style="font-size:20px; color:#C00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <h4>PIN</h4>
                                                <div class="panelcontent">
                                                    <div class="form-group has-success mr_input">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="icon-key"></i>
                                                            </span>
                                                            <input type="password" id="pin" name="pin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="padding-left: 0px;" class="panelcontent">
                                                <div class="form-actions">
                                                    <s:if test="pinStatus !=null">
                                                        <s:if test='pinStatus=="N"'>
                                                            <button id="bt" type="button" onclick="sendMRvalidate();" disabled class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Send
                                                            </button>
                                                        </s:if>
                                                        <s:else>
                                                            <button id="bt" type="button" onclick="sendMRvalidate();" class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Send
                                                            </button>
                                                        </s:else>
                                                    </s:if>
                                                    <button type="reset" class="btn default">Cancel</button>
                                                    <img id="createLoadingImage" src="" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div style="padding-top: 0px; margin-top: 0px;" class="portlet light">
                                <div class="portlet-title">
                                    <div style="font-size: 17px; padding-top: 11px; margin-top: 0px;" class="caption">Last&nbsp;20&nbsp;Mobile&nbsp;Recharge&nbsp;Requests</div>
                                    <div class="tools">
                                        <a href="" class="collapse"></a>
                                        <a href="" class="reload"></a>
                                        <a href="" class="remove"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>TrID</th>
                                                    <th>Sent&nbsp;By</th>
                                                    <th>Number</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th>Operator</th>
                                                </tr>
                                            </thead>
                                            <tbody id="lastMRHTable">
                                                <s:if test="lastMRechargeHInfoList !=null">
                                                    <s:if test="lastMRechargeHInfoList.size() !=0">
                                                        <s:iterator value="lastMRechargeHInfoList" var="lastMRHInfo">
                                                            <tr>
                                                                <td>${lastMRHInfo.trid}</td>
                                                                <td>${lastMRHInfo.sender}</td>
                                                                <td>${lastMRHInfo.receiver}</td>
                                                                <td>
                                                                    <s:if test="type==0">
                                                                        Prepaid
                                                                    </s:if>
                                                                    <s:else>
                                                                        <span style="color: #009966;">
                                                                            Postpaid
                                                                        </span>
                                                                    </s:else>
                                                                </td>
                                                                <td>${lastMRHInfo.givenBalance}</td>
                                                                <td>
                                                                    <s:if test="activeStatus=='Y'">
                                                                        <span>
                                                                            Success
                                                                        </span>
                                                                    </s:if>
                                                                    <s:elseif test="activeStatus=='N'">
                                                                        <span>
                                                                            Pending
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='P'">
                                                                        <span>
                                                                            Processing
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='W'">
                                                                        <span>
                                                                            Waiting
                                                                        </span>
                                                                    </s:elseif>
                                                                    <s:elseif test="activeStatus=='F'">
                                                                        <span>
                                                                            Failed
                                                                        </span>
                                                                    </s:elseif>
                                                                </td>
                                                                <td>${lastMRHInfo.operator}</td>
                                                            </tr>
                                                        </s:iterator>
                                                    </s:if>
                                                </s:if>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">My Contacts</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN ALERTS PORTLET -->
                                    <div class="portlet success">
                                        <div class="portlet-body">
                                            <form class="form-horizontal" role="form" id="contact_manage_form">
                                                <div id="contact_resp"></div>
                                                <div class="form-group">
                                                    <label for="inputEmail1" class="col-md-4 control-label">Name</label>
                                                    <div class="col-md-8">
                                                        <input type="text" id="contact_name" name="contact_name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPassword12" class="col-md-4 control-label">Phone Number</label>
                                                    <div class="col-md-8">
                                                        <input type="text" id="contact_number" name="contact_number" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-offset-4 col-md-8">
                                                        <button type="submit" class="btn blue contact_save">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                            <p class="render_my_contact">
                                            <div style="max-height: 230px;" class="table-scrollable">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Serial</th>
                                                            <th>Name</th>
                                                            <th>Phone Number</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tb">

                                                    </tbody>
                                                </table>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END ALERTS PORTLET -->
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn red" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <script src="<%= request.getContextPath()%>/my-js/jqueryLibrary.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery-migrate-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery-ui-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-hover-dropdown.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_005.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_003.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/jquery_004.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-switch.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/contacts.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/sendMRValidation.js" type="text/javascript"></script>

        <script>
                jQuery(document).ready(function () {                                                                     // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
            });

     </script>

    </body>
</html>