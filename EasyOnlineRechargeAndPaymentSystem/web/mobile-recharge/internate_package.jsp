<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <!--FOR LOGO-->
        <link href="<%= request.getContextPath()%>/my-css/newsfeed.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/company.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Internet Package <small> Recharge for Internet Package</small>
                            </h3>
                        </div>
                    </div>

                    <<%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <h3><i class="bd-grameenphone" style="color: #0077c3;"></i> GrameenPhone</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 30 - 75MB </span>
                                    <em> Validity: 7 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 45 - 100MB </span>
                                    <em> Validity: 30 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 275 - 1GB </span>
                                    <em> Validity: 28 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 350 - 2GB </span>
                                    <em> Validity: 28 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 500 - 4GB</span>
                                    <em> Validity: 28 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 950 - 8GB </span>
                                    <em> Validity: 28 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 2000 - 20GB </span>
                                    <em> Validity: 30 days </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 299 - Smart Plan</span>
                                    <em> Volume: 500MB, 200 Minutes (28 days) </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 499 - Smart Plan</span>
                                    <em> Volume: 1GB, 400 Minutes (30 days) </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 699 - Smart Plan </span>
                                    <em> Volume: 2GB, 600 Minutes (30 days) </em>
                                    <em> Valid For: Prepaid, Postpaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="top-news">
                                <a href="javascript:;" class="btn red">
                                    <span> Tk. 250 - Night 2GB </span>
                                    <em> Volume: 2GB internet from 12:00AM to 10:00AM (28 days) </em>
                                    <em> Valid For: Prepaid </em>
                                    <i class="bd-grameenphone top-news-icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <h3><i class="bd-robi" style="color: #e30001;"></i> Robi </h3>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th>Internet (MB)</th>
                                        <th>Validity</th>
                                        <th>USSD&nbsp; <br> Activation <br> Code </th>
                                        <th>Price for USSD (BDT)</th>
                                        <th>Auto renew</th>
                                        <th>Easyload Recharge Amount (BDT)</th>
                                    </tr>
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td>4000</td>
                                        <td rowspan="6">28 Days&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>*8444*4000#</td>
                                        <td>499</td>
                                        <td>Yes</td>
                                        <td>591</td>
                                    </tr>
                                    <tr>
                                        <td>2500</td>
                                        <td>*8444*92#</td>
                                        <td>377</td>
                                        <td>Yes</td>
                                        <td>449</td>
                                    </tr>
                                    <tr>
                                        <td>1500</td>
                                        <td>*8444*85#</td>
                                        <td>275</td>
                                        <td>Yes</td>
                                        <td>316</td>
                                    </tr>
                                    <tr>
                                        <td>750</td>
                                        <td>*8444*500#</td>
                                        <td>175</td>
                                        <td>Yes</td>
                                        <td>201</td>
                                    </tr>
                                    <tr>
                                        <td>250</td>
                                        <td>*8444*250#</td>
                                        <td>75</td>
                                        <td>Yes</td>
                                        <td>n/a</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>*8444*100#</td>
                                        <td>40</td>
                                        <td>Yes</td>
                                        <td>46</td>
                                    </tr>
                                    <tr>
                                        <td>100</td>
                                        <td>10 Days</td>
                                        <td>*8444*10019#</td>
                                        <td>19</td>
                                        <td>Yes</td>
                                        <td>n/a</td>
                                    </tr>
                                    <tr>
                                        <td>150</td>
                                        <td>7 Days</td>
                                        <td>*8444*2007#</td>
                                        <td>24</td>
                                        <td>Yes</td>
                                        <td>n/a</td>
                                    </tr>
                                    <tr>
                                        <td>45</td>
                                        <td rowspan="3">1 Day <br>( 24 hours)</td>
                                        <td>*8444*21#</td>
                                        <td>10</td>
                                        <td>Yes</td>
                                        <td>n/a</td>
                                    </tr>
                                    <tr>
                                        <td>25</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>No</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>*8444*4#</td>
                                        <td>2</td>
                                        <td>Yes</td>
                                        <td>n/a</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3><i class="bd-banglalink" style="color: #f47920;"></i> Banglalink </h3>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td class="head1" style="vertical-align: middle; text-align:center" rowspan="2">SL</td>
                                        <td class="head1" style="vertical-align: middle; text-align:center" rowspan="2">Package Name</td>
                                        <td class="head1" style="vertical-align: middle; text-align:center" colspan="2">USSD Purchase&nbsp;</td>
                                        <td class="head1" style="vertical-align: middle; text-align:center" rowspan="2">SMS Purchase Keywords</td>
                                    </tr>
                                    <tr>
                                        <td class="head1" style="vertical-align: middle; text-align:center">Buy with auto-renewal</td>
                                        <td class="head1" style="vertical-align: middle; text-align:center">Buy without auto-renewal</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">1</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">15GB / 30 days @ Tk. 1,500</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*510#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*410#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 15GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">2</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">8GB / 30 days @ Tk. 900</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*509#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*409#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 8GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">3</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">4GB / 30 days @ Tk. 500</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*508#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*408#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 4GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">4</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">2GB / 30 days @ Tk. 350</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*506#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*406#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 2GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">5</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">1.5GB / 30 days @ Tk. 275</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*511#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*411#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 1.5GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">6</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">1GB / 30 days @ Tk. 210</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*581#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*481#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 1GB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">7</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">600MB / 30 days @ Tk. 150</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*504#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*404#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 600MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">8</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">500MB / 7 days @ Tk. 100</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*582#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*482#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 500MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">9</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">300MB / 30 days @ Tk. 99</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*503#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*403#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 300MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">10</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">250MB / 10 days @ Tk. 75</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*517#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*417#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 250MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">11</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">160MB / 7 days @ Tk. 30</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*501#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*401#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 160MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">12</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">120MB / 30 days @ Tk. 50</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*523#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*423#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 120MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">13</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">100MB / 7 days @ Tk. 20</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*522#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*422#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 100MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">14</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">60MB / 3 days @ Tk. 15</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*502#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*402#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 60MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">15</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">45MB / 1 day @ Tk. 10</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*543#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*443#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 45MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">16</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">32MB / 1 days @ Tk. 9</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*529#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*429#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 32MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">17</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">12MB / 1 day @ Tk. 4</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*520#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*420#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 12MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">18</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">9MB / 1 day @ Tk. 3</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*513#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*413#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 9MB</td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align:center">19</td>
                                        <td style="vertical-align: middle; text-align:left; min-width:300px">4MB / 1 day @ Tk. 1.5</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*518#</td>
                                        <td style="vertical-align: middle; text-align:center">*5000*418#</td>
                                        <td style="vertical-align: middle; text-align:center">Start 4MB</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3><i class="bd-airtel" style="color: #da151c;"></i> Airtel </h3>
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="6" class="text-center">Recharge 3G internet packs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">Type</td>
                                        <td class="text-center">Recharge Amount*</td>
                                        <td class="text-center">Volume</td>
                                        <td class="text-center">Validity</td>
                                        <td class="text-center">Activation</td>
                                        <td class="text-center">Balance check</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" rowspan="8" style="vertical-align: middle;">Prepaid Only</td>
                                        <td class="text-center">17</td>
                                        <td class="text-center">60MB</td>
                                        <td class="text-center">3 days</td>
                                        <td class="text-center">Recharge 17</td>
                                        <td class="text-center" rowspan="8" style="vertical-align: middle;">*778*4#</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">89</td>
                                        <td class="text-center">650MB</td>
                                        <td class="text-center">7 days</td>
                                        <td class="text-center">Recharge 89</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">98</td>
                                        <td class="text-center">425 MB</td>
                                        <td class="text-center">7days</td>
                                        <td class="text-center">Recharge 98</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">151</td>
                                        <td class="text-center">1 GB</td>
                                        <td class="text-center">15 days</td>
                                        <td class="text-center">Recharge 151</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">229</td>
                                        <td class="text-center">1GB</td>
                                        <td class="text-center">30 days</td>
                                        <td class="text-center">Recharge 229</td
                                    </tr>
                                    <tr>
                                        <td class="text-center">398</td>
                                        <td class="text-center">2.5GB</td>
                                        <td class="text-center">30 days</td>
                                        <td class="text-center">Recharge 398</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">517</td>
                                        <td class="text-center">3GB</td>
                                        <td class="text-center">30 days</td>
                                        <td class="text-center">Recharge 517</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">747</td>
                                        <td class="text-center">5GB</td>
                                        <td class="text-center">30 days</td>
                                        <td class="text-center">Recharge 747</td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3><i class="bd-teletalk" style="color: #099a4d;"></i> Teletalk</h3>
                            <table border="1" cellpadding="0" cellspacing="0" class="table">
                                <tbody>
                                    <tr>
                                        <td style="height:34px; width:154px">
                                            <p style="text-align:center">Volume</p>
                                        </td>
                                        <td style="height:34px; width:154px">
                                            <p style="text-align:center">Price*</p>
                                        </td>
                                        <td style="height:34px; width:154px">
                                            <p style="text-align:center">Validity</p>
                                        </td>
                                        <td style="height:34px; width:154px">
                                            <p style="text-align:center">Short code</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">5 MB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">2</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1 day</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D56</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">20 MB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">8</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1 day</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D18</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">80</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">3 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D58</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">160</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">10 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D19</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">260</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D31</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">2 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">350</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D20</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">3 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">425</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D38</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">5 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">600</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D21</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">10 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">825</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D22</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 GB</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">1500</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:20px; width:154px">
                                            <p style="text-align:center">D32</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">1GB</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">99*</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">D60</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">5 GB</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">300*</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">30 days</p>
                                        </td>
                                        <td style="height:21px; width:154px">
                                            <p style="text-align:center">D63</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>

        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>


        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

            });

        </script>

    </body>
</html>