<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Recharge&nbsp;<small>batch&nbsp;top up using csv file</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-6">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet light"><!--box-->

                                <div class="portlet-title">
                                    <div class="caption">
                                        If any Problem in Uploading CSV file
                                        <a href="GroupsList">then Click Here to Use Group Mobile Recharge.</a>
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="#portlet-config" data-toggle="modal" class="config">
                                        </a>
                                        <a href="javascript:;" class="reload">
                                        </a>
                                        <a href="javascript:;" class="remove">
                                        </a>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form id="upFile" action="UploadCsvFile" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
                                        <div class="form-body">
                                            <div class="form-group last">
                                                <label class="col-xs-3 control-label">Choose File</label>
                                                <div class="col-xs-4">
                                                    <input type="file" id="uploadFile" name="uploadFile" style="height: 35px;"><!--class="form-control"-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="col-xs-offset-3 col-xs-9">
                                                        <button id="btnUpFile" type="submit" class="btn purple">
                                                            <i class="icon-check"></i>
                                                            Upload
                                                        </button>
                                                        <button type="reset" class="btn default">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END PORTLET-->
                        </div>

                        <div class="col-xs-6">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box blue-hoki">

                                <div class="portlet-title">
                                    <div class="caption">
                                        CSV Help
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <a href="images/sample.csv" class="btn yellow" >Download Sample CSV File</a>
                                    <br>
                                    <a href="images/sample.csv" class="thumbnail">
                                        <img src="images/csv-help.png" alt="CSV Help" style="height: 477px; display: block;">
                                    </a>
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/jquery_004.js" type="text/javascript"></script>

        <!--<script src="<%= request.getContextPath()%>/my-js/jqueryValidation.js" type="text/javascript"></script>-->

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

                EcommerceOrders.init();
            });
        </script>

        <script type="text/javascript">

//            $("#upFile").validate({
//                rules: {
//                    "uploadFile": {
//                        required: true
//                    }
//                },
//                messages: {
//                    uploadFile: {
//                        required: 'Please upload a csv file!'
//                    }
//                }
//            });

            $("#btnUpFile").on('click', function () {
                if ($('#uploadFile').val() == '') {
                    alert('Please upload a csv file!');
                    return false;
                }
                return true;
            });

            $("#uploadFile").change(function () {

                var extension = this.value.match(/\.([^\.]+)$/)[1];

                var ext = $('#uploadFile').val().split('.').pop().toLowerCase();

                var fileSize = this.files[0];
                var sizeInMb = fileSize.size / 1024;
                var sizeLimit = 1024 * 2;

                switch (ext) {
                    case 'csv':
                        break;
                    default:
                        this.value = '';
                        alert('Please upload a csv file!');
                        return false;
                }

                if (sizeInMb > sizeLimit) {
                    this.value = '';
                    alert('File size must be 2MB');
                    return false;
                }
            });

        </script>

    </body>
</html>