<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="allMRechargeHInfoList !=null">
    <s:if test="allMRechargeHInfoList.size() !=0">
        <s:iterator value="allMRechargeHInfoList" var="allMRHInfo">
            <tr>
                <td><input type="checkbox" id="chkb" name="chkb" value="${allMRHInfo.mobileRechargeId}" class="form-filter"></td>
                <td align="center" class="sorting_1">
                    ${allMRHInfo.trid}
                    <br/>
                    <a href="javascript:void(0);" id="${allMRHInfo.mobileRechargeId}" class="btn fview btn-circle btn-xs blue tooltips" data-container="body" data-placement="top" data-original-title="Show Details">
                        <i class="icon-list"></i>
                    </a>
                </td>
                <td align="center"><s:property value="purchasedOn"/></td>
                <td>${allMRHInfo.sender}</td>
                <td>${allMRHInfo.receiver}</td>
                <td>${allMRHInfo.operator}</td>
                <td>
                    <s:if test="type==0">
                        Prepaid
                    </s:if>
                    <s:else>
                        <span style="color: #009966;">
                            Postpaid
                        </span>
                    </s:else>
                </td>
                <td class="amount">${allMRHInfo.givenBalance}</td>
                <td align="center">
                    <s:if test="activeStatus=='W'">
                        <span>
                            Waiting
                        </span>
                    </s:if>
                    <s:elseif test="activeStatus=='S'">
                        <span>
                            Sent
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='Y'">
                        <span>
                            Success
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='R'">
                        <span>
                            Refunded
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='P'">
                        <span>
                            Processing
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='F'">
                        <span>
                            Failed
                        </span>
                    </s:elseif>
                </td>
                <td>${allMRHInfo.tnxid}</td>
                <td>
                    <s:if test='originator=="1"'>
                        Modem
                    </s:if>
                    <s:else>
                        Web
                    </s:else>
                </td>
                <td>${allMRHInfo.operatorBalance}</td>
                <td>
                    <input type="hidden" id="rID" value="<s:property value="sender"/>"/>
                    <a href="javascript:void(0);" id="<s:property value="mobileRechargeId"/>" class="fview btn btn-xs blue btn-editable">
                        <i class="icon-list"></i>
                        View
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>