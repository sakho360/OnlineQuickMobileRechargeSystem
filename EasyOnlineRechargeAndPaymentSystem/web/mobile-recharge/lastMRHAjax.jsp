<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<% int sId = 0; %>
<s:if test="lastMRechargeHInfoList !=null">
    <s:if test="lastMRechargeHInfoList.size() !=0">
        <s:iterator value="lastMRechargeHInfoList" var="lastMRHInfo">
            <% sId++;%>
            <tr>
                <td><%= sId%></td>
                <td>${lastMRHInfo.mobileRechargeId}</td>
                <td>${lastMRHInfo.sender}</td>
                <td>${lastMRHInfo.receiver}</td>
                <td>
                    <s:if test="type==0">
                        Pepaid
                    </s:if>
                    <s:else>
                        <span style="color: #009966;">
                            Postpaid
                        </span>
                    </s:else>
                </td>
                <td>${lastMRHInfo.givenBalance}</td>
                <td>
                    <s:if test="activeStatus=='Y'">
                        <span>
                            Success
                        </span>
                    </s:if>
                    <s:elseif test="activeStatus=='N'">
                        <span>
                            Pending
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='P'">
                        <span>
                            Processing
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='W'">
                        <span>
                            Waiting
                        </span>
                    </s:elseif>
                    <s:elseif test="activeStatus=='F'">
                        <span>
                            Failed
                        </span>
                    </s:elseif>
                </td>
                <td>${lastMRHInfo.trid}</td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>