<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png"  rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/componentsModal.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            table {
                counter-reset:section;
            }
            .count:before {
                counter-increment:section;
                content:counter(section);
            }
        </style>

        <script type="text/javascript">

            function showGroup() {

                $.ajax({
                    type: "POST",
                    url: 'ShowGroup',
                    success: function (data) {
                        $("#glTable").html(data);
                    }
                });
            }
            //            
            function mrGroupDetailsView(mrGNameId) {

                var dataString = 'mrGNameId=' + mrGNameId;

                $.ajax({
                    type: "POST",
                    url: 'MRGroupDetailsView',
                    data: dataString,
                    success: function (data) {
                        if (data.trim() == 1) {
                            window.location = 'ViewEditMRGroup?mrGNameId=' + mrGNameId;
                        } else {
                            $('#message').html(data);
                        }
                    }
                });
            }
            //            
            function deleteMRGroup(mrGNameId) {

                var message = confirm("Do you want to delete group ?");

                if (message == true) {

                    var dataString = 'mrGNameId=' + mrGNameId;

                    document.getElementById('createLoadingImage').src = 'images/loading.gif';

                    $.ajax({
                        type: "POST",
                        url: 'DeleteMRGroup',
                        data: dataString,
                        success: function (data) {
                            $("#message").html(data);
                            document.getElementById('createLoadingImage').src = '';
                            showGroup();
                        }
                    });
                }
            }
            //
            function sendTopUp(mrGNameId) {

                var dataString = 'mrGNameId=' + mrGNameId;

                $.ajax({
                    type: "POST",
                    url: 'MRGroupDetailsView',
                    data: dataString,
                    success: function (data) {
                        if (data.trim() == 1) {
                            window.location = 'SendMRByGroupPage?mrGNameId=' + mrGNameId;
                        } else {
                            $('#message').html(data);
                        }
                    }
                });
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Recharge&nbsp;<small>View&nbsp;Edit&nbsp;Group&nbsp;Mobile&nbsp;Recharge</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box green" id="ibox_form">
                                <div class="portlet-body">
                                    <form method="post" id="ib_form">
                                        <s:if test="singleMRGroupInfoList !=null">
                                            <s:if test="singleMRGroupInfoList.size() !=0">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Group&nbsp;Name</label>
                                                    <s:iterator value="singleMRGroupInfoList" status="gn">
                                                        <s:if test="#gn.first==true">
                                                            <input type="text" id="groupName" name="groupName" value="<s:property value="mrGroupName"/>" class="form-control">
                                                        </s:if>
                                                    </s:iterator>
                                                </div>
                                                <div class="table-scrollable">
                                                    <table class="table table-condensed table-hover invoice-table" id="invoice_items">
                                                        <thead>
                                                            <tr>
                                                                <th>Serial</th>
                                                                <th>Phone*<br>[e.g. 01XXXXXXXXX ]</th>
                                                                <th>Type*<br>[Prepaid = 0 &amp; Postpaid = 1]</th>
                                                                <th>Amount*</th>
                                                                <th>Modem*</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="groupTBody">
                                                            <s:iterator value="singleMRGroupInfoList">
                                                                <tr>
                                                                    <td class="count">

                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egPhone" name="egPhone" class="form-control input-medium item_name" value="<s:property value="mrGroupInfo.mobileNumber"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egType" name="egType" class="form-control input-small" value="<s:property value="mrGroupInfo.type"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egAmount" name="egAmount" class="form-control input-small" value="<s:property value="mrGroupInfo.amount"/>">
                                                                    </td>
                                                                    <td>
                                                                        <input type="text" id="egModem" name="egModem" class="form-control input-small" value="<s:property value="mrGroupInfo.modemNumber"/>">
                                                                    </td>
                                                                </tr>
                                                            </s:iterator>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <button type="button" class="btn green" id="blank-add">
                                                    Add&nbsp;Number&nbsp;Row
                                                </button>
                                                <button type="button" class="btn btn-danger" id="item-remove">
                                                    <i class="fa fa-minus-circle"></i>
                                                    Delete
                                                </button>
                                                <hr>

                                                <input type="hidden" id="mrGNameId" name="mrGNameId" value="<s:property value="mrGNameId"/>">
                                                <button type="button" id="submit" class="btn btn-success">
                                                    Edit&nbsp;Group
                                                </button>
                                                <img id="createLoadingImage" src="" alt="" />
                                                <hr>
                                            </s:if>
                                        </s:if>
                                    </form>

                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <i class="icon-list"></i>
                                                    Available&nbsp;Groups
                                                </th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody id="glTable">
                                            <s:if test="mrGroupInfoList !=null">
                                                <s:if test="mrGroupInfoList.size() !=0">
                                                    <s:iterator value="mrGroupInfoList">
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="success"></div>
                                                                <a href="javascript:void(0);" onclick="mrGroupDetailsView('<s:property value="mrGNameId"/>');">
                                                                    <s:property value="mrGroupName"/>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" onclick="sendTopUp('<s:property value="mrGNameId"/>');" class="btn btn-xs purple-medium">
                                                                    <i class="icon-paper-plane"></i>
                                                                    Mobile Recharge
                                                                </a>
                                                                <a href="javascript:void(0);" onclick="mrGroupDetailsView('<s:property value="mrGNameId"/>');" class="btn btn-primary btn-xs purple">
                                                                    <i class="icon-pencil"></i>
                                                                    View&nbsp;/&nbsp;Edit
                                                                </a>
                                                                <a href="javascript:void(0);" onclick="deleteMRGroup('<s:property value="mrGNameId"/>');" class="btn btn-danger btn-xs purple">
                                                                    <i class="icon-trash"></i>
                                                                    Delete
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END CONDENSED TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>


        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/mrGroupsEdit.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                    jQuery(document).ready(function () {
                                                                        // initiate layout and plugins
                                                                        Metronic.init(); // init metronic core components
                                                                        Layout.init(); // init current layout
                                                                        QuickSidebar.init() // init quick sidebar
                                                                        //        UIIdleTimeout.init();
                                                                    });
        </script>

    </body>
</html>