<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png"  rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/componentsModal.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            table {
                counter-reset:section;
            }
            .count:before {
                counter-increment:section;
                content:counter(section);
            }
        </style>

        <script type="text/javascript">

            function showGroup() {

                $.ajax({
                    type: "POST",
                    url: 'ShowGroup',
                    success: function (data) {
                        $("#glTable").html(data);
                    }
                });
            }
            //
            function createGroup() {

                var dataString = "";

                var tabel = document.getElementById('invoice_items');

                var row = tabel.rows.length;

                for (i = 1; i < row; i++) {

                    var inputs = tabel.rows.item(i).getElementsByTagName("input");

                    var inputsLength = inputs.length;

                    for (var j = 0; j < inputsLength; j++) {
                        dataString += inputs[j].value + '/fd/';
                    }
                    dataString += '/rd/';
                }

                var gName = ($("#groupName").val());

                if ((notEmpty(gName, "Group name can't be Empty")) && (forGroupName(gName, "Invalid group name")) && (lengthRestriction(gName, 4, 15, "group name"))) {

                    if (dataString.length != 0) {

                        var message = confirm("Do you want to create new group ?");

                        if (message == true) {

                            var form = $('#ib_form');

                            var param = "groupName=" + gName + "&groupFormString=" + dataString;

                            document.getElementById('createLoadingImage').src = 'images/loading.gif';

                            $.ajax({
                                type: "POST",
                                url: 'SaveGroup',
                                data: param,
                                success: function (data) {
                                    document.getElementById('createLoadingImage').src = '';
                                    $("#message").html(data);
                                    form.trigger('reset');
                                    showGroup();
                                }
                            });
                        }
                    } else {
                        alert('Blank field not allow');
                    }
                }
            }
            //            
            function mrGroupDetailsView(mrGNameId) {

                var dataString = 'mrGNameId=' + mrGNameId;

                $.ajax({
                    type: "POST",
                    url: 'MRGroupDetailsView',
                    data: dataString,
                    success: function (data) {
                        if (data.trim() == 1) {
                            window.location = 'ViewEditMRGroup?mrGNameId=' + mrGNameId;
                        } else {
                            $('#message').html(data);
                        }
                    }
                });
            }
            //            
            function deleteMRGroup(mrGNameId) {

                var message = confirm("Do you want to delete group ?");

                if (message == true) {

                    var dataString = 'mrGNameId=' + mrGNameId;

                    document.getElementById('createLoadingImage').src = 'images/loading.gif';

                    $.ajax({
                        type: "POST",
                        url: 'DeleteMRGroup',
                        data: dataString,
                        success: function (data) {
                            $("#message").html(data);
                            document.getElementById('createLoadingImage').src = '';
                            showGroup();
                        }
                    });
                }
            }
            //            
            function sendTopUp(mrGNameId) {

                var dataString = 'mrGNameId=' + mrGNameId;

                $.ajax({
                    type: "POST",
                    url: 'MRGroupDetailsView',
                    data: dataString,
                    success: function (data) {
                        if (data.trim() == 1) {
                            window.location = 'SendMRByGroupPage?mrGNameId=' + mrGNameId;
                        } else {
                            $('#message').html(data);
                        }
                    }
                });
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function forGroupName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_ ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Mobile&nbsp;Recharge&nbsp;<small>Group&nbsp;Mobile&nbsp;Recharge</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box green" id="ibox_form">
                                <div class="portlet-body">
                                    <form method="post" id="ib_form">
                                        <div class="form-group has-success">
                                            <label class="control-label">Group&nbsp;Name</label>
                                            <input type="text" id="groupName" name="groupName" class="form-control">
                                        </div>
                                        <div class="table-scrollable">
                                            <table class="table table-condensed table-hover invoice-table" id="invoice_items">
                                                <thead>
                                                    <tr>
                                                        <th>Serial</th>
                                                        <th>Phone*<br>[e.g. 01XXXXXXXXX ]</th>
                                                        <th>Type*<br>[Prepaid = 0 &amp; Postpaid = 1]</th>
                                                        <th>Amount*</th>
                                                        <th>Modem*</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="groupTBody">
                                                    <tr>
                                                        <td class="count">

                                                        </td>
                                                        <td>
                                                            <input type="text" id="phone" name="phone" class="form-control input-medium item_name" value="">
                                                        </td>
                                                        <td>
                                                            <input type="text" id="type" name="type" class="form-control input-small" value="">
                                                        </td>
                                                        <td>
                                                            <input type="text" id="amount" name="amount" class="form-control input-small" value="">
                                                        </td>
                                                        <td>
                                                            <input type="text" id="modem" name="modem" class="form-control input-small" value="1">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <button type="button" class="btn green" id="blank-add">
                                            Add&nbsp;Number&nbsp;Row
                                        </button>
                                        <button type="button" class="btn btn-danger" id="item-remove">
                                            <i class="fa fa-minus-circle"></i>
                                            Delete
                                        </button>
                                        <hr>
                                        <button type="button" id="submitButton" onclick="createGroup();" class="btn btn-success">
                                            Save&nbsp;Group
                                        </button>
                                        <img id="createLoadingImage" src="" alt="" />
                                        <hr>
                                    </form>

                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <i class="icon-list"></i>
                                                    Available&nbsp;Groups
                                                </th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody id="glTable">
                                            <s:if test="mrGroupInfoList !=null">
                                                <s:if test="mrGroupInfoList.size() !=0">
                                                    <s:iterator value="mrGroupInfoList">
                                                        <tr>
                                                            <td class="highlight">
                                                                <div class="success"></div>
                                                                <a href="javascript:void(0);" onclick="mrGroupDetailsView('<s:property value="mrGNameId"/>');">
                                                                    <s:property value="mrGroupName"/>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" onclick="sendTopUp('<s:property value="mrGNameId"/>');" class="btn btn-xs purple-medium">
                                                                    <i class="icon-paper-plane"></i>
                                                                    Mobile Recharge
                                                                </a>
                                                                <a href="javascript:void(0);" onclick="mrGroupDetailsView('<s:property value="mrGNameId"/>');" class="btn btn-primary btn-xs purple">
                                                                    <i class="icon-pencil"></i>
                                                                    View&nbsp;/&nbsp;Edit
                                                                </a>
                                                                <a href="javascript:void(0);" onclick="deleteMRGroup('<s:property value="mrGNameId"/>');" class="btn btn-danger btn-xs purple">
                                                                    <i class="icon-trash"></i>
                                                                    Delete
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END CONDENSED TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>


        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/mrGroupsSave.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                    jQuery(document).ready(function () {
                                                                        // initiate layout and plugins
                                                                        Metronic.init(); // init metronic core components
                                                                        Layout.init(); // init current layout
                                                                        QuickSidebar.init() // init quick sidebar
                                                                        //        UIIdleTimeout.init();
                                                                    });
        </script>

    </body>
</html>