<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleResallerInfoList !=null">
    <s:if test="singleResallerInfoList.size() !=0">
        <s:iterator value="singleResallerInfoList">
            <s:if test="suspendActivity=='Y'">
                <a href="javascript:void(0);" onclick="suspendReseller('<s:property value="userId"/>');" class="btn btn yellow">Suspend</a>
            </s:if>
            <s:else>
                <a href="javascript:void(0);" onclick="unSuspendReseller('<s:property value="userId"/>');" class="btn btn green">Unsuspend</a>
            </s:else>
        </s:iterator>
    </s:if>
</s:if>