<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function viewResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "ViewResellerButtonClicked?resellerId=" + dataString;
            }

            function editResellerButtonClicked(resellerId) {

                var dataString = resellerId;

                window.location = "EditResellerButtonClicked?resellerId=" + dataString;
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Resellers <small>show resellers</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption font-green-sharp">
                                        <span class="caption-subject bold uppercase">Resellers</span>
                                        <span class="caption-helper">Show Resellers...</span>
                                    </div>

                                    <div class="actions">
                                        <a href="AddResellerSubAdmin" class="btn btn-circle blue btn-sm"><i class="icon-plus"></i> Add Reseller </a>
                                        <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen"></a>
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div style="padding-top: 16px;" class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                                <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="20%">Reseller&nbsp;ID</th>
                                                        <th width="17%">Full&nbsp;Name</th>
                                                        <th width="8%">Phone</th>
                                                        <th width="8%">MR&nbsp;Balance</th>
                                                        <th width="8%">MM&nbsp;Balance</th>
                                                        <th width="7%">Type</th>
                                                        <th width="7%"> tatus</th>
                                                        <th width="5%">OTP</th>
                                                        <th width="18%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <s:if test="resallersSubAdminInfoList !=null">
                                                        <s:if test="resallersSubAdminInfoList.size() !=0">
                                                            <s:iterator value="resallersSubAdminInfoList">
                                                                <tr>
                                                                    <td>
                                                                        <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');">
                                                                            <s:property value="userId"/>
                                                                        </a>
                                                                    </td>
                                                                    <td><s:property value="userName"/></td>
                                                                    <td>
                                                                        <s:if test="contactNumber1 !=null">
                                                                            <s:property value="contactNumber1"/>
                                                                        </s:if>
                                                                        <s:else>
                                                                            No Phone Number
                                                                        </s:else>
                                                                    </td>
                                                                    <td>
                                                                        <strong>
                                                                            <s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/>
                                                                        </strong>
                                                                        <br/>
                                                                        <% if (PM03CM01 && PERMISSION_MR) {%>
                                                                        <a href="Payments?resellerId=<s:property value="userId"/>" class="btn btn-xs green-meadow">
                                                                            <i class="icon-plus"></i>
                                                                            Add Fund
                                                                        </a>
                                                                        <br/>
                                                                        <br/>
                                                                        <a href="MRPaymentsMade" class="btn btn-xs green-meadow">
                                                                            <i class="icon-paper-plane"></i>
                                                                            History
                                                                        </a>
                                                                        <% }%>
                                                                    </td>
                                                                    <td>
                                                                        <strong>
                                                                            <s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/>
                                                                        </strong>
                                                                        <br/>
                                                                        <% if (PM04CM01 && PERMISSION_MM) {%>
                                                                        <a href="Payments?resellerId=<s:property value="userId"/>&loadType=<s:property value="serviceMMoney"/>" class="btn btn-xs purple-medium">
                                                                            <i class="icon-plus"></i>
                                                                            Add Fund
                                                                        </a>
                                                                        <br/>
                                                                        <br/>
                                                                        <a href="MMPaymentsMade" class="btn btn-xs purple-medium">
                                                                            <i class="icon-wallet"></i>
                                                                            History
                                                                        </a>
                                                                        <% }%>
                                                                    </td>
                                                                    <td><s:property value="userGroupInfo.groupName"/></td>
                                                                    <td align="center">
                                                                        <s:if test="suspendActivity=='Y'">Active</s:if>
                                                                        <s:else>
                                                                            <p style="color: white;background: red;">Inactive</p>
                                                                        </s:else>
                                                                    </td>
                                                                    <td align="center">
                                                                        <s:if test="otpStatus=='Y'">Enable</s:if>
                                                                        <s:else>
                                                                            <p style="color: white;background: red;">Disable</p>
                                                                        </s:else>
                                                                    </td>
                                                                    <td>
                                                                        <a href="javascript:void(0);" onclick="viewResellerButtonClicked('<s:property value="userId"/>');" class="btn blue btn-xs ">
                                                                            <i class="icon-list"></i>
                                                                            View
                                                                        </a>
                                                                        <br>
                                                                        <br>
                                                                        <a href="javascript:void(0);" onclick="editResellerButtonClicked('<s:property value="userId"/>');" class="btn btn-xs red btn-editable">
                                                                            <i class="icon-note"></i>
                                                                            Edit
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/c_002.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>

        <script type="text/javascript">
                                                                            $(document).ready(function () {
                                                                                $('#datatable_orders').DataTable({
                                                                                    "orderCellsTop": true,
                                                                                    "pagingType": "full_numbers"
                                                                                });
                                                                            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout

                UIIdleTimeout.init();
                $("#clock").clock();

                EcommerceOrders.init("sub-admin");
            });
        </script>
    </body>
</html>