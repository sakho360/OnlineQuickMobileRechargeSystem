<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="allManageRateInfoList !=null">
    <s:if test="allManageRateInfoList.size() !=0">
        <s:iterator value="allManageRateInfoList">
            <tr>
                <td>
                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>">
                        <s:property value="rateName"/>
                    </a>
                </td>
                <td>
                    <a href="ManageRateDetailsView?manageRateId=<s:property value="manageRateId"/>" class="btn btn-primary btn-xs purple">
                        <i class="icon-pencil"></i>
                        View&nbsp;/&nbsp;Edit
                    </a>

                    <a href="javascript:void(0);" id="<s:property value="manageRateId"/>" class="btn btn-danger btn-xs purple btn_delete">
                        <i class="icon-trash"></i>
                        Delete
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>