<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function createNewReseller() {

                var userGroupId = $("#userLevelList").val();

                var mobileRecharge = document.getElementById("mRecharge");

                var mobileMoney = document.getElementById("mMoney");

                var resellerId = document.getElementById("userId").value;

                var password = document.getElementById("password").value;

                var fullName = document.getElementById("fullName").value;

                var email = document.getElementById("email").value;

                var phone = document.getElementById("phone").value;

                var manageRateId = $("#rateList").val();

                var otpStatus = document.getElementById("otpY");

                var pinEnabledStatus = document.getElementById('pinEnabledY');
                var loadPin = document.getElementById("loadPin").value;

                var apiStatus = document.getElementById("apiY");

                var sendEmailStatus = document.getElementById("sendEmailY");

                var pinConfirmStatus = '';

                var currentUPin = document.getElementById("upin").value;
                //
                if (!mobileRecharge.checked) {
                    mobileRecharge = 'N';
                    flag = true;
                } else {
                    mobileRecharge = document.getElementById("mRecharge").value;
                    flag = true;
                }
                //
                if (!mobileMoney.checked) {
                    mobileMoney = 'N';
                    flag = true;
                } else {
                    mobileMoney = document.getElementById("mMoney").value;
                    flag = true;
                }
                //
                if ((!notEmpty(resellerId, "User id can't be empty !")) & (!forUserId(resellerId, "Invalid User ID Format !")) & (!lengthRestriction(resellerId, 6, 18, "your user id"))) {
                    return false;
                }
                //
                if ((!notEmpty(password, "Password can't be empty !")) & (!isAlphanumeric(password, "Invalid password Format !")) & (!lengthRestriction(password, 6, 18, "your password"))) {
                    return false;
                }
                //
                if ((!notEmpty(fullName, "Full Name can't be empty !")) & (!forFullName(fullName, "Invalid fullName Format !")) & (!lengthRestriction(fullName, 3, 20, "your fullName"))) {
                    return false;
                }
                //
                if (email == "") {
                    email = '';
                    flag = true;
                } else if (!emailValidator(email, "Invalid Email Format !")) {
                    return false;
                }
                //
                if (phone == "") {
                    phone = '';
                    flag = true;
                } else if (!forPhone(phone)) {
                    return false;
                }
                //
                if (otpStatus.checked) {
                    otpStatus = document.getElementById("otpY").value;
                    flag = true;
                } else {
                    otpStatus = document.getElementById("otpN").value;
                    flag = true;
                }
                //
                if (pinEnabledStatus.checked) {
                    if ((!notEmpty(loadPin, "Pin can't be empty !")) & (!isAlphanumeric(loadPin, "Invalid Pin Format !")) & (!lengthRestriction(loadPin, 6, 10, "your pin"))) {
                        return false;
                    }
                    pinConfirmStatus = loadPin;
                    pinEnabledStatus = document.getElementById('pinEnabledY').value;
                } else {
                    pinConfirmStatus = '';
                    pinEnabledStatus = document.getElementById('pinEnabledN').value;
                    flag = true;
                }
                //
                if (apiStatus.checked) {
                    apiStatus = document.getElementById("apiY").value;
                    flag = true;
                } else {
                    apiStatus = document.getElementById("apiN").value;
                    flag = true;
                }
                //
                if (sendEmailStatus.checked) {
                    sendEmailStatus = document.getElementById("sendEmailY").value;
                    flag = true;
                } else {
                    sendEmailStatus = document.getElementById("sendEmailN").value;
                    flag = true;
                }
                //
                var message = confirm("Do you want to create New Reseller ?");

                if (message == true) {

                    var flag = true;

                    if (flag) {

                        var dataString = 'userLevelList=' + userGroupId;
                        dataString += '&mobileRecharge=' + mobileRecharge;
                        dataString += '&mobileMoney=' + mobileMoney;
                        dataString += '&resellerId=' + resellerId;
                        dataString += '&password=' + password;
                        dataString += '&fullName=' + fullName;
                        dataString += '&email=' + email;
                        dataString += '&phone=' + phone;
                        dataString += '&rateList=' + manageRateId;
                        dataString += '&otpStatus=' + otpStatus;
                        dataString += '&pinEnabledStatus=' + pinEnabledStatus;
                        dataString += '&apiStatus=' + apiStatus;
                        dataString += '&sendEmailStatus=' + sendEmailStatus;
                        dataString += '&pinConfirmStatus=' + pinConfirmStatus;
                        dataString += '&currentUPin=' + currentUPin;
                        //
                        document.getElementById('createLoadingImage').src = 'images/loading.gif';
                        //
                        $.ajax({
                            type: "POST",
                            url: 'AddNewReseller',
                            data: dataString,
                            success: function (data) {

                                if (data.trim() == 1) {
                                    window.location = "ConfirmAddNewReseller?resellerId=" + resellerId;
                                    document.getElementById('createLoadingImage').src = '';
                                } else {
                                    document.getElementById('createLoadingImage').src = '';
                                    $("#message").html(data);
                                }
                            }
                        });
                    }
                }
            }
            //            
            function forUserId(id, helperMsg) {
                var alphaExp = /^[0-9a-z_]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function forFullName(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z_. ]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //            
            function isAlphanumeric(id, helperMsg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function emailValidator(id, helperMsg) {

                var emailExp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (id.match(emailExp)) {
                    return true;
                } else {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
            }
            //
            function notEmpty(id, helperMsg) {
                if (id.length == 0) {
                    alert(helperMsg);
                    id.focus();
                    return false;
                }
                return true;
            }
            //
            function lengthRestriction(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    alert("Please enter " + msg + " between " + min + " to " + max + " characters");
                    id.focus();
                    return false;
                }
            }
            //
            function phoneTest(a) {
                var b = /^(\+88|88|)(\d{11})$/;
                return b.test(a)
            }
            //
            function forPhone(a) {

                var d = a;

                var h = phoneTest(d);

                if (h == true) {

                    if (d.length > 11) {
                        d = d.substring(d.length - 11);
                    }

                    var i = d.substring(0, 3);

                    if (i == "019" || i == "018" || i == "017" || i == "016" || i == "015") {
                        return true;
                    } else {
                        alert("Invalid Mobile No");
                    }
                } else {
                    alert("Invalid Mobile No");
                }
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Reseller&nbsp;<small>Add&nbsp;New&nbsp;Reseller</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <s:if test="pinStatus !=null">
                        <s:if test='pinStatus=="N"'>
                            <div class="alert alert-danger fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                Warning !
                                <br/>
                                Your Transaction Pin is Disabled Please Enable Your Transaction Pin &nbsp;&nbsp;<a style="font-size: 14px; text-decoration: none;" href="EnablePin">Enable Pin ?</a>
                            </div>
                        </s:if>
                    </s:if>

                    <div class="row">
                        <div class=" col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">

                                    <div class="errorMessage" style="margin-bottom: 5px;">
                                        <s:actionerror/>
                                        <span id="nameloc"></span>
                                        <br/>
                                        <span id="passwordloc"></span>
                                    </div>

                                    <div class="caption">
                                        Add&nbsp;New&nbsp;Reseller&nbsp;4
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="javascript:;" class="remove">
                                        </a>
                                    </div>
                                </div>

                                <div class="portlet-body form" id="pwd-container">

                                    <!-- BEGIN FORM-->
                                    <form id="" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label" for="rtype">Type*</label>
                                                <div class=" col-md-4">
                                                    <select id="userLevelList" name="userLevelList" class="form-control">
                                                        <s:if test="userGroupInfoList !=null">
                                                            <s:if test="userGroupInfoList.size() !=0">
                                                                <s:iterator value="userGroupInfoList">
                                                                    <option <s:if test="groupId==2"> selected </s:if> value="<s:property value="groupId"/>"><s:property value="groupName"/></option>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Services</label>
                                                <div class=" col-md-7">
                                                    <div class="checkbox-list">

                                                        <% if (PM03CM01 && PERMISSION_MR) {%>
                                                        <label>
                                                            <input type="checkbox" id="mRecharge" name="mRecharge" value="MR" checked>
                                                            Mobile&nbsp;Recharge
                                                        </label>
                                                        <% }%>

                                                        <% if (PM04CM01 && PERMISSION_MM) {%>
                                                        <label>
                                                            <input type="checkbox" id="mMoney" name="mMoney" value="MM" checked>
                                                            Mobile&nbsp;Money
                                                        </label>
                                                        <% }%>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">User&nbsp;ID*</label>
                                                <div class=" col-md-4">

                                                    <input type="text" id="userId" name="userId" class="form-control">

                                                    <span class="help-block"> Enter username 5 to 12 character </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Password*</label>
                                                <div class=" col-md-4">
                                                    <div class="input-group">
                                                        <div class="input-icon">
                                                            <i class="icon-lock"></i>

                                                            <input type="text" id="password" name="password" autocomplete="off" class="form-control pws" value="aK4xMUyO"/>

                                                        </div>
                                                        <span class="input-group-btn">
                                                            <button id="genpassword" class="btn btn-success" type="button">
                                                                <i class="icon-shuffle"></i>
                                                                Random
                                                            </button>
                                                        </span>
                                                    </div>
                                                    <span class="help-block"> For better security, Please use Strong password. </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label"></label>
                                                <div class=" col-md-4">
                                                    <div class="pwstrength_viewport_progress"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">User&nbsp;Fullname*</label>
                                                <div class=" col-md-4">

                                                    <input type="text" id="fullName" name="fullName" class="form-control">


                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Email&nbsp;Address</label>
                                                <div class=" col-md-4">

                                                    <input type="text" id="email" name="email" class="form-control">

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Phone&nbsp;Number</label>
                                                <div class=" col-md-4">

                                                    <input type="text" id="phone" name="phone" class="form-control">

                                                    <span class="help-block">Enter phone number with country code </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Rate&nbsp;Plan</label>
                                                <div class=" col-md-4">
                                                    <select id="rateList" name="rateList" class="form-control">
                                                        <option selected value="0">Default</option>
                                                        <s:if test="allManageRateInfoList !=null">
                                                            <s:if test="allManageRateInfoList.size() !=0">
                                                                <s:iterator value="allManageRateInfoList">
                                                                    <option value="<s:property value="manageRateId"/>"><s:property value="rateName"/></option>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                    <span class="help-block"><a href="ManageRates">Manage&nbsp;Rates</a> </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Enable&nbsp;OTP&nbsp;?</label>
                                                <div class=" col-md-4">
                                                    <div class="radio-list">

                                                        <label class="radio-inline"><input type="radio" id="otpY" name="otp" value="Y">&nbsp;Yes</label>
                                                        <label class="radio-inline"><input type="radio" id="otpN" name="otp" value="N" checked>&nbsp;No</label>

                                                    </div>
                                                    <span class="help-block"> Enable OTP for better security, You must use a valid email address for OTP </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Enable&nbsp;PIN&nbsp;?</label>
                                                <div class="col-xs-4">
                                                    <div class="radio-list">

                                                        <label class="radio-inline"><input type="radio" id="pinEnabledY" name="pinEnabled" value="Y">&nbsp;Yes</label>
                                                        <label class="radio-inline"><input type="radio" id="pinEnabledN" name="pinEnabled" value="N" checked>&nbsp;No</label>
                                                        <input type="hidden" id="ipin" value="Yes">

                                                    </div>
                                                    <span class="help-block"> You can enable transactional PIN. If enabled PIN will be required for transactions </span>
                                                </div>
                                            </div>
                                            <div class="form-group" id="pin">
                                                <label class=" col-md-3 control-label">PIN</label>
                                                <div class=" col-md-4">

                                                    <input type="text" id="loadPin"  name="loadPin" autocomplete="off" class="form-control">

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label">Enable&nbsp;API&nbsp;Access&nbsp;?</label>
                                                <div class=" col-md-4">
                                                    <div class="radio-list">

                                                        <label class="radio-inline"><input type="radio" id="apiY" name="api" value="Y">&nbsp;Yes</label>
                                                        <label class="radio-inline"><input type="radio" id="apiN" name="api" value="N" checked>&nbsp;No</label>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class=" col-md-3 control-label">Send&nbsp;Email&nbsp;Confirmation&nbsp;?</label>
                                                <div class=" col-md-4">
                                                    <div class="radio-list">

                                                        <label class="radio-inline"><input type="radio" id="sendEmailY" name="sendEmail" value="Y">&nbsp;Yes</label>
                                                        <label class="radio-inline"><input type="radio" id="sendEmailN" name="sendEmail" value="N" checked> &nbsp;No</label>

                                                    </div>
                                                    <span class="help-block"> Send an Email with Login Details for this users </span>
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="form-group last">
                                                <label for="upin" class="col-xs-3 control-label">Your&nbsp;PIN&nbsp;to&nbsp;Confirm:&nbsp;</label>
                                                <div class="col-xs-4">
                                                    <input type="password" id="upin" name="upin" class="form-control input-lg" autocomplete="off" style="font-size:20px; color:#C00">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class=" col-md-offset-3 col-md-9">

                                                <!--<a class="btn blue" href="javascript:void(0);" onclick="createNewReseller();" >Submit</a>-->

                                                <s:if test="pinStatus !=null">
                                                    <s:if test='pinStatus=="N"'>
                                                        <button type="button" onclick="createNewReseller();" disabled class="btn blue">
                                                            <i class="icon-check"></i>
                                                            Submit
                                                        </button>
                                                    </s:if>
                                                    <s:else>
                                                        <button type="button" onclick="createNewReseller();" class="btn blue">
                                                            <i class="icon-check"></i>
                                                            Submit
                                                        </button>
                                                    </s:else>
                                                </s:if>
                                                <button type="reset" class="btn default">Cancel</button>
                                                <img id="createLoadingImage" src="" alt="" />
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>


        <!--FOR PASSWORD-->
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/passwordStrength.js"></script>
        <script src="<%= request.getContextPath()%>/my-js/checkpw.js"></script>

        <script>
                                                            function randString(n)
                                                            {
                                                                if (!n)
                                                                {
                                                                    n = 10;
                                                                }

                                                                var text = '';
                                                                var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

                                                                for (var i = 0; i < n; i++)
                                                                {
                                                                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                                                                }

                                                                return text;
                                                            }

        </script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

                $("#pin").hide();
                $("input[name$='pinEnabled']").click(function () {
                    var pin_enabled = $(this).val();
                    if (pin_enabled == "Y") {
                        $("#pin").show();
                    }
                    else {
                        $("#pin").hide();
                    }
                });

                $("#genpassword").click(function () {
                    var gpass = randString(10);
                    $("#password").val(gpass);
                });
            }
            );

        </script>

    </body>
</html>