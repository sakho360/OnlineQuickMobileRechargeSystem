<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/menuControl.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <script type="text/javascript">

            function parentMenuCheckBox(chkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    $('.chkbox' + id).each(function () {

                        this.checked = true;
                    });
                } else {
                    $('.chkbox' + id).each(function () {

                        this.checked = false;
                    });

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function subMenuCheckBox(chkBxId, pchkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    document.getElementById(pchkBxId).checked = true;

                } else {

                    if ($(".chkbox" + id + ":checked").length == 0) {

                        document.getElementById(pchkBxId).checked = false;
                    }

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function menuShowHide(collapseSubMenu) {

                for (var i = 1; i < collapseSubMenu.length; i++) {
                    $("." + collapseSubMenu + i).hide();
                }
                $("." + collapseSubMenu).slideToggle("slow");
            }

            function groupLevel(levelId, levelName) {

                var message = confirm("Your Selected Reseller Group  " + levelName);

                if (message == true) {

                    var dataString = 'levelId=' + levelId;

                    document.getElementById('groupListImageId').src = 'images/data-loader.gif';

                    $.ajax({
                        type: "POST",
                        url: 'SelectedGroupLevelName',
                        data: dataString,
                        success: function (data) {
                            document.getElementById('groupListImageId').src = '';
                            $("#menu-containt").html(data);
                        }
                    });
                } else {
                    ($("#groupLevelList").val('-1'));
                }
            }

            function menuPermissionSave() {

                if ($("#groupLevelList").val() == '-1') {
                    alert("Please select a valid Reseller");
                } else {

                    var message = confirm("Do you want to permission this menu ?");

                    if (message == true) {

                        var selected = new Array();
                        $('input:checkbox').each(function () {
                            selected.push($(this).attr('id'));
                        });

                        var stng = "";

                        for (var i = 0; i < selected.length; i++) {

                            if (document.getElementById(selected[i]).checked) {
                                stng += document.getElementById(selected[i]).value + '/fd/Y/rd/';
                            } else {
                                stng += document.getElementById(selected[i]).value + '/fd/N/rd/'
                            }
                        }

                        if ($("#groupLevelList").val() != '-1') {

                            if (stng.length != 0) {

                                var param = "levelId=" + $("#groupLevelList").val() + "&groupFormString=" + stng;

                                document.getElementById('createLoadingImage').src = 'images/loading.gif';

                                $.ajax({
                                    type: "POST",
                                    url: 'GroupLevelSubMenuSave',
                                    data: param,
                                    success: function (data) {
                                        document.getElementById('createLoadingImage').src = '';
                                        $("#message").html(data);
                                    }
                                });
                            }
                        }
                    }
                }
            }

            function startMobileRechargeThread() {

                var message = confirm("Do you want to start Mobile Recharge ?");

                if (message == true) {

                    document.getElementById('loginLoadingImage').src = 'images/load.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'StartMobileRechargeThread',
                        success: function (data) {
                            document.getElementById('loginLoadingImage').src = '';
                            $("#message").html(data);
                            buttonUpdate();
                        }
                    });
                }
            }

            function stopMobileRechargeThread() {

                var message = confirm("Do you want to stop Mobile Recharge ?");

                if (message == true) {

                    document.getElementById('loginLoadingImage').src = 'images/load.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'StopMobileRechargeThread',
                        success: function (data) {
                            document.getElementById('loginLoadingImage').src = '';
                            $("#message").html(data);
                            buttonUpdate();
                        }
                    });
                }
            }

            function startMobileMoneyThread() {

                var message = confirm("Do you want to start Mobile Money ?");

                if (message == true) {

                    document.getElementById('loginLoadingImage').src = 'images/load.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'StartMobileMoneyThread',
                        success: function (data) {
                            document.getElementById('loginLoadingImage').src = '';
                            $("#message").html(data);
                        }
                    });
                }
            }

            function stopMobileMoneyThread() {

                var message = confirm("Do you want to stop Mobile Money ?");

                if (message == true) {

                    document.getElementById('loginLoadingImage').src = 'images/load.gif';
                    //
                    $.ajax({
                        type: "POST",
                        url: 'StopMobileMoneyThread',
                        success: function (data) {
                            document.getElementById('loginLoadingImage').src = '';
                            $("#message").html(data);
                        }
                    });
                }
            }

            function buttonUpdate() {
                $.ajax({
                    type: "POST",
                    url: 'MRThreadButtonUpdate',
                    success: function (data) {
                        $("#thread_mr").html(data);
                    }
                });
            }

        </script>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Menu Control <small>Admin Menu Control</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <!--return message div-->
                    <div id="message">

                    </div>

                    <div class="row">
                        <div class=" col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Money Control Panel
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="javascript:;" class="remove">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form" id="pwd-container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                            <div style="border: 1px solid #b1bdbd;" class="portlet yellow-crusta box">
                                                <div style="background-color: #35aa47;" class="portlet-title">
                                                    <div style="width: 100%" class="caption">
                                                        <div style="margin-left: 15px; width: 20%; float: left;">
                                                            <i class="icon-paper-plane"></i>
                                                            Mobile Recharge
                                                        </div>
                                                        <div style="width: 18%; float: right;">
                                                            <i class="icon-wallet"></i>
                                                            Mobile Money
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div style="padding-top: 14px;" class="row static-info">
                                                        <div id="thread_mr" style="margin-left: 15px; float: left; width: 25%" class="col-xs-5 name">
                                                            <s:if test="isMRThreadRunning">
                                                                <button type="button" onclick="startMobileRechargeThread()" disabled class="btn red" style="margin-right: 5px;">
                                                                    <i class="icon-close"></i>
                                                                    Running....
                                                                </button>
                                                                <button type="button" onclick="stopMobileRechargeThread()" class="btn green">
                                                                    <i class="icon-check"></i>
                                                                    Stop
                                                                </button>
                                                            </s:if>
                                                            <s:else>
                                                                <button type="button" onclick="startMobileRechargeThread()" class="btn green" style="margin-right: 5px;">
                                                                    <i class="icon-check"></i>
                                                                    Start
                                                                </button>
                                                                <button type="button" onclick="stopMobileRechargeThread()" disabled class="btn red">
                                                                    <i class="icon-close"></i>
                                                                    Stop
                                                                </button>
                                                            </s:else>
                                                        </div>
                                                        <div style="text-align: center;width: 50%" class="col-xs-7 value">
                                                            <img id="loginLoadingImage" style="width: 30px; height: 25px;" src="" alt="" />
                                                        </div>

                                                        <div style="float: right; width: 20%" class="col-xs-7 value">
                                                            <button disabled type="button" style="margin-right: 5px;" onclick="startMobileMoneyThread()" class="btn green" >
                                                                <i class="icon-check"></i>
                                                                Start
                                                            </button>
                                                            <button disabled type="button" onclick="stopMobileMoneyThread()" class="btn red">
                                                                <i class="icon-close"></i>
                                                                Stop
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet light">
                                <div class="portlet-title">

                                    <div class="caption">
                                        Control Menu Access
                                    </div>

                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                        <a href="javascript:;" class="remove">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body form" id="pwd-container">

                                    <form class="form-horizontal">

                                        <!-- BEGIN FORM-->
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class=" col-md-3 control-label" for="rtype">User&nbsp;Group</label>
                                                <div class=" col-md-4">

                                                    <select class="form-control" name="groupLevelList" id="groupLevelList">
                                                        <option value="-1">Select&nbsp;A&nbsp;User</option>

                                                        <s:if test="userLevelInfoList !=null">
                                                            <s:if test="userLevelInfoList.size() !=0">
                                                                <s:iterator value="userLevelInfoList">
                                                                    <option onclick="groupLevel('<s:property value="levelId"/>', '<s:property value="levelName"/>');" value="<s:property value="levelId"/>"><s:property value="levelName"/></option>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                    <img id="groupListImageId" src="" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END FORM-->

                                        <div id="menu-containt" style="margin-left: 160px;max-width: 433px;" class="admin-menu-control navbar-collapse collapse in">

                                            <s:if test="parentMenuInfoList !=null">
                                                <s:if test="parentMenuInfoList.size() !=0">
                                                    <ul class="admin-menu-control-menu" data-auto-scroll="true" data-slide-speed="200">
                                                        <% int pid = 0;%>
                                                        <li class="">
                                                            <s:iterator value="parentMenuInfoList" >

                                                                <% pid++;%>
                                                                <% int cid = 0;%>
                                                                <a>
                                                                    <s:if test="parentMenuStatus=='Y'">
                                                                        <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu" checked>
                                                                    </s:if>

                                                                    <s:else>
                                                                        <input type="checkbox" value="<s:property value="menuId"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="parentMenu">
                                                                    </s:else>

                                                                    <i class="icon-home"></i>
                                                                    <span class="title">
                                                                        <s:property value="parentMenuName"/>
                                                                    </span>

                                                                    <span id=""  onclick="menuShowHide('collapseMenu<%=pid%>')" class="arrow"></span>

                                                                </a>

                                                                <!-- childMenuList come from ParentMenu persistance class -->
                                                                <s:iterator value="childMenuList"  >

                                                                    <% cid++;%>

                                                                    <ul id="" class="sub-menu collapseMenu<%=pid%>">

                                                                        <li>
                                                                            <a>

                                                                                <s:if test="childMenuStatus=='Y'">
                                                                                    <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu" checked>
                                                                                </s:if>

                                                                                <s:else>
                                                                                    <input type="checkbox" value="<s:property value="childMenuId"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenu">
                                                                                </s:else>

                                                                                <s:property value="childMenuName"/>

                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </s:iterator>
                                                            </s:iterator>
                                                        </li>
                                                    </ul>
                                                    <div class="form-actions fluid">
                                                        <div class=" col-md-offset-3 col-md-9">

                                                            <button type="button" onclick="menuPermissionSave()" class="btn blue">
                                                                <i class="icon-check"></i>
                                                                Save
                                                            </button>
                                                            <button type="reset" class="btn default">Cancel</button>
                                                            <img id="createLoadingImage" src="" alt="" />
                                                        </div>
                                                    </div> 
                                                </s:if>
                                            </s:if>
                                        </div>
                                    </form> 
                                    <div id="selectedGroupDivId"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>


        <!--FOR PASSWORD-->
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script type="text/javascript">

                                                                jQuery(document).ready(function () {

                                                                    // initiate layout and plugins
                                                                    Metronic.init(); // init metronic core components
                                                                    Layout.init(); // init current layout
                                                                    QuickSidebar.init() // init quick sidebar
                                                                    //        UIIdleTimeout.init();
                                                                });
        </script>

    </body>
</html>