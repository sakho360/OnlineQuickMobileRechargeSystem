<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png"  rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Settings <small>Application Settings</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row profile">
                        <div class="col-xs-12">
                            <!--BEGIN TABS-->
                            <div class="tabbable tabbable-custom tabbable-full-width">

                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_3" data-toggle="tab"> Account </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <!--tab_1_2-->
                                    <div class="tab-pane active" id="tab_1_3">
                                        <div class="row profile-account">

                                            <div class="col-xs-3">
                                                <ul class="ver-inline-menu tabbable margin-bottom-10">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#tab_1-1"><i class="icon-settings"></i>Application Info</a>
                                                        <span class="after"></span>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_2-2"><i class="icon-user"></i>Owner Info</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-7"><i class="icon-credit-card"></i>Admin Balance</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_3-3"><i class="icon-notebook"></i>Modules</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-4"><i class="icon-equalizer"></i>Notification</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-5"><i class="icon-paper-plane"></i>Mobile Recharge Settings</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-6"><i class="icon-wallet"></i>Mobile Money Settings</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_4-9"><i class="icon-wallet"></i>DBBL Settings</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_security_settings"><i class="icon-lock"></i>Security Settings</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab_db_cleanup"><i class="icon-trash"></i>Database Cleanup</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-xs-9">
                                                <div class="tab-content">

                                                    <div id="tab_1-1" class="tab-pane active">

                                                        <form method="post" role="form" action="#">

                                                            <div class="form-group">
                                                                <label class="control-label">Company Name</label>
                                                                <input type="text" name="company_name" value="Demo FLMCS" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Website Title</label>
                                                                <input type="text" name="website_title" value="Automatic Mobile Recharge and bKash" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Short News</label>
                                                                <input type="text" name="shortnews" value="Please visit flmcs.com for more details . Contact: sms/call/viber/whatsup/ 01833333339" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Balance Type</label>
                                                                <select name="same_bal" class="form-control input-medium">
                                                                    <option value="yes"  selected >Single Balance</option>
                                                                    <option value="no" >Separate Balance for bKash</option>
                                                                </select>
                                                                <span class="help-block">Please do not change this if you are not sure.</span>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">After Login Redirect URL</label>
                                                                <select name="redirecturl" class="form-control input-small">
                                                                    <option value="home"  selected >Home</option>
                                                                    <option value="send-mm" >Send bKash</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="header_js" class="control-label">Header Javascript</label>
                                                                <textarea id="header_js" name="header_js" class="form-control" rows="5"></textarea>
                                                                <span class="help-block">Javascript code for Livechat, Google Analytics etc.</span>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="footer_js" class="control-label">Footer Javascript</label>
                                                                <textarea id="footer_js" name="footer_js" class="form-control" rows="5"></textarea>
                                                                <span class="help-block">Javascript code to append end of the Page.</span>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Android App URL</label>
                                                                <input type="text" name="android" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">IOS App URL</label>
                                                                <input type="text" name="ios" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Theme Style</label>
                                                                <select name="theme" class="form-control input-small">
                                                                    <option value="style1"  selected >Style 1</option>
                                                                    <option value="style2" >Style 2</option>
                                                                    <option value="style3" >Style 3</option>
                                                                    <option value="style4" >Style 4</option>
                                                                    <option value="oldmate" >Oldmate</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Theme Color</label>
                                                                <select name="color" class="form-control input-small">
                                                                    <option value="default" >Default</option>
                                                                    <option value="dark" >Dark</option>
                                                                    <option value="blue"  selected >Blue</option>
                                                                    <option value="red" >Red</option>
                                                                    <option value="green" >Green</option>
                                                                    <option value="green-red" >Green Red</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Frontend Theme</label>
                                                                <select name="frontend" class="form-control input-small">
                                                                    <option value="default" >Default</option>
                                                                    <option value="light"  selected >Light</option>
                                                                    <option value="gtheme" >gTheme</option>
                                                                    <option value="modern" >Modern</option>
                                                                    <option value="bracket" >Bracket</option>
                                                                    <option value="login_def" >Android UI</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label" for="df">Date Format</label>
                                                                <select class="form-control" name="df" id="df">
                                                                    <option value="d/m/Y" >22/03/2016</option>
                                                                    <option value="d.m.Y" >22.03.2016</option>
                                                                    <option value="d-m-Y" >22-03-2016</option>
                                                                    <option value="m/d/Y" >03/22/2016</option>
                                                                    <option value="Y/m/d" >2016/03/22</option>
                                                                    <option value="Y-m-d" >2016-03-22</option>
                                                                    <option value="M d Y"  selected="selected" >Mar 22 2016</option>
                                                                    <option value="d M Y" >22 Mar 2016</option>
                                                                    <option value="jS M y" >22nd Mar 16</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label" for="df">Currency Format</label>
                                                                <select class="form-control" name="cformat" id="cformat">
                                                                    <option value="1" > 1234.56 </option>
                                                                    <option value="2"  selected="selected" > 1,234.56 </option>
                                                                    <option value="3" > 1234,56 </option>
                                                                    <option value="4" > 1.234,56 </option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Currency Code</label>
                                                                <input type="text" name="currency_code" value="Tk" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">API Handler</label>
                                                                <select name="apiv" class="form-control input-small">
                                                                    <option value="1" >V1</option>
                                                                    <option value="2" >V2</option>
                                                                    <option value="3" >V3</option>
                                                                    <option value="4"  selected >AB</option>
                                                                    <option value="5" >mPayBD</option>

                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Connect Mobile Recharge with</label>
                                                                <select name="flexiapi" class="form-control input-small">
                                                                    <option value="0"  selected >Modem / Manual</option>
                                                                    <option value="1" >API</option>
                                                                </select>
                                                                <span class="help-block">Please do not change this if you are not sure.</span>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Recharge API Host</label>
                                                                <input type="text" name="flexiapihost" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Recharge API User</label>
                                                                <input type="text" name="flexiapiuser" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Recharge API Key</label>
                                                                <input type="text" name="flexiapikey" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Recharge API Password</label>
                                                                <input type="text" name="flexiapipass" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Connect Mobile Money with</label>
                                                                <select name="mmapi" class="form-control input-small">
                                                                    <option value="0"  selected >Modem / Manual</option>
                                                                    <option value="1" >API</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Money API Host</label>
                                                                <input type="text" name="mmapihost" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Money API User</label>
                                                                <input type="text" name="mmapiuser" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Money API Key</label>
                                                                <input type="text" name="mmapikey" value="" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Mobile Money API Key</label>
                                                                <input type="text" name="mmapikey" value="" class="form-control"/>
                                                            </div>

                                                            <div class="margiv-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_2-2" class="tab-pane">

                                                        <form method="post" role="form" action="#">

                                                            <div class="form-group">
                                                                <label class="control-label">Email</label>
                                                                <input type="text" name="email" value="demo@example.com" class="form-control"/>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="control-label">Phone</label>
                                                                <input type="text" name="phone" value="01827112299"  class="form-control"/>
                                                            </div>

                                                            <div class="margiv-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_3-3" class="tab-pane">

                                                        <form method="post" role="form" action="#">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Mobile Recharge</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="flexiload" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="flexiload" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Mobile Money</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="mm" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="mm" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>bKash</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="bkash" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="bkash" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>DBBL</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="dbbl" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="dbbl" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>mCash</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="mcash" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="mcash" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>SMS</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="sms" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="sms" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>International Recharge</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="intrecharge" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="intrecharge" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Bank Transfer</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="bt" value="yes" checked/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="bt" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_4-4" class="tab-pane">

                                                        <form method="post" action="#">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Enable Email Notifications ?</td>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <label>Notification For</label>
                                                                            <div class="checkbox-list">
                                                                                <label><input type="checkbox" name="notify_bkash_personal" value="yes"  checked > bKash Personal </label>
                                                                                <label><input type="checkbox" name="notify_bkash_agent" value="yes"  checked > bKash Agent </label>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Notification Email</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="text" name="notify_email" value="" class="form-control"/></label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Enable SMS Notifications ?</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="radio" name="notify" value="yes"/> Yes </label>
                                                                        <label class="uniform-inline"><input type="radio" name="notify" value="no"/> No </label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>SMS Driver</td>
                                                                    <td>
                                                                        <select name="smsurl" class="form-control">
                                                                            <option value="msg21" >msg21.com</option>
                                                                            <option value="rsms"   selected >System SMS</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>SMS User</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="text" name="smsuser" value="admin" class="form-control"/></label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>SMS Password</td>
                                                                    <td>
                                                                        <label class="uniform-inline"><input type="text" name="smspass" value="123456" class="form-control"/></label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                                <a href="" class="btn default">Cancel</a>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_4-5" class="tab-pane">

                                                        <form action="#" method="post" class="form-horizontal">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Enable Mobile Recharge ?</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="flexiload" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="flexiload" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Minimum Mobile Recharge Amount: </td>
                                                                    <td>
                                                                        <input type="text" name="fl_min" value="1000000" class="form-control"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Maximum Mobile Recharge Amount: </td>
                                                                    <td>
                                                                        <input type="text" name="fl_max" value="1000000" class="form-control"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>GrameenPhone</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="gp" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="gp" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Robi</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="robi" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="robi" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Banglalink</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="bl" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="bl" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Airtel</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="airtel" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="airtel" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Teletalk</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="tl" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="tl" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_4-6" class="tab-pane">

                                                        <form action="#" method="post" class="form-horizontal">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Enable bKash ?</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="bkash" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="bkash" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Enable bKash Agent Number Submission ?</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="bkashagent" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="bkashagent" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Minimum Amount for Cashout</td>
                                                                    <td>
                                                                        <input type="text" name="bkashmin" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Maximum Amount For Cashout</td>
                                                                    <td>
                                                                        <input type="text" name="bkashmax" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cashout Fee (%)</td>
                                                                    <td>
                                                                        <input type="text" name="bkashfee" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_4-9" class="tab-pane">

                                                        <form action="#" method="post" class="form-horizontal">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Enable DBBL ?</td>
                                                                    <td>
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline"><input type="radio" name="dbbl" value="Yes"  checked  > Yes </label>
                                                                            <label class="radio-inline"><input type="radio" name="dbbl" value="No"  > No </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Minimum Amount for Cashout</td>
                                                                    <td>
                                                                        <input type="text" name="dbblmin" class="form-control input-inline input-medium" value="250000">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Maximum Amount For Cashout</td>
                                                                    <td>
                                                                        <input type="text" name="dbblmax" class="form-control input-inline input-medium" value="250000">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Cashout Fee (%)</td>
                                                                    <td>
                                                                        <input type="text" name="dbblfee" class="form-control input-inline input-medium" value="250000.00">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_4-7" class="tab-pane">

                                                        <form action="#" method="post" class="form-horizontal">

                                                            <table class="table table-bordered table-striped">
                                                                <tr>
                                                                    <td>Admin Mobile Recharge Balance</td>
                                                                    <td>
                                                                        <input type="text" name="fbal" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Admin Mobile Money Balance</td>
                                                                    <td>
                                                                        <input type="text" name="mmbal" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Admin International Recharge Balance</td>
                                                                    <td>
                                                                        <input type="text" name="intbal" class="form-control input-inline input-medium" value="1000000.00">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!--end profile-settings-->
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green">Save Changes</button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                    <div id="tab_security_settings" class="tab-pane">

                                                        <a href="#" class="btn red">Force Password Change</a>
                                                        <br>
                                                        <br>
                                                        <p class="alert alert-info">This will force all user to change the Password after login. You will be logged out. After Login again, you will be forced to set new password. You will have to login again with the new password.</p>
                                                        <hr>
                                                    </div>

                                                    <div id="tab_db_cleanup" class="tab-pane">

                                                        <a href="#" class="btn red">Clear IP Banned Logs</a>
                                                        <hr>

                                                        <a href="#" class="btn red">Delete All Logs</a>
                                                        <hr>

                                                        <a href="#" class="btn red">Delete All Mobile Recharge History</a>
                                                        <hr>

                                                        <a href="#" class="btn red">Delete All Mobile Money History</a>
                                                        <hr>

                                                        <a href="#" class="btn red">Delete Confirmation Message</a>
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

                FormWizard.init();

            });

        </script>

    </body>
</html>