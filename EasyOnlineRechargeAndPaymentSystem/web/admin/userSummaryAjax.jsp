<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="mrBalanceInfoList !=null">
    <s:if test="mrBalanceInfoList.size() !=0">
        <s:iterator value="mrBalanceInfoList">
            <div class="well">
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Payments Received from Parent []
                    </div>
                    <div class="col-xs-3 value">
                        Tk.  
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Payments Returned to Parent []
                    </div>
                    <div class="col-xs-3 value">
                        Tk.  
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Actual Payment (Sent - Returned)
                    </div>
                    <div class="col-xs-3 value">

                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Success Mobile Recharge Request Made by this User &amp; User's Sub User
                    </div>
                    <div class="col-xs-3 value">
                        Tk. 
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Balance Diff (Actual Payment - Usage)
                    </div>
                    <div class="col-xs-3 value">
                        Tk. 
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Current Balance
                    </div>
                    <div class="col-xs-3 value">
                        Tk. <s:property value="mrCurrentBalance"/>
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Current Balance - Diff
                    </div>
                    <div class="col-xs-3 value">
                        Tk. <s:property value="mrCurrentBalance"/>
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        User Tree
                    </div>
                    <div class="col-xs-3 value">
                        admin -&gt; <s:property value="userInfo.userId"/>
                    </div>
                </div>
            </div>
        </s:iterator>
    </s:if>
</s:if>
<s:if test="mrBalanceInfoList.size()==0">
    <div class="well">
        <div class="row static-info align-reverse">
            <div class="col-xs-8 name">
                Haven't any mobile recharge summary to this reseller.
            </div>
        </div>
    </div>
</s:if>

<s:if test="mmBalanceInfoList !=null">
    <s:if test="mmBalanceInfoList.size() !=0">
        <s:iterator value="mmBalanceInfoList">
            <div class="well">
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Payments Received from Parent []
                    </div>
                    <div class="col-xs-3 value">
                        Tk.  
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Payments Returned to Parent []
                    </div>
                    <div class="col-xs-3 value">
                        Tk.  
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Actual Payment (Sent - Returned)
                    </div>
                    <div class="col-xs-3 value">

                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Total Success Mobile Recharge Request Made by this User &amp; User's Sub User
                    </div>
                    <div class="col-xs-3 value">
                        Tk. 
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Balance Diff (Actual Payment - Usage)
                    </div>
                    <div class="col-xs-3 value">
                        Tk. 
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Current Balance
                    </div>
                    <div class="col-xs-3 value">
                        Tk. <s:property value="mmCurrentBalance"/>
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        Current Balance - Diff
                    </div>
                    <div class="col-xs-3 value">
                        Tk. <s:property value="mmCurrentBalance"/>
                    </div>
                </div>
                <div class="row static-info align-reverse">
                    <div class="col-xs-8 name">
                        User Tree
                    </div>
                    <div class="col-xs-3 value">
                        admin -&gt; <s:property value="userInfo.userId"/>
                    </div>
                </div>
            </div>
        </s:iterator>
    </s:if>
</s:if>
<s:if test="mmBalanceInfoList.size()==0">
    <div class="well">
        <div class="row static-info align-reverse">
            <div class="col-xs-8 name">
                Haven't any mobile money summary to this reseller.
            </div>
        </div>
    </div>
</s:if>