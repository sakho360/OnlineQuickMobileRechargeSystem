<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Dashboard <small>Fast Management Console</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">

                        <div class="col-xs-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Order # 1
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <h1>01784888089</h1>
                                    <h4>ID: 1</h4>
                                    <h4>Sender: admin</h4>
                                    <h4>Reseller 4: -1</h4>
                                    <h4>Reseller 3: -1</h4>
                                    <h4>Reseller 2: -1</h4>
                                    <h4>Operator: GrameenPhone</h4>
                                    <h4>Amount: 10</h4>
                                    <h4>Type: Prepaid</h4>
                                    <h4>Modem: 1</h4>
                                    <h4>Status: Waiting</h4>
                                    <h4>Request Time:  Feb 03, 2016 @ 10:42:52 </h4>
                                    <h4>Sent Time: </h4>
                                    <h4>Success Time: </h4>
                                    <h4>Transaction ID: </h4>
                                    <h4>IP: 104.131.114.100</h4>
                                    <h4>RR: 0</h4>
                                    <h4>A: <textarea class="form-control" rows="4">2cac691973f2929b383863f05a8b444e</textarea></h4>
                                    <h4>SNT: 1</h4>

                                    <h3>History:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Sender </th>
                                                <th> Phone </th>
                                                <th> Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> admin </td>
                                                <td> 01784888089 </td>
                                                <td> 2016-02-03 22:42:52 </td>
                                                <td> <span class="label label-sm label-danger"> Waiting </span> </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3>Related Message:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Time </th>
                                                <th> Status </th>
                                                <th> SMS </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h3>API Response:</h3>

                                    n/a

                                    <hr>

                                    <h4>Request Time:  Feb 03, 2016 @ 10:42:52 </h4>
                                    <h4>Amount: 10</h4>

                                    <hr>

                                    <h3>Action for Above Order:</h3>

                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>

                                    <br>
                                    <br>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>Failed Reason: </label>
                                            <input type="text" autocomplete="off" class="form-control input-xlarge"  name="trid" value="">
                                        </div>

                                        <button type="submit" class="btn btn-danger">Refund</button>
                                    </form>

                                    <hr>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>TrID: </label>
                                            <input type="text" class="form-control input-xlarge"  autocomplete="off" name="trid">
                                        </div>

                                        <button type="submit" class="btn btn-success">Manual Confirm</button>
                                    </form>

                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>


                        <div class="col-xs-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Order # 2
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <h1>01783385643</h1>
                                    <h4>ID: 2</h4>
                                    <h4>Sender: admin</h4>
                                    <h4>Reseller 4: -1</h4>
                                    <h4>Reseller 3: -1</h4>
                                    <h4>Reseller 2: -1</h4>
                                    <h4>Operator: GrameenPhone</h4>
                                    <h4>Amount: 567</h4>
                                    <h4>Type: Prepaid</h4>
                                    <h4>Modem: 1</h4>
                                    <h4>Status: Waiting</h4>
                                    <h4>Request Time:  Feb 04, 2016 @ 02:20:16 </h4>
                                    <h4>Sent Time: </h4>
                                    <h4>Success Time: </h4>
                                    <h4>Transaction ID: </h4>
                                    <h4>IP: 104.131.114.100</h4>
                                    <h4>RR: 0</h4>
                                    <h4>A: <textarea class="form-control" rows="4">6d095936bb6c761336397c19e5b50add</textarea></h4>
                                    <h4>SNT: 0</h4>

                                    <h3>History:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Sender </th>
                                                <th> Phone </th>
                                                <th> Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> admin </td>
                                                <td> 01783385643 </td>
                                                <td> 2016-02-04 14:20:16 </td>
                                                <td> <span class="label label-sm label-danger"> Waiting </span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3>Related Message:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Time </th>
                                                <th> Status </th>
                                                <th> SMS </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h3>API Response:</h3>

                                    n/a

                                    <hr>

                                    <h4>Request Time:  Feb 04, 2016 @ 02:20:16 </h4>
                                    <h4>Amount: 567</h4>

                                    <hr>

                                    <h3>Action for Above Order:</h3>

                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>

                                    <br>
                                    <br>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>Failed Reason: </label>
                                            <input type="text" autocomplete="off" class="form-control input-xlarge"  name="trid" value="">
                                        </div>

                                        <button type="submit" class="btn btn-danger">Refund</button>
                                    </form>

                                    <hr>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>TrID: </label>
                                            <input type="text" class="form-control input-xlarge"  autocomplete="off" name="trid">
                                        </div>

                                        <button type="submit" class="btn btn-success">Manual Confirm</button>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>

                        <div class="col-xs-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Order # 4
                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <h1>01553524982</h1>
                                    <h4>ID: 4</h4>
                                    <h4>Sender: admin</h4>
                                    <h4>Reseller 4: -1</h4>
                                    <h4>Reseller 3: -1</h4>
                                    <h4>Reseller 2: -1</h4>
                                    <h4>Operator: Teletalk</h4>
                                    <h4>Amount: 200</h4>
                                    <h4>Type: Prepaid</h4>
                                    <h4>Modem: 1</h4>
                                    <h4>Status: Waiting</h4>
                                    <h4>Request Time:  Feb 11, 2016 @ 06:27:52 </h4>
                                    <h4>Sent Time: </h4>
                                    <h4>Success Time: </h4>
                                    <h4>Transaction ID: </h4>
                                    <h4>IP: 104.131.114.100</h4>
                                    <h4>RR: 0</h4>
                                    <h4>A: <textarea class="form-control" rows="4">4c334e3758f173f167ddb6d5545e4562</textarea></h4>
                                    <h4>SNT: 0</h4>

                                    <h3>History:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Sender </th>
                                                <th> Phone </th>
                                                <th> Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> admin </td>
                                                <td> 01553524982 </td>
                                                <td> 2016-02-11 18:27:52 </td>
                                                <td> <span class="label label-sm label-danger"> Waiting </span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3>Related Message:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Time </th>
                                                <th> Status </th>
                                                <th> SMS </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h3>API Response:</h3>

                                    n/a

                                    <hr>

                                    <h4>Request Time:  Feb 11, 2016 @ 06:27:52 </h4>
                                    <h4>Amount: 200</h4>

                                    <hr>

                                    <h3>Action for Above Order:</h3>

                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>

                                    <br>
                                    <br>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>Failed Reason: </label>
                                            <input type="text" autocomplete="off" class="form-control input-xlarge"  name="trid" value="">
                                        </div>

                                        <button type="submit" class="btn btn-danger">Refund</button>
                                    </form>

                                    <hr>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>TrID: </label>
                                            <input type="text" class="form-control input-xlarge"  autocomplete="off" name="trid">
                                        </div>

                                        <button type="submit" class="btn btn-success">Manual Confirm</button>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>

                        <div class="col-xs-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Order # 5
                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <h1>01613100107</h1>
                                    <h4>ID: 5</h4>
                                    <h4>Sender: admin</h4>
                                    <h4>Reseller 4: -1</h4>
                                    <h4>Reseller 3: -1</h4>
                                    <h4>Reseller 2: -1</h4>
                                    <h4>Operator: Airtel</h4>
                                    <h4>Amount: 50</h4>
                                    <h4>Type: Prepaid</h4>
                                    <h4>Modem: 1</h4>
                                    <h4>Status: Waiting</h4>
                                    <h4>Request Time:  Feb 11, 2016 @ 08:57:23 </h4>
                                    <h4>Sent Time: </h4>
                                    <h4>Success Time: </h4>
                                    <h4>Transaction ID: </h4>
                                    <h4>IP: 104.131.114.100</h4>
                                    <h4>RR: 0</h4>
                                    <h4>A: <textarea class="form-control" rows="4">b7ad0a9baa5c4ae098491d9a08741bcb</textarea></h4>
                                    <h4>SNT: 0</h4>

                                    <h3>History:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Sender </th>
                                                <th> Phone </th>
                                                <th> Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> admin </td>
                                                <td> 01613100107 </td>
                                                <td> 2016-02-11 20:57:23 </td>
                                                <td><span class="label label-sm label-danger"> Waiting </span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3>Related Message:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Time </th>
                                                <th> Status </th>
                                                <th> SMS </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h3>API Response:</h3>

                                    n/a

                                    <hr>

                                    <h4>Request Time:  Feb 11, 2016 @ 08:57:23 </h4>
                                    <h4>Amount: 50</h4>

                                    <hr>

                                    <h3>Action for Above Order:</h3>

                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>

                                    <br>
                                    <br>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>Failed Reason: </label>
                                            <input type="text" autocomplete="off" class="form-control input-xlarge"  name="trid" value="">
                                        </div>

                                        <button type="submit" class="btn btn-danger">Refund</button>
                                    </form>

                                    <hr>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>TrID: </label>
                                            <input type="text" class="form-control input-xlarge"  autocomplete="off" name="trid">
                                        </div>

                                        <button type="submit" class="btn btn-success">Manual Confirm</button>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>


                        <div class="col-xs-12">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">

                                <div class="portlet-title">
                                    <div class="caption">
                                        Order # 7
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <h1>01685809300</h1>
                                    <h4>ID: 7</h4>
                                    <h4>Sender: admin</h4>
                                    <h4>Reseller 4: -1</h4>
                                    <h4>Reseller 3: -1</h4>
                                    <h4>Reseller 2: -1</h4>
                                    <h4>Operator: Airtel</h4>
                                    <h4>Amount: 10</h4>
                                    <h4>Type: Prepaid</h4>
                                    <h4>Modem: 1</h4>
                                    <h4>Status: Waiting</h4>
                                    <h4>Request Time:  Feb 20, 2016 @ 09:15:08 </h4>
                                    <h4>Sent Time: </h4>
                                    <h4>Success Time: </h4>
                                    <h4>Transaction ID: </h4>
                                    <h4>IP: 104.131.114.100</h4>
                                    <h4>RR: 0</h4>
                                    <h4>A: <textarea class="form-control" rows="4">b760556c889054102e02a7679c7c1f0e</textarea></h4>
                                    <h4>SNT: 0</h4>

                                    <h3>History:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Sender </th>
                                                <th> Phone </th>
                                                <th> Date </th>
                                                <th> Status </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> 1 </td>
                                                <td> admin </td>
                                                <td> 01685809300 </td>
                                                <td> 2016-02-20 21:15:08 </td>
                                                <td><span class="label label-sm label-danger"> Waiting </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> 2 </td>
                                                <td> admin </td>
                                                <td> 01685809300 </td>
                                                <td> 2016-02-21 13:31:54 </td>
                                                <td><span class="label label-sm label-danger"> Waiting </span></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <h3>Related Message:</h3>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th> # </th>
                                                <th> Time </th>
                                                <th> Status </th>
                                                <th> SMS </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                    <h3>API Response:</h3>

                                    n/a

                                    <hr>

                                    <h4>Request Time:  Feb 20, 2016 @ 09:15:08 </h4>
                                    <h4>Amount: 10</h4>

                                    <hr>

                                    <h3>Action for Above Order:</h3>

                                    <a href="#" class="btn red">Click Here for Resend From Modem</a>

                                    <br>
                                    <br>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>Failed Reason: </label>
                                            <input type="text" autocomplete="off" class="form-control input-xlarge"  name="trid" value="">
                                        </div>

                                        <button type="submit" class="btn btn-danger">Refund</button>
                                    </form>

                                    <hr>

                                    <form method="post" class="form-inline" role="form" action="#" target="_blank">

                                        <div class="form-group">
                                            <label>TrID: </label>
                                            <input type="text" class="form-control input-xlarge"  autocomplete="off" name="trid">
                                        </div>

                                        <button type="submit" class="btn btn-success">Manual Confirm</button>
                                    </form>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>

                        <div class="col-md-12">
                            <a href="#" class="btn btn-info">Reload This Page</a>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>



        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

            });

        </script>

    </body>
</html>