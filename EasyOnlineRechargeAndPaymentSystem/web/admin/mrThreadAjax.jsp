<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="isMRThreadRunning">
    <button type="button" onclick="startMobileRechargeThread()" disabled class="btn red" style="margin-right: 5px;">
        <i class="icon-close"></i>
        Running....
    </button>
    <button type="button" onclick="stopMobileRechargeThread()" class="btn green">
        <i class="icon-check"></i>
        Stop
    </button>
</s:if>
<s:else>
    <button type="button" onclick="startMobileRechargeThread()" class="btn green" style="margin-right: 5px;">
        <i class="icon-check"></i>
        Start
    </button>
    <button type="button" onclick="stopMobileRechargeThread()" disabled class="btn red">
        <i class="icon-close"></i>
        Stop
    </button>
</s:else>