<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css">

        <link href="<%= request.getContextPath()%>/my-css/modalCss.css" rel="stylesheet" type="text/css">

        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/select2.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/dataTables.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Payments <small>Payments made history</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Begin: life time stats -->
                            <div class="portlet light">
                                <div class="portlet-body">
                                    <div class="table-container">
                                        <div class="table-actions-wrapper">
                                            <span></span>
                                        </div>
                                        <div style="padding-top: 16px;" class="table-scrollable">
                                            <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                                <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="5%">ID</th>
                                                        <th width="12%">Date</th>
                                                        <th width="18%">From</th>
                                                        <th width="10%">To</th>
                                                        <th width="10%">Amount</th>
                                                        <th width="10%">Type</th>
                                                        <th width="6%">Memo</th>
                                                        <th width="10%">Old Bal</th>
                                                        <th width="10%">New Bal</th>
                                                        <th width="7%">Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <s:if test="mrTransactionHInfoList !=null">
                                                        <s:if test="mrTransactionHInfoList.size() !=0">
                                                            <s:iterator value="mrTransactionHInfoList">
                                                                <tr>
                                                                    <td><s:property value="mrTransactionHistoryId"/></td>
                                                                    <td><s:property value="balanceGivenDate"/></td>
                                                                    <td><s:property value="balanceGivenBy"/></td>
                                                                    <td><s:property value="userInfo.userId"/></td>
                                                                    <td><s:property value="addBalance"/></td>
                                                                    <td>
                                                                        <s:if test="type=='Y'">
                                                                            Transfer
                                                                        </s:if>
                                                                        <s:else>
                                                                            <span style="color: #ff0033;">
                                                                                Waiting
                                                                            </span>
                                                                        </s:else>
                                                                    </td>
                                                                    <td><s:property value="mrBalanceDescription"/></td>
                                                                    <td><s:property value="0"/></td>
                                                                    <td><s:property value="0"/></td>
                                                                    <td>
                                                                        <a href="javascript:void(0);" id="<s:property value="mrTransactionHistoryId"/>" class="fview btn btn-xs default btn-editable">
                                                                            <i class="icon-list"></i>
                                                                            View
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="4" style="text-align:right">Total:</th>
                                                        <th colspan="6" style="text-align: left"></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End: life time stats -->
                        </div>
                    </div>

                    <div id="ajax-modal" class="modal fade" tabindex="-1">

                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>

        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/bootstrap-modal.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/select2.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js-css/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js-css/dataTables.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/datatable.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/transactions.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable_orders').DataTable({
                    "order": [[0, "asc"]],
                    "orderCellsTop": true,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(4)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(4, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(4).footer()).html(
                                'Page Total ' + pageTotal + ' [ All Total ' + total + ' ]'
                                );
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                //initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //UIIdleTimeout.init();

                EcommerceOrders.init();
            });
        </script>

        <script type="text/javascript">

            $(document).on("click", '.fview', function (e) {

                e.preventDefault();

                var id = this.id;

                $('body').modalmanager('loading');

                setTimeout(function () {
                    $('#ajax-modal').load('ViewTransactionsHistoryDetails', {mrTransactionHistoryId: id}, function () {
                        $('#ajax-modal').modal();
                    });
                }, 2000, id);
            });
        </script>

    </body>
</html>