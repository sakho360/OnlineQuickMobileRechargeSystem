<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Easy Online Recharge and Payment System</title>
        <link href="<%= request.getContextPath()%>/images/title.png" rel="shortcut icon"/>

        <link href="<%= request.getContextPath()%>/my-css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/my-css/s1-blue.css" rel="stylesheet" type="text/css"/>
        <!--BOOTSTRAP.CSS BEFORE CUSTOM.CSS-->
        <link href="<%= request.getContextPath()%>/my-css/custom.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/my-css/comp-sep.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="page-header-fixed page-quick-sidebar-over-content ">

        <!--PAGE HEADER-->
        <%@include file="/administration/header.jsp" %>

        <div class="clearfix">

        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!--SIDE MENU-->
            <%@include file="/administration/left_menu.jsp" %>


            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Modem <small>modem balance and latest sms info</small>
                            </h3>
                        </div>
                    </div>

                    <%@include file="/administration/marquee.jsp" %>

                    <!--END DASHBOARD-->

                    <div class="row">
                        <div class="col-xs-6">

                            <div class="tiles">
                                <div class="tile bg-blue-steel">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            GP
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-blue-steel">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Flexiload</h4>
                                        <p>     </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            GrameenPhone
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-red-thunderbird">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Robi
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-thunderbird">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>8383</h4>
                                        <p>     </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Robi
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-yellow-casablanca">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            BL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-yellow-casablanca">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Topup</h4>
                                        <p>    </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Banglalink
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-red-flamingo">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            Airtel
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-flamingo">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Recharge</h4>
                                        <p>       </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Airtel
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tiles">
                                <div class="tile bg-green-turquoise">
                                    <div class="tile-body">
                                        <i class="icon-calendar"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            TL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-green-turquoise">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>Tele Recharge</h4>
                                        <p>      </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            Teletalk
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">

                            <div class="tiles">
                                <div class="tile bg-purple-seance">
                                    <div class="tile-body">
                                        <i class="icon-wallet"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            bKash
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-purple-seance">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>bKash</h4>
                                        <p>    </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            bKash
                                        </div>
                                    </div>
                                </div>

                                <div class="tile bg-red-flamingo">
                                    <div class="tile-body">
                                        <i class="icon-wallet"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            DBBL
                                        </div>
                                        <div class="number">
                                            0.00
                                        </div>
                                    </div>
                                </div>
                                <div class="tile double selected bg-red-flamingo">
                                    <div class="corner"></div>
                                    <div class="check"></div>
                                    <div class="tile-body">
                                        <h4>DBBL</h4>
                                        <p>      </p>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <i class="icon-envelope"></i>
                                        </div>
                                        <div class="number">
                                            DBBL
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>


        </div>

        <!--PAGE FOOTER-->
        <%@ include file="/administration/footer.jsp" %>


        <!--JAVA SCRIPT AND JQUERY PART-->
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap-hover-dropdown.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/metronic.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/layout.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/quick-sidebar.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/my-js/jquery_002.js" type="text/javascript"></script>

        <script>
            jQuery(document).ready(function () {
                // initiate layout and plugins
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init() // init quick sidebar
                //        UIIdleTimeout.init();

            });

        </script>

    </body>
</html>