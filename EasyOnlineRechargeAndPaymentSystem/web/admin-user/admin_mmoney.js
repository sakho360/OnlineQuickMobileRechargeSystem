(function ($) {

    var btn_view = $(".mm_btn_view");

    var order_details = $("#order_details");

    var progress = '<div class="progress progress-sm progress-striped active"><div class="progress-bar primary" style="width: 100%"></div></div>';

    var display_order_id = $("#display_order_id");

    var oid = "";

    btn_view.on('click', function () {

        order_details.html(progress);

        var vid = this.id;

        oid = vid.replace("t", "");

        display_order_id.html("Order ID: " + oid);

        var mobileNo = $(this).val();

        $.get("AdminUserMMoneyView", {mobileMoneyId: oid, searchPhone: mobileNo}, function (data) {
            order_details.html(data);
        });
    });
}(jQuery));

