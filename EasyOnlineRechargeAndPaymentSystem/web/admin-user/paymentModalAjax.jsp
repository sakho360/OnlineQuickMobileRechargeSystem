<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singlePaymentInfoList !=null">
    <s:if test="singlePaymentInfoList.size() !=0">
        <s:iterator value="singlePaymentInfoList">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 30%">Reseller</th>
                        <th><s:property value="userId"/></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Full Name</td>
                        <td><s:property value="userName"/></td>
                    </tr>
                    <tr>
                        <td>MR Balance</td>
                        <td><s:property value="mobileRechargeDebitInfo.mrCurrentBalance"/></td>
                    </tr>
                    <tr>
                        <td>MM Balance</td>
                        <td><s:property value="mobileMoneyDebitInfo.mmCurrentBalance"/></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><s:property value="contactNumber1"/></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td><s:property value="emailAddress"/></td>
                    </tr>
                    <tr>
                        <td>Reseller Type</td>
                        <td><s:property value="userGroupInfo.groupName"/></td>
                    </tr>
                    <tr>
                        <td>Last Login</td>
                        <td><s:property value="lastLogin"/></td>
                    </tr>
                    <tr>
                        <td>Last Logout</td>
                        <td><s:property value="lastLogout"/></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>
                            <s:if test="suspendActivity=='Y'">
                                Active
                            </s:if>
                            <s:else>
                                Inactive
                            </s:else>
                        </td>
                    </tr>
                </tbody>
            </table>
        </s:iterator>
    </s:if>
</s:if>
<s:if test="singlePaymentInfoList ==null">
    no record found
</s:if>
<s:elseif test="singlePaymentInfoList.size() ==0">
    no record found
</s:elseif>