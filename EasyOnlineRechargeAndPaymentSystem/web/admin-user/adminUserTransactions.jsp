<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN USER PANEL</title>
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/admin-user/logo.png">

        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/animate.css"/>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/glyphicons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/material-design-icons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/app.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font.css" type="text/css" />

    </head>
    <body class="container pace-done">
        <div class="app" id="app">
            <div id="aside" class="app-aside modal fade folded md nav-expand">

                <div class="left navside indigo-900 dk" layout="column">
                    <div class="navbar navbar-md no-radius">
                        <a class="navbar-brand">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24" height="24">
                            <path d="M 4 4 L 44 4 L 44 44 Z" fill="#03A9F4"></path>
                            <path d="M 4 4 L 34 4 L 24 24 Z" fill="rgba(0,0,0,0.15)"></path>
                            <path d="M 4 4 L 24 4 L 4  44 Z" fill="#6887ff"></path>
                            </svg>
                            <img src="<%= request.getContextPath()%>/admin-user/logo.png" alt="." class="hide">
                            <span class="hidden-folded inline">BB IT</span>
                        </a>
                    </div>
                    <div flex="" class="hide-scroll">
                        <nav class="scroll nav-active-primary">
                            <ul class="nav" ui-nav="">
                                <li class="nav-header hidden-folded">
                                    <small class="text-muted">Main</small>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserDashBoard">
                                        <span class="nav-icon">
                                            <i class="material-icons">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                                                <path d="M24,20c-7.72,0-14,6.28-14,14h4c0-5.51,4.49-10,10-10s10,4.49,10,10h4C38,26.28,31.721,20,24,20z" fill="#6887ff"></path>
                                                </svg>
                                            </i>
                                        </span>
                                        <span class="nav-text">Dashboard</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileRecharge">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Recharge</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserMobileMoney">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Mobile Money</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserPayments">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Make Payment</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="AdminUserTransactions">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Transactions</span>
                                    </a>
                                </li>
                                <li ui-sref-active="active">
                                    <a href="Home">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Back to Home</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div flex-no-shrink="">
                        <nav ui-nav="">
                            <ul class="nav">
                                <li>
                                    <div class="b-b b m-t-sm"></div>
                                </li>
                                <li class="no-bg">
                                    <a href="AdminUserLogout">
                                        <span class="nav-icon">
                                            <i class="material-icons"></i>
                                        </span>
                                        <span class="nav-text">Logout</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

            <div id="content" class="app-content box-shadow-z0" role="main">
                <div ui-view class="app-body" id="view">
                    <div class="padding">
                        <div class="box">
                            <div class="box-header">
                                <h2>MR Recent Transactions</h2>
                                <!--                                <div class="btn-group m-t-sm">
                                                                    <a href="" class="btn btn-sm info active">Show All</a>
                                                                    <a href="" class="btn btn-sm success active">Mobile Recharge</a>
                                                                    <a href="" class="btn btn-sm warning">Mobile Money</a>
                                                                </div>-->
                            </div>
                            <!--                            <div class="box-body">
                                                            <input id="filter" type="text" class="form-control  inline m-r" placeholder="Search..." />
                                                        </div>-->
                            <div>
                                <table class="table m-b-none table-responsive" ui-jp="footable" data-filter="#filter" data-page-size="100">
                                    <thead>
                                        <tr>
                                            <!--<th data-toggle="true">Tr ID</th>-->
                                            <th>Type</th>
                                            <th>Date</th>
                                            <th>Amount</th>
                                            <th>Memo</th>
                                            <!--                                            <th>Old Bal</th>
                                                                                        <th>New Bal</th>
                                                                                        <th>Action</th>-->
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_render">
                                        <s:if test="mrTransactionHInfoList !=null">
                                            <s:if test="mrTransactionHInfoList.size() !=0">
                                                <s:iterator value="mrTransactionHInfoList">
                                                    <tr>
                                                        <!--<td></td>-->
                                                        <<td>
                                                            <s:if test="type=='Y'">
                                                                Refund
                                                            </s:if>
                                                            <s:else>
                                                                <span style="color: #009966;">
                                                                    Waiting
                                                                </span>
                                                            </s:else>
                                                        </td>
                                                        <td><s:property value="balanceGivenDate"/></td>
                                                        <td><s:property value="addBalance"/></td>
                                                        <td><s:property value="mrBalanceDescription"/></td>
<!--                                                        <td><s:property value="0"/></td>
                                                        <td><s:property value="0"/></td>
                                                        <td></td>-->
                                                    </tr>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                        <s:if test="mrTransactionHInfoList ==null">
                                            <tr>
                                                <td style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;" colspan="8">
                                                    no record found
                                                </td>
                                            </tr>
                                        </s:if>
                                        <s:elseif test="mrTransactionHInfoList.size() ==0">
                                            <tr>
                                                <td style="text-align: center; color: #ff66cc; font-size: 16px; font-weight: bold;" colspan="8">
                                                    no record found
                                                </td>
                                            </tr>
                                        </s:elseif>
                                    </tbody>
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <ul class="pagination"></ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/admin-user/ui-nav.js" type="text/javascript"></script>

        <script type="text/javascript">
            function ReloadPage() {
                location.reload();
            }
        </script>

        <script>
            jQuery(document).ready(function () {
                setTimeout("ReloadPage()", 180000);
            });
        </script>

    </body>
</html>
