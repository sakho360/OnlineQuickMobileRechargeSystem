<div id="aside" class="app-aside modal fade folded md nav-expand">
    <div class="left navside indigo-900 dk" layout="column">
        <div class="navbar navbar-md no-radius">
            <a class="navbar-brand">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24" height="24">
                    <path d="M 4 4 L 44 4 L 44 44 Z" fill="#03A9F4"></path>
                    <path d="M 4 4 L 34 4 L 24 24 Z" fill="rgba(0,0,0,0.15)"></path>
                    <path d="M 4 4 L 24 4 L 4  44 Z" fill="#6887ff"></path>
                </svg>
                <img src="<%= request.getContextPath()%>/admin-user/logo.png" alt="." class="hide">
                    <span class="hidden-folded inline">BB IT</span>
            </a>
        </div>
        <div flex="" class="hide-scroll">
            <nav class="scroll nav-active-primary">
                <ul class="nav" ui-nav="">
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">Main</small>
                    </li>
                    <li ui-sref-active="active">
                        <a href="AdminUserDashBoard">
                            <span class="nav-icon">
                                <i class="material-icons">
                                    ?<svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
                                        <path d="M24,20c-7.72,0-14,6.28-14,14h4c0-5.51,4.49-10,10-10s10,4.49,10,10h4C38,26.28,31.721,20,24,20z" fill="#6887ff"></path>
                                    </svg>
                                </i>
                            </span>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li ui-sref-active="active">
                        <a href="AdminUserMobileRecharge">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Mobile Recharge</span>
                        </a>
                    </li>
                    <li ui-sref-active="active">
                        <a href="AdminUserMobileMoney">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Mobile Money</span>
                        </a>
                    </li>
                    <li ui-sref-active="active">
                        <a href="AdminUserPayments">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Make Payment</span>
                        </a>
                    </li>
                    <li ui-sref-active="active">
                        <a href="AdminUserTransactions">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Transactions</span>
                        </a>
                    </li>
                    <li ui-sref-active="active">
                        <a href="Home">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Back to Home</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div flex-no-shrink="">
            <nav ui-nav="">
                <ul class="nav">
                    <li>
                        <div class="b-b b m-t-sm"></div>
                    </li>
                    <li class="no-bg">
                        <a href="AdminUserLogout">
                            <span class="nav-icon">
                                <i class="material-icons">?</i>
                            </span>
                            <span class="nav-text">Logout</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>