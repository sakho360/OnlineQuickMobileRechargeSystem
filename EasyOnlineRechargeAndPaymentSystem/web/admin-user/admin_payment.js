(function ($) {

    var btn_view = $(".payment_btn_view");

    var user_details = $("#user_details");

    var progress = '<div class="progress progress-sm progress-striped active"><div class="progress-bar primary" style="width: 100%"></div></div>';

    var display_order_id = $("#display_order_id");

    var rid = "";

    btn_view.on('click', function () {

        user_details.html(progress);

        var vid = this.id;

        rid = vid.replace("t", "");

        display_order_id.html("Reseller : " + rid);

        $.get("AdminUserPaymentView", {resellerId: rid}, function (data) {
            user_details.html(data);
        });
    });
}(jQuery));