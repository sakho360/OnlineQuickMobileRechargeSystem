<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN USER PANEL</title>
        <link rel="shortcut icon" href="<%= request.getContextPath()%>/admin-user/logo.png">

        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/animate.css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/glyphicons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/material-design-icons.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/app.css" type="text/css" />
        <link rel="stylesheet" href="<%= request.getContextPath()%>/admin-user/font.css" type="text/css" />

    </head>
    <body class="container pace-done">
        <div class="app" id="app">
            <div class="center-block w-xxl w-auto-xs p-y-md">
                <div class="navbar">
                    <div class="pull-center">
                        <a class="navbar-brand">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24" height="24">
                            <path d="M 4 4 L 44 4 L 44 44 Z" fill="#03A9F4"></path>
                            <path d="M 4 4 L 34 4 L 24 24 Z" fill="rgba(0,0,0,0.15)"></path>
                            <path d="M 4 4 L 24 4 L 4  44 Z" fill="#6887ff"></path>
                            </svg>
                            <img src="<%= request.getContextPath()%>/admin-user/logo.png" alt="." class="hide">
                            <span class="hidden-folded inline">Brand Bangla IT</span>
                        </a>
                    </div>
                </div>

                <div class="p-a-md box-color r box-shadow-z1 text-color m-a">
                    <form name="form" method="post" id="admin_login">
                        <div class="md-form-group float-label">
                            <input type="text" id="userName" name="userName" class="md-input" required>
                            <label>Username</label>
                        </div>
                        <div class="md-form-group float-label">
                            <input type="password" id="password" name="password" class="md-input" required>
                            <label>Password</label>
                        </div>
                        <div class="m-b-md">
                            <label class="md-check">
                                <input type="checkbox">
                                <i class="primary"></i>
                                Keep me signed in
                            </label>
                        </div>
                        <button type="submit" class="btn primary btn-block p-x-md" id="btn_login">Sign in</button>
                    </form>
                </div>
            </div>
        </div>

        <div id="m-sm" class="modal" data-backdrop="true">
            <div class="row-col h-v">
                <div class="row-cell v-m">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-body text-center p-lg">
                                <p id="errorMess"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn danger p-x-md" data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="<%= request.getContextPath()%>/my-js/jquery-1.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/admin-user/tether.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/my-js/bootstrap.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                var btn_login = $("#btn_login");

                btn_login.on('click', function (e) {

                    e.preventDefault();

                    btn_login.prop('disabled', true);

                    $.post("AdminUserLogin", $("#admin_login").serialize()).done(function (data) {
                        
                        if (data.trim() == 1) {
                            window.location = 'AdminUserDashBoard';
                        } else {
                            $('#m-sm').modal('show');
                            $('#errorMess').html(data);
                            btn_login.prop('disabled', false);
                        }
                    });
                });
            });

        </script>
    </body>
</html>