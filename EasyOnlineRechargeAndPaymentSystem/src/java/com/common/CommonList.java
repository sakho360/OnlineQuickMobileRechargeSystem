package com.common;

import com.persistance.AdminGroup;
import com.persistance.AdminUser;
import com.persistance.BalanceRequestHistory;
import com.persistance.ChildMenu;
import com.persistance.ChildUserInfo;
import com.persistance.Contact;
import com.persistance.MRGroupInfo;
import com.persistance.MRGroupName;
import com.persistance.MobileMoneyDebit;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileRechargeDebit;
import com.persistance.MobileRechargeHistory;
import com.persistance.ParentMenu;
import com.persistance.ManageRate;
import com.persistance.MobileMoneyTransactionHistory;
import com.persistance.MobileOperatorCodeInfo;
import com.persistance.MobileRechargeTransactionHistory;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import com.persistance.UserLevel;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommonList {

    public List<UserInfo> suspendTest(Connection connection, String userId, String password) {

        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        List<UserInfo> userInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSuspendUserInfo(userId, password));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //
                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return userInfoList;
    }

    public List<UserInfo> loginInfo(Connection connection, String userId, String password) {

        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        List<UserInfo> userInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLoginInfo(userId, password));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //
                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return userInfoList;
    }

    public List<AdminUser> adminUserLoginInfo(Connection connection, String userName, String password) {

        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;

        AdminUser adminUserInfo = null;
        AdminGroup adminGroup = null;
        List<AdminUser> adminUserInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAdminUserLoginInfo(userName, password));
                rs = ps.executeQuery();

                adminUserInfoList = new ArrayList<AdminUser>();

                while (rs.next()) {

                    adminUserInfo = new AdminUser();
                    adminGroup = new AdminGroup();

                    adminUserInfo.setAdminUserId(rs.getInt("admin_user_id"));
                    adminUserInfo.setUserName(rs.getString("user_name"));
                    adminUserInfo.setPassword(rs.getString("password"));
                    adminUserInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    adminGroup.setAdminGroupId(rs.getInt("admin_group_id"));
                    adminGroup.setAdminGroupName(rs.getString("admin_group_name"));

                    adminUserInfo.setAdminGroupInfo(adminGroup);

                    adminUserInfoList.add(adminUserInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                adminUserInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return adminUserInfoList;
    }

    public List<MobileRechargeHistory> getSingleMobileRechargeHistoryInfo(Connection connection, String uId, Integer mrhId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitInfo = null;
        List<MobileRechargeHistory> singleMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMRecharge(uId, mrhId));
                rs = ps.executeQuery();

                singleMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();
                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitInfo = new MobileRechargeDebit();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));

                    DateFormat formatter = new SimpleDateFormat("YYYY-MMM-dd" + "\n" + "HH:mm:ss");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mobileRechargeHistoryInfo.setPurchasedOn(fDate);

                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    mobileRechargeDebitInfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));

                    mobileRechargeHistoryInfo.setMobileRechargeDebitInfo(mobileRechargeDebitInfo);

                    singleMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                singleMobileRechargeHistoryInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getLastMobileRechargeHistoryInfo(Connection connection, String uId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLastMRecharge(uId));
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getLatestProcessingOrdersMRHInfo(Connection connection, String uId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLatestProcessingOrdersMR(uId));
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getLatestWaitingMRHInfo(Connection connection, String uId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLatestWaitingOrdersMR(uId));
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getLatestSuccessMRHInfo(Connection connection, String uId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLatestSuccessOrdersMR(uId));
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getAllMobileRechargeHistoryInfo(Connection connection, String uId) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> allMobileRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllMRecharge(uId));
                rs = ps.executeQuery();

                allMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mobileRechargeHistoryInfo.setPurchasedOn(fDate);

                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setTnxid(rs.getString("tnxid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    allMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allMobileRechargeHistoryInfoList;
    }

    public List<MobileMoneyHistory> getSingleMobileMoneyHistoryInfo(Connection connection, String uId, Integer mmhId) {

        MobileMoneyHistory mobileMoneyHistoryInfo = null;
        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileMoneyDebit moneyDebitInfo = null;
        List<MobileMoneyHistory> singleMobileMoneyHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMMoney(uId, mmhId));
                rs = ps.executeQuery();

                singleMobileMoneyHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mobileMoneyHistoryInfo = new MobileMoneyHistory();
                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    moneyDebitInfo = new MobileMoneyDebit();

                    mobileMoneyHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));

                    DateFormat formatter = new SimpleDateFormat("YYYY-MMM-dd" + "\n" + "HH:mm:ss");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mobileMoneyHistoryInfo.setPurchasedOn(fDate);

                    mobileMoneyHistoryInfo.setSender(rs.getString("sender"));
                    mobileMoneyHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileMoneyHistoryInfo.setOperator(rs.getString("operator"));
                    mobileMoneyHistoryInfo.setType(rs.getString("type"));
                    mobileMoneyHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileMoneyHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileMoneyHistoryInfo.setTotalAmount(rs.getDouble("total_amount"));
                    mobileMoneyHistoryInfo.setTrid(rs.getString("trid"));
                    mobileMoneyHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileMoneyHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    moneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    moneyDebitInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));

                    mobileMoneyHistoryInfo.setMoneyDebitInfo(moneyDebitInfo);

                    singleMobileMoneyHistoryInfoList.add(mobileMoneyHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleMobileMoneyHistoryInfoList;
    }

    public List<MobileMoneyHistory> getLastMobileMoneyHistoryInfo(Connection connection, String uId) {

        MobileMoneyHistory mobileMoneyHistoryInfo = null;
        List<MobileMoneyHistory> lastMobileMoneyHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLastMMoney(uId));
                rs = ps.executeQuery();

                lastMobileMoneyHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mobileMoneyHistoryInfo = new MobileMoneyHistory();

                    mobileMoneyHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));
                    mobileMoneyHistoryInfo.setSender(rs.getString("sender"));
                    mobileMoneyHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileMoneyHistoryInfo.setOperator(rs.getString("operator"));
                    mobileMoneyHistoryInfo.setType(rs.getString("type"));
                    mobileMoneyHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileMoneyHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileMoneyHistoryInfo.setTotalAmount(rs.getDouble("total_amount"));
                    mobileMoneyHistoryInfo.setTrid(rs.getString("trid"));
                    mobileMoneyHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileMoneyHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    lastMobileMoneyHistoryInfoList.add(mobileMoneyHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return lastMobileMoneyHistoryInfoList;
    }

    public List<MobileMoneyHistory> getAllMobileMoneyHistoryInfo(Connection connection, String uId) {

        MobileMoneyHistory mobileMoneyHistoryInfo = null;
        List<MobileMoneyHistory> allMobileMoneyHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllMMoney(uId));
                rs = ps.executeQuery();

                allMobileMoneyHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mobileMoneyHistoryInfo = new MobileMoneyHistory();

                    mobileMoneyHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mobileMoneyHistoryInfo.setPurchasedOn(fDate);

                    mobileMoneyHistoryInfo.setSender(rs.getString("sender"));
                    mobileMoneyHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileMoneyHistoryInfo.setOperator(rs.getString("operator"));
                    mobileMoneyHistoryInfo.setType(rs.getString("type"));
                    mobileMoneyHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileMoneyHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileMoneyHistoryInfo.setTotalAmount(rs.getDouble("total_amount"));
                    mobileMoneyHistoryInfo.setTrid(rs.getString("trid"));
                    mobileMoneyHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileMoneyHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    allMobileMoneyHistoryInfoList.add(mobileMoneyHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allMobileMoneyHistoryInfoList;
    }

    public List<UserGroup> getAllUserGroupInfo(Connection connection) {

        UserGroup userGroupInfo = null;
        List<UserGroup> allUserGroupInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllUserGroup());
                rs = ps.executeQuery();

                allUserGroupInfoList = new ArrayList<UserGroup>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    allUserGroupInfoList.add(userGroupInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        allUserGroupInfoList.remove(0);
        return allUserGroupInfoList;
    }

    public List<ManageRate> getAllManageRateInfo(Connection connection, String uId) {

        ManageRate manageRateInfo = null;
        List<ManageRate> allManageRateInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllManageRate(uId));
                rs = ps.executeQuery();

                allManageRateInfoList = new ArrayList<ManageRate>();

                while (rs.next()) {

                    manageRateInfo = new ManageRate();

                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    manageRateInfo.setGrameenPhone(rs.getFloat("grameen_phone"));
                    manageRateInfo.setRobi(rs.getFloat("robi"));
                    manageRateInfo.setBanglalink(rs.getFloat("banglalink"));
                    manageRateInfo.setAirtel(rs.getFloat("airtel"));
                    manageRateInfo.setTeletalk(rs.getFloat("teletalk"));
                    manageRateInfo.setBkash(rs.getFloat("bkash"));
                    manageRateInfo.setDbbl(rs.getFloat("dbbl"));
                    manageRateInfo.setInsertBy(rs.getString("insert_by"));

                    allManageRateInfoList.add(manageRateInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allManageRateInfoList;
    }

    public List<ManageRate> getSingleManageRateInfo(Connection connection, String uId, Integer rateId) {

        ManageRate manageRateInfo = null;
        List<ManageRate> singleManageRateInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleManageRate(uId, rateId));
                rs = ps.executeQuery();

                singleManageRateInfoList = new ArrayList<ManageRate>();

                while (rs.next()) {

                    manageRateInfo = new ManageRate();

                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    manageRateInfo.setGrameenPhone(rs.getFloat("grameen_phone"));
                    manageRateInfo.setRobi(rs.getFloat("robi"));
                    manageRateInfo.setBanglalink(rs.getFloat("banglalink"));
                    manageRateInfo.setAirtel(rs.getFloat("airtel"));
                    manageRateInfo.setTeletalk(rs.getFloat("teletalk"));
                    manageRateInfo.setBkash(rs.getFloat("bkash"));
                    manageRateInfo.setDbbl(rs.getFloat("dbbl"));
                    manageRateInfo.setInsertBy(rs.getString("insert_by"));

                    singleManageRateInfoList.add(manageRateInfo);
                }
            } catch (Exception e) {
                singleManageRateInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleManageRateInfoList;
    }

    public List<UserInfo> getResellerSubAdminInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> resellerSubAdminInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerSubAdmin(pId));
                rs = ps.executeQuery();

                resellerSubAdminInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    resellerSubAdminInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return resellerSubAdminInfoList;
    }

    public List<UserInfo> getResellerFourInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> resellerFourInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerFour(pId));
                rs = ps.executeQuery();

                resellerFourInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    resellerFourInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return resellerFourInfoList;
    }

    public List<UserInfo> getResellerThreeInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> resellerThreeInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerThree(pId));
                rs = ps.executeQuery();

                resellerThreeInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    resellerThreeInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return resellerThreeInfoList;
    }

    public List<UserInfo> getResellerTwoInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> resellerTwoInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerTwo(pId));
                rs = ps.executeQuery();

                resellerTwoInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    resellerTwoInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return resellerTwoInfoList;
    }

    public List<UserInfo> getResellerOneInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> resellerOneInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerOne(pId));
                rs = ps.executeQuery();

                resellerOneInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    resellerOneInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return resellerOneInfoList;
    }

    public List<UserInfo> getAllResellerInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> allResellerInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllReseller(pId));
                rs = ps.executeQuery();

                allResellerInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    allResellerInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return allResellerInfoList;
    }

    public List<UserInfo> getAllResellerInfoByAdmin(Connection connection) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> allResellerByAdminInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllResellerByAdmin());
                rs = ps.executeQuery();

                allResellerByAdminInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));

                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));

                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);

                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);

                    allResellerByAdminInfoList.add(userInfo);
                }
            } catch (Exception e) {
                allResellerByAdminInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return allResellerByAdminInfoList;
    }

    public List<AdminGroup> getAllAdminGroupInfo(Connection connection) {

        AdminGroup adminGroupInfo = null;
        List<AdminGroup> allAdminGroupInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                allAdminGroupInfoList = new ArrayList<AdminGroup>();

                ps = connection.prepareStatement(QueryStatement.selectAllAdminGroup());
                rs = ps.executeQuery();

                while (rs.next()) {

                    adminGroupInfo = new AdminGroup();

                    adminGroupInfo.setAdminGroupId(rs.getInt("admin_group_id"));
                    adminGroupInfo.setAdminGroupName(rs.getString("admin_group_name"));
                    adminGroupInfo.setAdminGroupStatus(rs.getString("admin_group_status").charAt(0));

                    allAdminGroupInfoList.add(adminGroupInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allAdminGroupInfoList;
    }

    public List<AdminUser> getAllAdminUserInfo(Connection connection) {

        AdminUser adminUserInfo = null;
        AdminGroup adminGroupInfo = null;
        List<AdminUser> allAdminUserInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                allAdminUserInfoList = new ArrayList<AdminUser>();

                ps = connection.prepareStatement(QueryStatement.selectAllAdminUser());
                rs = ps.executeQuery();

                while (rs.next()) {

                    adminUserInfo = new AdminUser();
                    adminGroupInfo = new AdminGroup();

                    adminUserInfo.setAdminUserId(rs.getInt("admin_user_id"));
                    adminUserInfo.setFullName(rs.getString("full_name"));
                    adminUserInfo.setEmail(rs.getString("email"));
                    adminUserInfo.setPhone(rs.getString("phone"));
                    adminUserInfo.setUserName(rs.getString("user_name"));
                    adminUserInfo.setPassword(rs.getString("password"));
                    adminUserInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    adminGroupInfo.setAdminGroupId(rs.getInt("admin_group_id"));
                    adminGroupInfo.setAdminGroupName(rs.getString("admin_group_name"));

                    adminUserInfo.setAdminGroupInfo(adminGroupInfo);

                    allAdminUserInfoList.add(adminUserInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allAdminUserInfoList;
    }

    public List<AdminUser> getOneAdminUserInfo(Connection connection, Integer id) {

        AdminUser adminUserInfo = null;
        AdminGroup adminGroupInfo = null;
        List<AdminUser> singleAdminUserInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                singleAdminUserInfoList = new ArrayList<AdminUser>();

                ps = connection.prepareStatement(QueryStatement.selectSingleAdminUser(id));
                rs = ps.executeQuery();

                while (rs.next()) {

                    adminUserInfo = new AdminUser();
                    adminGroupInfo = new AdminGroup();

                    adminUserInfo.setAdminUserId(rs.getInt("admin_user_id"));
                    adminUserInfo.setFullName(rs.getString("full_name"));
                    adminUserInfo.setEmail(rs.getString("email"));
                    adminUserInfo.setPhone(rs.getString("phone"));
                    adminUserInfo.setUserName(rs.getString("user_name"));
                    adminUserInfo.setPassword(rs.getString("password"));
                    adminUserInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    adminGroupInfo.setAdminGroupId(rs.getInt("admin_group_id"));
                    adminGroupInfo.setAdminGroupName(rs.getString("admin_group_name"));

                    adminUserInfo.setAdminGroupInfo(adminGroupInfo);

                    singleAdminUserInfoList.add(adminUserInfo);
                }
            } catch (Exception e) {
                singleAdminUserInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleAdminUserInfoList;
    }

    public List<UserInfo> getMyProfileInfo(Connection connection, String id) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> profileInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMyProfileInfo(id));
                rs = ps.executeQuery();

                profileInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setPinConfirmation(rs.getString("pin_conf_status"));

                    Date dbDate = (rs.getTimestamp("last_login") == null ? null : rs.getTimestamp("last_login"));
                    if (dbDate == null) {
                        userInfo.setLastLogin("");
                    } else {
                        String fmDate = formatter.format(dbDate);
                        userInfo.setLastLogin(fmDate);
                    }

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));
                    //
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //
                    profileInfoList.add(userInfo);
                }
            } catch (Exception e) {
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return profileInfoList;
    }

    public List<UserLevel> getAllUserLevelInfo(Connection connection) {

        UserLevel userLevelInfo = null;
        List<UserLevel> allUserLevelInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllUserLevel());
                rs = ps.executeQuery();

                allUserLevelInfoList = new ArrayList<UserLevel>();

                while (rs.next()) {

                    userLevelInfo = new UserLevel();

                    userLevelInfo.setLevelId(rs.getInt("level_id"));
                    userLevelInfo.setLevelName(rs.getString("level_name"));
                    userLevelInfo.setStatus(rs.getString("status").charAt(0));
                    userLevelInfo.setComCode(rs.getString("com_code"));

                    allUserLevelInfoList.add(userLevelInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                return allUserLevelInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allUserLevelInfoList;
    }

    public List<ParentMenu> getAllParentMenuInfo(Connection connection) {

        ParentMenu parentMenuInfo = null;
        List<ParentMenu> parentMenuInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllParentMenu());
                rs = ps.executeQuery();

                parentMenuInfoList = new ArrayList<ParentMenu>();

                while (rs.next()) {

                    parentMenuInfo = new ParentMenu();

                    parentMenuInfo.setMenuId(rs.getInt("menu_id"));
                    parentMenuInfo.setParentMenuName(rs.getString("menu_name"));
                    parentMenuInfo.setParentMenuTitle(rs.getString("menu_title"));
                    parentMenuInfo.setParentMenuId(rs.getInt("parent_menu_id"));
                    parentMenuInfo.setParentMenuUrl(rs.getString("menu_url"));
                    parentMenuInfo.setParentMenuStatus(rs.getString("menu_status").charAt(0));

                    parentMenuInfo.setChildMenuList(getAllChildMenuInfo(connection, rs.getInt("menu_id")));

                    parentMenuInfoList.add(parentMenuInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                parentMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return parentMenuInfoList;
    }

    public List<ChildMenu> getAllChildMenuInfo(Connection connection, Integer pid) {

        ChildMenu childMenuInfo = null;
        List<ChildMenu> childMenuInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllChildMenu(pid));
                rs = ps.executeQuery();

                childMenuInfoList = new ArrayList<ChildMenu>();

                while (rs.next()) {

                    childMenuInfo = new ChildMenu();

                    childMenuInfo.setChildMenuId(rs.getInt("menu_id"));
                    childMenuInfo.setChildMenuName(rs.getString("menu_name"));
                    childMenuInfo.setChildMenuTitle(rs.getString("menu_title"));
                    childMenuInfo.setParentMenuId(rs.getInt("parent_menu_id"));
                    childMenuInfo.setChildMenuUrl(rs.getString("menu_url"));
                    childMenuInfo.setChildMenuStatus(rs.getString("menu_status").charAt(0));

                    childMenuInfoList.add(childMenuInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                childMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return childMenuInfoList;
    }

    public List<UserLevelMenu> getUserLevelMenu(Connection connection, Integer lvId) {

        UserLevelMenu userLevelMenuInfo = null;
        ParentMenu parentMenuInfo = null;
        List<UserLevelMenu> userLevelMenuInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectUselLevelMenu(lvId));
                rs = ps.executeQuery();

                userLevelMenuInfoList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    userLevelMenuInfo = new UserLevelMenu();
                    parentMenuInfo = new ParentMenu();

                    userLevelMenuInfo.setUserLevelMenuId(rs.getInt("user_level_menu_id"));
                    userLevelMenuInfo.setMenuId(rs.getString("menu_id"));
                    userLevelMenuInfo.setLevelId(rs.getInt("level_id"));
                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("level_menu_status").charAt(0));

                    parentMenuInfo.setMenuId(rs.getInt("menu_id"));
                    parentMenuInfo.setParentMenuId(rs.getInt("parent_menu_id"));
                    parentMenuInfo.setParentMenuName(rs.getString("menu_name"));

                    userLevelMenuInfo.setParentMenuInfo(parentMenuInfo);

                    userLevelMenuInfo.setUserLevelChildMenuList(getUserLevelChildMenu(connection, rs.getInt("level_id"), rs.getInt("menu_id")));

                    userLevelMenuInfoList.add(userLevelMenuInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userLevelMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return userLevelMenuInfoList;
    }

    public List<ChildMenu> getUserLevelChildMenu(Connection connection, Integer lvId, Integer pmId) {

        ChildMenu childMenuInfo = null;
        UserLevelMenu userLevelMenuInfo = null;
        List<ChildMenu> userLevelChildMenuInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectUselLevelChildMenu(lvId, pmId));
                rs = ps.executeQuery();

                userLevelChildMenuInfoList = new ArrayList<ChildMenu>();

                while (rs.next()) {

                    childMenuInfo = new ChildMenu();
                    userLevelMenuInfo = new UserLevelMenu();

                    childMenuInfo.setChildMenuId(rs.getInt("menu_id"));
                    childMenuInfo.setChildMenuName(rs.getString("menu_name"));
                    childMenuInfo.setParentMenuId(rs.getInt("parent_menu_id"));

                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("level_menu_status").charAt(0));

                    childMenuInfo.setUserLevelMenuInfo(userLevelMenuInfo);

                    userLevelChildMenuInfoList.add(childMenuInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userLevelChildMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return userLevelChildMenuInfoList;
    }

    public List<UserInfo> getSingleResellerInfo(Connection connection, String pId, String uId) {

        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> singleResellerInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleReseller(pId, uId));
                rs = ps.executeQuery();

                singleResellerInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    //
                    userInfo.setUserId(rs.getString("user_id") == null ? "" : rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setPinConfirmation(rs.getString("pin_conf_status"));

                    Date cDate = rs.getTimestamp("insert_date");
                    String cfDate = formatter.format(cDate);
                    userInfo.setInsertDate(cfDate);

                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                    Date dbDate = (rs.getTimestamp("last_login") == null ? null : rs.getTimestamp("last_login"));
                    if (dbDate == null) {
                        userInfo.setLastLogin("");
                    } else {
                        String fmDate = formatter.format(dbDate);
                        userInfo.setLastLogin(fmDate);
                    }

                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId((rs.getInt("group_id") == 0 ? 0 : rs.getInt("group_id")));
                    userGroupInfo.setGroupName((rs.getString("group_name") == null ? "" : rs.getString("group_name").isEmpty() ? "" : rs.getString("group_name")));
                    //
                    manageRateInfo.setManageRateId((rs.getInt("manage_rate_id") == 0 ? 0 : rs.getInt("manage_rate_id")));
                    manageRateInfo.setRateName((rs.getString("rate_name") == null ? "" : rs.getString("rate_name").isEmpty() ? "" : rs.getString("rate_name")));
                    //
                    mobileRechargeDebitinfo.setMobileRechargeDebitId((rs.getInt("mr_debit_id") == 0 ? 0 : rs.getInt("mr_debit_id")));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal") == 0d ? 0d : rs.getDouble("mr_cur_bal")));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId((rs.getInt("mm_debit_id") == 0 ? 0 : rs.getInt("mm_debit_id")));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal") == 0d ? 0d : rs.getDouble("mm_cur_bal")));
                    //                    
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //                    
                    userInfo.setManageRateInfo(manageRateInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //                    
                    singleResellerInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleResellerInfoList;
    }

    public List<MRGroupName> getSingleMRGroupInfo(Connection connection, String uId, Integer id) {

        MRGroupName mrGroupName = null;
        MRGroupInfo mrGroupInfo = null;
        List<MRGroupName> singleMRGroupNameList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String allMRGNameSelectStatement = " SELECT "
                        + " MRGI.mr_group_id, "
                        + " MRGI.mobile_no, "
                        + " MRGI.type, "
                        + " MRGI.amount, "
                        + " MRGI.modem_no, "
                        + " MRGI.user_id, "
                        + " MRGI.mr_gname_id, "
                        + " MRGN.mr_gname_id, "
                        + " MRGN.mr_group_name, "
                        + " MRGN.insert_by "
                        + " FROM "
                        + " mr_group_info MRGI, "
                        + " mr_group_name MRGN "
                        + " WHERE "
                        + " MRGN.mr_gname_id = MRGI.mr_gname_id "
                        + " AND "
                        + " MRGN.user_id = '" + uId + "' "
                        + " AND "
                        + " MRGN.mr_gname_id = '" + id + "' ";

                ps = connection.prepareStatement(allMRGNameSelectStatement);
                rs = ps.executeQuery();

                singleMRGroupNameList = new ArrayList<MRGroupName>();

                while (rs.next()) {

                    mrGroupName = new MRGroupName();
                    mrGroupInfo = new MRGroupInfo();

                    mrGroupName.setMrGNameId(rs.getInt("mr_gname_id"));
                    mrGroupName.setMrGroupName(rs.getString("mr_group_name"));
                    mrGroupName.setInsertBy(rs.getString("insert_by"));

                    mrGroupInfo.setMobileNumber(rs.getString("mobile_no"));
                    mrGroupInfo.setType(rs.getInt("type"));
                    mrGroupInfo.setAmount(rs.getDouble("amount"));
                    mrGroupInfo.setModemNumber(rs.getInt("modem_no"));

                    mrGroupName.setMrGroupInfo(mrGroupInfo);

                    singleMRGroupNameList.add(mrGroupName);
                }
            } catch (Exception e) {
                singleMRGroupNameList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleMRGroupNameList;
    }

    public List<MRGroupName> getAllMRGroupName(Connection connection, String uId) {

        MRGroupName mrGroupName = null;
        List<MRGroupName> allMRGroupNameList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllMRGroupName(uId));
                rs = ps.executeQuery();

                allMRGroupNameList = new ArrayList<MRGroupName>();

                while (rs.next()) {

                    mrGroupName = new MRGroupName();

                    mrGroupName.setMrGNameId(rs.getInt("mr_gname_id"));
                    mrGroupName.setMrGroupName(rs.getString("mr_group_name"));
                    mrGroupName.setInsertBy(rs.getString("insert_by"));

                    allMRGroupNameList.add(mrGroupName);
                }
            } catch (Exception e) {
                System.out.println(e);
                allMRGroupNameList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allMRGroupNameList;
    }

    public List<MobileRechargeDebit> getSingMRBalance(Connection connection, String id) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        List<MobileRechargeDebit> singleMRBalanceInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerMRBalance(id));
                rs = ps.executeQuery();

                singleMRBalanceInfoList = new ArrayList<MobileRechargeDebit>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    //
                    mobileRechargeDebitInfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //
                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    //
                    mobileRechargeDebitInfo.setUserInfo(userInfo);
                    mobileRechargeDebitInfo.setUserGroupInfo(userGroupInfo);
                    mobileRechargeDebitInfo.setManageRateInfo(manageRateInfo);
                    //
                    singleMRBalanceInfoList.add(mobileRechargeDebitInfo);
                }
            } catch (Exception e) {
                singleMRBalanceInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleMRBalanceInfoList;
    }

    public List<MobileMoneyDebit> getSingMMBalance(Connection connection, String id) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        List<MobileMoneyDebit> singleMMBalanceInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerMMBalance(id));
                rs = ps.executeQuery();

                singleMMBalanceInfoList = new ArrayList<MobileMoneyDebit>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    //
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //
                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    //
                    mobileMoneyDebitInfo.setUserInfo(userInfo);
                    mobileMoneyDebitInfo.setUserGroupInfo(userGroupInfo);
                    mobileMoneyDebitInfo.setManageRateInfo(manageRateInfo);
                    //
                    singleMMBalanceInfoList.add(mobileMoneyDebitInfo);
                }
            } catch (Exception e) {
                singleMMBalanceInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singleMMBalanceInfoList;
    }

    public List<MobileRechargeHistory> getAllMRNumber(Connection connection, String sId, String mrNumber) {

        MobileRechargeHistory mrHistoryInfo = null;

        List<MobileRechargeHistory> mrHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMRNumber(sId, mrNumber));
                rs = ps.executeQuery();

                mrHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mrHistoryInfo = new MobileRechargeHistory();

                    DateFormat formatter = new SimpleDateFormat("YYYY-MMM-dd" + "\n" + "HH:mm:ss");

                    mrHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mrHistoryInfo.setSender(rs.getString("sender"));
                    mrHistoryInfo.setReceiver(rs.getString("receiver"));
                    mrHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mrHistoryInfo.setType(rs.getInt("type"));
                    mrHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    mrHistoryInfo.setTrid(rs.getString("trid"));

                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fmfDate = formatter.format(dbDate);
                    mrHistoryInfo.setPurchasedOn(fmfDate);

                    mrHistoryInfoList.add(mrHistoryInfo);
                }
            } catch (Exception e) {
                mrHistoryInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return mrHistoryInfoList;
    }

    public List<MobileMoneyHistory> getAllMMNumber(Connection connection, String uId, String mNumber) {

        MobileMoneyHistory mmHistoryInfo = null;

        List<MobileMoneyHistory> mmHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMMNumber(uId, mNumber));
                rs = ps.executeQuery();

                mmHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mmHistoryInfo = new MobileMoneyHistory();

                    DateFormat formatter = new SimpleDateFormat("YYYY-MMM-dd" + "\n" + "HH:mm:ss");

                    mmHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));
                    mmHistoryInfo.setSender(rs.getString("sender"));
                    mmHistoryInfo.setReceiver(rs.getString("receiver"));
                    mmHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mmHistoryInfo.setType(rs.getString("type"));
                    mmHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    mmHistoryInfo.setTrid(rs.getString("trid"));

                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fmfDate = formatter.format(dbDate);
                    mmHistoryInfo.setPurchasedOn(fmfDate);

                    mmHistoryInfoList.add(mmHistoryInfo);
                }
            } catch (Exception e) {
                mmHistoryInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return mmHistoryInfoList;
    }

    public List<MobileRechargeHistory> getAllMRHistoryByDate(Connection connection, String uId, String cMrDate) {

        MobileRechargeHistory mrHistoryInfo = null;

        List<MobileRechargeHistory> mrHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMRHistoryByDate(uId, cMrDate));
                rs = ps.executeQuery();

                mrHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mrHistoryInfo = new MobileRechargeHistory();

                    mrHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mrHistoryInfo.setSender(rs.getString("sender"));
                    mrHistoryInfo.setReceiver(rs.getString("receiver"));
                    mrHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mrHistoryInfo.setType(rs.getInt("type"));

                    DateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mrHistoryInfo.setPurchasedOn(fDate);

                    mrHistoryInfo.setTrid(rs.getString("trid"));
                    mrHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    mrHistoryInfoList.add(mrHistoryInfo);
                }
            } catch (Exception e) {
                mrHistoryInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrHistoryInfoList;
    }

    public List<MobileMoneyHistory> getAllMMHistoryByDate(Connection connection, String uId, String cMmDate) {

        MobileMoneyHistory mmHistoryInfo = null;

        List<MobileMoneyHistory> mmHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMMHistoryByDate(uId, cMmDate));
                rs = ps.executeQuery();

                mmHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mmHistoryInfo = new MobileMoneyHistory();

                    mmHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));
                    mmHistoryInfo.setSender(rs.getString("sender"));
                    mmHistoryInfo.setReceiver(rs.getString("receiver"));
                    mmHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mmHistoryInfo.setType(rs.getString("type"));

                    DateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mmHistoryInfo.setPurchasedOn(fDate);

                    mmHistoryInfo.setTrid(rs.getString("trid"));
                    mmHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    mmHistoryInfoList.add(mmHistoryInfo);
                }
            } catch (Exception e) {
                mmHistoryInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return mmHistoryInfoList;
    }

    public List<UserInfo> getOnlineUSer(Connection connection) {

        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> onLineUserInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectOnlineUser());
                rs = ps.executeQuery();

                onLineUserInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));

                    Date cDate = rs.getTimestamp("insert_date");
                    String cfDate = formatter.format(cDate);
                    userInfo.setInsertDate(cfDate);

                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                    Date dbDate = rs.getTimestamp("last_login");
                    String fDate = formatter.format(dbDate);
                    userInfo.setLastLogin(fDate);

                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //
                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    //
                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    //                            
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //                    
                    userInfo.setManageRateInfo(manageRateInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //                    
                    onLineUserInfoList.add(userInfo);
                }
            } catch (Exception e) {
                onLineUserInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return onLineUserInfoList;
    }

    public List<UserInfo> getAdmin(Connection connection, String uId) {

        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> adminInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAdmin(uId));
                rs = ps.executeQuery();

                adminInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));

                    Date cDate = rs.getTimestamp("insert_date");
                    String cfDate = formatter.format(cDate);
                    userInfo.setInsertDate(cfDate);

                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                    Date dbDate = rs.getTimestamp("last_login");
                    String fDate = formatter.format(dbDate);
                    userInfo.setLastLogin(fDate);

                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //
                    manageRateInfo.setManageRateId(rs.getInt("manage_rate_id"));
                    manageRateInfo.setRateName(rs.getString("rate_name"));
                    //
                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    //                    
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //                    
                    userInfo.setManageRateInfo(manageRateInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //                    
                    adminInfoList.add(userInfo);
                }
            } catch (Exception e) {
                adminInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return adminInfoList;
    }

    public List<UserInfo> getParentId(Connection connection, String pId, String uId) {

        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> parentIdInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectParentId(pId, uId));
                rs = ps.executeQuery();

                parentIdInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));

                    Date cDate = (rs.getTimestamp("insert_date") == null ? null : rs.getTimestamp("insert_date"));
                    if (cDate == null) {
                        userInfo.setInsertDate("");
                    } else {
                        String cfDate = formatter.format(cDate);
                        userInfo.setInsertDate(cfDate);
                    }

                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                    Date dbDate = (rs.getTimestamp("last_login") == null ? null : rs.getTimestamp("last_login"));
                    if (dbDate == null) {
                        userInfo.setLastLogin("");
                    } else {
                        String fDate = formatter.format(dbDate);
                        userInfo.setLastLogin(fDate);
                    }

                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //                    
                    parentIdInfoList.add(userInfo);
                }
            } catch (Exception e) {
                parentIdInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return parentIdInfoList;
    }

    public List<UserLevelMenu> getManus(Connection connection, String uId, Integer gId) {

        UserLevelMenu userLevelMenuInfo = null;
        UserInfo userInfo = new UserInfo();
        List<UserLevelMenu> userLevelMenusList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMenu(uId, gId));
                rs = ps.executeQuery();

                userLevelMenusList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    userLevelMenuInfo = new UserLevelMenu();

                    userLevelMenuInfo.setMenuId(rs.getString("menu_id"));
                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("level_menu_status").charAt(0));

                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));

                    userLevelMenuInfo.setUserInfo(userInfo);

                    userLevelMenusList.add(userLevelMenuInfo);
                }
            } catch (Exception e) {
                userLevelMenusList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return userLevelMenusList;
    }

    public List<Contact> getAllContact(Connection connection, String uId) {

        Contact contactInfo = null;
        List<Contact> contactList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAllContact(uId));
                rs = ps.executeQuery();

                contactList = new ArrayList<Contact>();

                while (rs.next()) {

                    contactInfo = new Contact();

                    contactInfo.setContactId(rs.getInt("contact_id"));
                    contactInfo.setContactName(rs.getString("contact_name"));
                    contactInfo.setContactNumber(rs.getString("contact_number"));

                    contactList.add(contactInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                contactList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return contactList;
    }

    public List<Contact> getContactNumber(Connection connection, Integer cId, String uId) {

        Contact contactInfo = null;
        List<Contact> contactNumberList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectContactNumber(cId, uId));
                rs = ps.executeQuery();

                contactNumberList = new ArrayList<Contact>();

                while (rs.next()) {

                    contactInfo = new Contact();

                    contactInfo.setContactId(rs.getInt("contact_id"));
                    contactInfo.setContactName(rs.getString("contact_name"));
                    contactInfo.setContactNumber(rs.getString("contact_number"));

                    contactNumberList.add(contactInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                contactNumberList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return contactNumberList;
    }

    public List<UserInfo> getCurrentPin(Connection connection, String uId, String uCPin) {

        UserInfo userInfo = null;
        List<UserInfo> curPinInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCurrentPin(uId, uCPin));
                rs = ps.executeQuery();

                curPinInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setPinConfirmation(rs.getString("pin_conf_status"));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    //                    
                    curPinInfoList.add(userInfo);
                }
            } catch (Exception e) {
                curPinInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return curPinInfoList;
    }

    public List<UserInfo> getCurrentPassword(Connection connection, String uId, String uCPass) {

        UserInfo userInfo = null;
        List<UserInfo> cPassInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCurrentPassword(uId, uCPass));
                rs = ps.executeQuery();

                cPassInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setUserPassword(rs.getString("user_password"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    //                    
                    cPassInfoList.add(userInfo);
                }
            } catch (Exception e) {
                cPassInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return cPassInfoList;
    }

    public List<UserInfo> getPaymentInfo(Connection connection, String pId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> paymentInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectPaymentInfo(pId));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "\n" + " HH:mm:ss");

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));

                    Date dbDate = ((rs.getTimestamp("last_login")) == null ? null : (rs.getTimestamp("last_login")));
                    if (dbDate == null) {
                        userInfo.setLastLogin("");
                    } else {
                        String fmDate = formatter.format(dbDate);
                        userInfo.setLastLogin(fmDate);
                    }
                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0 ? 0 : (rs.getDouble("mr_cur_bal")));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0 ? 0 : (rs.getDouble("mm_cur_bal")));
                    //
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //
                    paymentInfoList.add(userInfo);
                }
            } catch (Exception e) {
                paymentInfoList = null;
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<UserInfo> getSinglePaymentInfo(Connection connection, String pId, String rId) {

        UserGroup userGroupInfo = null;
        UserInfo userInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<UserInfo> singlePaymentInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSinglePaymentInfo(pId, rId));
                rs = ps.executeQuery();

                singlePaymentInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userGroupInfo = new UserGroup();
                    userInfo = new UserInfo();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "\n" + " HH:mm:ss");

                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setContactNumber1(rs.getString("contact_number1"));
                    userInfo.setEmailAddress(rs.getString("email_address"));
                    userInfo.setParentId(rs.getString("parent_id"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    userInfo.setInsertBy(rs.getString("insert_by"));
                    userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                    userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                    userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));

                    Date dbDate = rs.getTimestamp("last_login");
                    String fmDate = formatter.format(dbDate);
                    userInfo.setLastLogin(fmDate);

                    Date dDate = rs.getTimestamp("last_logout");
                    String fDate = formatter.format(dDate);
                    userInfo.setLastLogout(fDate);

                    userGroupInfo.setGroupId(rs.getInt("group_id"));
                    userGroupInfo.setGroupName(rs.getString("group_name"));
                    //                    
                    mobileRechargeDebitinfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitinfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    //
                    userInfo.setUserGroupInfo(userGroupInfo);
                    //
                    userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //
                    singlePaymentInfoList.add(userInfo);
                }
            } catch (Exception e) {
                singlePaymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return singlePaymentInfoList;
    }

    //
    public List<MobileRechargeTransactionHistory> getTransaction(Connection connection, String pId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> transactionHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectTransactionHistory(pId));
                rs = ps.executeQuery();

                transactionHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    mrTHistoryInfo = new MobileRechargeTransactionHistory();
                    //
                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY" + "\n" + "HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setBalanceReturnBy(rs.getString("bal_return_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    transactionHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                transactionHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return transactionHInfoList;
    }

    public List<MobileRechargeTransactionHistory> singleTransaction(Connection connection, String pId, Integer thId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> transactionHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleTransactionHistory(pId, thId));
                rs = ps.executeQuery();

                transactionHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    mrTHistoryInfo = new MobileRechargeTransactionHistory();
                    //
                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY" + "\n" + "HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setBalanceReturnBy(rs.getString("bal_return_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    transactionHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                transactionHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return transactionHInfoList;
    }

    //
    public List<MobileRechargeTransactionHistory> getMRPaymentReceiveHistory(Connection connection, String uId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> mrPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMRPaymentReceiveHistory(uId));
                rs = ps.executeQuery();

                mrPReceiveHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setBalanceReturnBy(rs.getString("bal_return_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    mrPReceiveHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                mrPReceiveHInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrPReceiveHInfoList;
    }

    public List<MobileRechargeTransactionHistory> getSingleMRPaymentReceiveHistory(Connection connection, String uId, Integer mrthId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> mrPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMRPaymentReceiveHistory(uId, mrthId));
                rs = ps.executeQuery();

                mrPReceiveHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    mrPReceiveHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                mrPReceiveHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrPReceiveHInfoList;
    }

    public List<MobileRechargeTransactionHistory> searchMRPaymentReceiveHistory(Connection connection, String uId, String fName, String fValue) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> searchMrPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMRPaymentReceiveHistory(uId, fName, fValue));
                rs = ps.executeQuery();

                searchMrPReceiveHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    searchMrPReceiveHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                searchMrPReceiveHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchMrPReceiveHInfoList;
    }

    //
    public List<MobileRechargeTransactionHistory> getMRPaymentMadeHistory(Connection connection, String uId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> mrPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMRPaymentMadeHistory(uId));
                rs = ps.executeQuery();

                mrPMadeHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    mrPMadeHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                mrPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrPMadeHInfoList;
    }

    public List<MobileRechargeTransactionHistory> getSingleMRPaymentMadeHistory(Connection connection, String uId, Integer mrthId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> mrPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMRPaymentMadeHistory(uId, mrthId));
                rs = ps.executeQuery();

                mrPMadeHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    mrPMadeHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                mrPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrPMadeHInfoList;
    }

    public List<MobileRechargeTransactionHistory> searchMRPaymentMadeHistory(Connection connection, String uId, String fName, String fValue) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        MobileRechargeTransactionHistory mrTHistoryInfo = null;
        List<MobileRechargeTransactionHistory> searchMrPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMRPaymentMadeHistory(uId, fName, fValue));
                rs = ps.executeQuery();

                searchMrPMadeHInfoList = new ArrayList<MobileRechargeTransactionHistory>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mrTHistoryInfo = new MobileRechargeTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mrTHistoryInfo.setMrTransactionHistoryId(rs.getInt("mr_tr_his_id"));
                    mrTHistoryInfo.setMrCurrentBalance(rs.getDouble("mr_cur_bal"));
                    mrTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mrTHistoryInfo.setTrid(rs.getString("trid"));
                    mrTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mrTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mrTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mrTHistoryInfo.setMrBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mrTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mrTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mrTHistoryInfo.setUserInfo(userInfo);
                    //
                    searchMrPMadeHInfoList.add(mrTHistoryInfo);
                }
            } catch (Exception e) {
                searchMrPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchMrPMadeHInfoList;
    }

    //
    public List<MobileMoneyTransactionHistory> getMMPaymentReceiveHistory(Connection connection, String uId) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> mmPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMMPaymentReceiveHistory(uId));
                rs = ps.executeQuery();

                mmPReceiveHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    mmPReceiveHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                mmPReceiveHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mmPReceiveHInfoList;
    }

    public List<MobileMoneyTransactionHistory> getSingleMMPaymentReceiveHistory(Connection connection, String uId, Integer mmthId) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> mmPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMMPaymentReceiveHistory(uId, mmthId));
                rs = ps.executeQuery();

                mmPReceiveHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    mmPReceiveHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                mmPReceiveHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mmPReceiveHInfoList;
    }

    public List<MobileMoneyTransactionHistory> searchMMPaymentReceiveHistory(Connection connection, String uId, String fName, String fValue) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> searchMMPReceiveHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMMPaymentReceiveHistory(uId, fName, fValue));
                rs = ps.executeQuery();

                searchMMPReceiveHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    searchMMPReceiveHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                searchMMPReceiveHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchMMPReceiveHInfoList;
    }

    //
    public List<MobileMoneyTransactionHistory> getMMPaymentMadeHistory(Connection connection, String uId) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> mmPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMMPaymentMadeHistory(uId));
                rs = ps.executeQuery();

                mmPMadeHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    mmPMadeHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                mmPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mmPMadeHInfoList;
    }

    public List<MobileMoneyTransactionHistory> getSingleMMPaymentMadeHistory(Connection connection, String uId, Integer mmthId) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> mmPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleMMPaymentMadeHistory(uId, mmthId));
                rs = ps.executeQuery();

                mmPMadeHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    mmPMadeHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                mmPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mmPMadeHInfoList;
    }

    public List<MobileMoneyTransactionHistory> searchMMPaymentMadeHistory(Connection connection, String uId, String fName, String fValue) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;

        UserInfo userInfo = null;
        MobileMoneyTransactionHistory mmTHistoryInfo = null;
        List<MobileMoneyTransactionHistory> searchMMPMadeHInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMMPaymentMadeHistory(uId, fName, fValue));
                rs = ps.executeQuery();

                searchMMPMadeHInfoList = new ArrayList<MobileMoneyTransactionHistory>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();

                    userInfo = new UserInfo();
                    mmTHistoryInfo = new MobileMoneyTransactionHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mmTHistoryInfo.setMmTransactionHistoryId(rs.getInt("mm_tr_his_id"));
                    mmTHistoryInfo.setMmCurrentBalance(rs.getDouble("mm_cur_bal"));
                    mmTHistoryInfo.setBalanceGivenBy(rs.getString("bal_given_by") == null ? rs.getString("bal_return_by") : rs.getString("bal_given_by"));
                    mmTHistoryInfo.setTrid(rs.getString("trid"));
                    mmTHistoryInfo.setAddBalance(rs.getDouble("bal_add") == 0d ? rs.getDouble("bal_return") : rs.getDouble("bal_add"));
                    mmTHistoryInfo.setOldBalance(rs.getDouble("old_bal"));
                    mmTHistoryInfo.setType(rs.getString("type").charAt(0));
                    mmTHistoryInfo.setMmBalanceDescription(rs.getString("memo"));

                    Date dbDate = (rs.getTimestamp("bal_given_date") == null ? rs.getTimestamp("bal_return_date") : rs.getTimestamp("bal_given_date"));
                    if (dbDate == null) {
                        mmTHistoryInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mmTHistoryInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    userInfo.setUserId(rs.getString("user_id"));
                    userInfo.setUserName(rs.getString("user_name"));
                    userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                    userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                    userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                    //
                    mmTHistoryInfo.setUserInfo(userInfo);
                    //
                    searchMMPMadeHInfoList.add(mmTHistoryInfo);
                }
            } catch (Exception e) {
                searchMMPMadeHInfoList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchMMPMadeHInfoList;
    }

    //
    public List<MobileRechargeHistory> searchMobileRechargeHistoryInfo(Connection connection, String uId, String fName, String fValue) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> mRechargeHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMRecharge(uId, fName, fValue));
                rs = ps.executeQuery();

                mRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));

                    DateFormat formatter2 = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    DateFormat formatter = new SimpleDateFormat("YYYY/MM/dd");
                    //                    
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter2.format(dbDate);
                    mobileRechargeHistoryInfo.setPurchasedOn(fDate);

                    mobileRechargeHistoryInfo.setSender(rs.getString("sender"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setOperator(rs.getString("operator"));
                    mobileRechargeHistoryInfo.setType(rs.getInt("type"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));
                    mobileRechargeHistoryInfo.setTnxid(rs.getString("tnxid"));
                    mobileRechargeHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileRechargeHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    mRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mRechargeHistoryInfoList;
    }

    public List<MobileMoneyHistory> searchMobileMoneyHistoryInfo(Connection connection, String uId, String fName, String fValue) {

        MobileMoneyHistory mobileMoneyHistoryInfo = null;
        List<MobileMoneyHistory> mobileMoneyHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchMMoney(uId, fName, fValue));
                rs = ps.executeQuery();

                mobileMoneyHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mobileMoneyHistoryInfo = new MobileMoneyHistory();

                    mobileMoneyHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    Date dbDate = rs.getTimestamp("purchased_on");
                    String fDate = formatter.format(dbDate);
                    mobileMoneyHistoryInfo.setPurchasedOn(fDate);

                    mobileMoneyHistoryInfo.setSender(rs.getString("sender"));
                    mobileMoneyHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileMoneyHistoryInfo.setOperator(rs.getString("operator"));
                    mobileMoneyHistoryInfo.setType(rs.getString("type"));
                    mobileMoneyHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileMoneyHistoryInfo.setOperatorBalance(rs.getDouble("operator_balance"));
                    mobileMoneyHistoryInfo.setTotalAmount(rs.getDouble("total_amount"));
                    mobileMoneyHistoryInfo.setTrid(rs.getString("trid"));
                    mobileMoneyHistoryInfo.setOriginator(rs.getString("originator"));
                    mobileMoneyHistoryInfo.setActiveStatus(rs.getString("active_status").charAt(0));

                    mobileMoneyHistoryInfoList.add(mobileMoneyHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return mobileMoneyHistoryInfoList;
    }

    public List<MobileRechargeDebit> mrCurBal(Connection connection, String uId) {

        MobileRechargeDebit mobileRechargeDebitInfo = null;
        List<MobileRechargeDebit> mrCurBalList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMRCurBal(uId));
                rs = ps.executeQuery();

                mrCurBalList = new ArrayList<MobileRechargeDebit>();

                while (rs.next()) {

                    mobileRechargeDebitInfo = new MobileRechargeDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mobileRechargeDebitInfo.setMobileRechargeDebitId(rs.getInt("mr_debit_id"));
                    mobileRechargeDebitInfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal")) == 0d ? 0d : (rs.getDouble("mr_cur_bal")));
                    mobileRechargeDebitInfo.setBalanceGivenBy(rs.getString("bal_given_by"));
                    mobileRechargeDebitInfo.setTrid(rs.getString("trid"));
                    mobileRechargeDebitInfo.setAddBalance(rs.getDouble("bal_add"));
                    mobileRechargeDebitInfo.setOldBalance(rs.getDouble("old_bal"));
                    mobileRechargeDebitInfo.setType(rs.getString("type").charAt(0));
                    mobileRechargeDebitInfo.setMrBalanceDescription(rs.getString("mr_bal_description"));

                    Date dbDate = ((rs.getTimestamp("bal_given_date")) == null ? null : (rs.getTimestamp("bal_given_date")));
                    if (dbDate == null) {
                        mobileRechargeDebitInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mobileRechargeDebitInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    mrCurBalList.add(mobileRechargeDebitInfo);
                }
            } catch (Exception e) {
                mrCurBalList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mrCurBalList;
    }

    public List<MobileMoneyDebit> mmCurBal(Connection connection, String uId) {

        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<MobileMoneyDebit> mmCurBalList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMMCurBal(uId));
                rs = ps.executeQuery();

                mmCurBalList = new ArrayList<MobileMoneyDebit>();

                while (rs.next()) {

                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    mobileMoneyDebitInfo.setMobileMoneyDebitId(rs.getInt("mm_debit_id"));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal")) == 0d ? 0d : (rs.getDouble("mm_cur_bal")));
                    mobileMoneyDebitInfo.setBalanceGivenBy(rs.getString("bal_given_by"));
                    mobileMoneyDebitInfo.setTrid(rs.getString("trid"));
                    mobileMoneyDebitInfo.setAddBalance(rs.getDouble("bal_add"));
                    mobileMoneyDebitInfo.setOldBalance(rs.getDouble("old_bal"));
                    mobileMoneyDebitInfo.setType(rs.getString("type").charAt(0));
                    mobileMoneyDebitInfo.setMmBalanceDescription(rs.getString("mm_bal_description"));

                    Date dbDate = ((rs.getTimestamp("bal_given_date")) == null ? null : (rs.getTimestamp("bal_given_date")));
                    if (dbDate == null) {
                        mobileMoneyDebitInfo.setBalanceGivenDate("");
                    } else {
                        String sDate = formatter.format(dbDate);
                        mobileMoneyDebitInfo.setBalanceGivenDate(sDate);
                    }
                    //
                    mmCurBalList.add(mobileMoneyDebitInfo);
                }
            } catch (Exception e) {
                mmCurBalList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return mmCurBalList;
    }

    public List<BalanceRequestHistory> getBalanceRequestReceive(Connection connection, String uId) {

        BalanceRequestHistory balanceRequestHistoryInfo = null;
        List<BalanceRequestHistory> balanceRequestReceiveInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectBalanceRequestReceiveHistory(uId));
                rs = ps.executeQuery();

                balanceRequestReceiveInfoList = new ArrayList<BalanceRequestHistory>();

                while (rs.next()) {

                    balanceRequestHistoryInfo = new BalanceRequestHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    balanceRequestHistoryInfo.setBalanceRequestId(rs.getInt("bal_request_id"));
                    balanceRequestHistoryInfo.setAmount((rs.getDouble("amount")) == 0d ? 0d : (rs.getDouble("amount")));
                    balanceRequestHistoryInfo.setBalanceType(rs.getString("bal_type"));
                    balanceRequestHistoryInfo.setRequestSender(rs.getString("user_id"));
                    balanceRequestHistoryInfo.setRequestStatus(rs.getString("request_status").charAt(0));
                    balanceRequestHistoryInfo.setRequestResponseBy(rs.getString("response_by"));
                    balanceRequestHistoryInfo.setRequestReceiver(rs.getString("request_to"));
                    balanceRequestHistoryInfo.setTrid(rs.getString("trid"));

                    Date rtDate = ((rs.getTimestamp("request_time")) == null ? null : (rs.getTimestamp("request_time")));
                    if (rtDate == null) {
                        balanceRequestHistoryInfo.setRequestTime("");
                    } else {
                        String sDate = formatter.format(rtDate);
                        balanceRequestHistoryInfo.setRequestTime(sDate);
                    }
                    //
                    Date rstDate = ((rs.getTimestamp("request_success_time")) == null ? null : (rs.getTimestamp("request_success_time")));
                    if (rstDate == null) {
                        balanceRequestHistoryInfo.setRequestSuccessTime("");
                    } else {
                        String sDate = formatter.format(rstDate);
                        balanceRequestHistoryInfo.setRequestSuccessTime(sDate);
                    }
                    //
                    balanceRequestReceiveInfoList.add(balanceRequestHistoryInfo);
                }
            } catch (Exception e) {
                balanceRequestReceiveInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return balanceRequestReceiveInfoList;
    }

    public List<BalanceRequestHistory> getSingleBalanceRequestReceive(Connection connection, String uId, Integer brhId) {

        BalanceRequestHistory balanceRequestHistoryInfo = null;
        List<BalanceRequestHistory> singleBalanceRequestReceiveInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleBalanceRequestReceiveHistory(uId, brhId));
                rs = ps.executeQuery();

                singleBalanceRequestReceiveInfoList = new ArrayList<BalanceRequestHistory>();

                while (rs.next()) {

                    balanceRequestHistoryInfo = new BalanceRequestHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    balanceRequestHistoryInfo.setBalanceRequestId(rs.getInt("bal_request_id"));
                    balanceRequestHistoryInfo.setAmount((rs.getDouble("amount")) == 0d ? 0d : (rs.getDouble("amount")));
                    balanceRequestHistoryInfo.setBalanceType(rs.getString("bal_type"));
                    balanceRequestHistoryInfo.setRequestSender(rs.getString("user_id"));
                    balanceRequestHistoryInfo.setRequestStatus(rs.getString("request_status").charAt(0));
                    balanceRequestHistoryInfo.setRequestResponseBy(rs.getString("response_by"));
                    balanceRequestHistoryInfo.setRequestReceiver(rs.getString("request_to"));
                    balanceRequestHistoryInfo.setTrid(rs.getString("trid"));

                    Date rtDate = ((rs.getTimestamp("request_time")) == null ? null : (rs.getTimestamp("request_time")));
                    if (rtDate == null) {
                        balanceRequestHistoryInfo.setRequestTime("");
                    } else {
                        String sDate = formatter.format(rtDate);
                        balanceRequestHistoryInfo.setRequestTime(sDate);
                    }
                    //
                    Date rstDate = ((rs.getTimestamp("request_success_time")) == null ? null : (rs.getTimestamp("request_success_time")));
                    if (rstDate == null) {
                        balanceRequestHistoryInfo.setRequestSuccessTime("");
                    } else {
                        String sDate = formatter.format(rstDate);
                        balanceRequestHistoryInfo.setRequestSuccessTime(sDate);
                    }
                    //
                    singleBalanceRequestReceiveInfoList.add(balanceRequestHistoryInfo);
                }
            } catch (Exception e) {
                singleBalanceRequestReceiveInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return singleBalanceRequestReceiveInfoList;
    }

    public List<BalanceRequestHistory> searchBalanceRequestHistoryInfo(Connection connection, String uId, String fName, String fValue) {

        BalanceRequestHistory balanceRequestHistoryInfo = null;
        List<BalanceRequestHistory> searchBalReqHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.searchBalanceRequestHistory(uId, fName, fValue));
                rs = ps.executeQuery();

                searchBalReqHistoryInfoList = new ArrayList<BalanceRequestHistory>();

                while (rs.next()) {

                    balanceRequestHistoryInfo = new BalanceRequestHistory();

                    DateFormat formatter = new SimpleDateFormat("MMM dd YYYY " + "|" + " HH:mm:ss");
                    //
                    balanceRequestHistoryInfo.setBalanceRequestId(rs.getInt("bal_request_id"));
                    balanceRequestHistoryInfo.setAmount((rs.getDouble("amount")) == 0d ? 0d : (rs.getDouble("amount")));
                    balanceRequestHistoryInfo.setBalanceType(rs.getString("bal_type"));
                    balanceRequestHistoryInfo.setRequestSender(rs.getString("user_id"));
                    balanceRequestHistoryInfo.setRequestStatus(rs.getString("request_status").charAt(0));
                    balanceRequestHistoryInfo.setRequestResponseBy(rs.getString("response_by"));
                    balanceRequestHistoryInfo.setRequestReceiver(rs.getString("request_to"));
                    balanceRequestHistoryInfo.setTrid(rs.getString("trid"));

                    Date rtDate = ((rs.getTimestamp("request_time")) == null ? null : (rs.getTimestamp("request_time")));
                    if (rtDate == null) {
                        balanceRequestHistoryInfo.setRequestTime("");
                    } else {
                        String sDate = formatter.format(rtDate);
                        balanceRequestHistoryInfo.setRequestTime(sDate);
                    }
                    //
                    Date rstDate = ((rs.getTimestamp("request_success_time")) == null ? null : (rs.getTimestamp("request_success_time")));
                    if (rstDate == null) {
                        balanceRequestHistoryInfo.setRequestSuccessTime("");
                    } else {
                        String sDate = formatter.format(rstDate);
                        balanceRequestHistoryInfo.setRequestSuccessTime(sDate);
                    }
                    //
                    searchBalReqHistoryInfoList.add(balanceRequestHistoryInfo);
                }
            } catch (Exception e) {
                searchBalReqHistoryInfoList = null;
                System.out.println(e);
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return searchBalReqHistoryInfoList;
    }

    public List<MobileRechargeHistory> getMobileRechargeWatingStatusInfo(Connection connection) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        UserInfo userInfo = null;
        MobileOperatorCodeInfo moci = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMobileRechargeWatingStatusInfo());
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    userInfo = new UserInfo();
                    moci = new MobileOperatorCodeInfo();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));

                    userInfo.setUserId(rs.getString("user_id"));
                    mobileRechargeHistoryInfo.setUserInfo(userInfo);

                    moci.setCountry(rs.getString("country"));
                    moci.setOperator(rs.getString("operator"));
                    moci.setOperatorCode(rs.getString("operator_code"));
                    mobileRechargeHistoryInfo.setMobileOperatorCodeInfo(moci);

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileRechargeHistory> getMobileRechargeSentStatusInfo(Connection connection) {

        MobileRechargeHistory mobileRechargeHistoryInfo = null;
        List<MobileRechargeHistory> lastMobileRechargeHistoryInfoList = null;

        UserInfo userInfo = null;
        MobileOperatorCodeInfo moci = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMobileRechargeSentStatusInfo());
                rs = ps.executeQuery();

                lastMobileRechargeHistoryInfoList = new ArrayList<MobileRechargeHistory>();

                while (rs.next()) {

                    mobileRechargeHistoryInfo = new MobileRechargeHistory();

                    userInfo = new UserInfo();
                    moci = new MobileOperatorCodeInfo();

                    mobileRechargeHistoryInfo.setMobileRechargeId(rs.getInt("mobile_recharge_id"));
                    mobileRechargeHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileRechargeHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileRechargeHistoryInfo.setTrid(rs.getString("trid"));

                    userInfo.setUserId(rs.getString("user_id"));
                    mobileRechargeHistoryInfo.setUserInfo(userInfo);

                    moci.setCountry(rs.getString("country"));
                    moci.setOperator(rs.getString("operator"));
                    moci.setOperatorCode(rs.getString("operator_code"));
                    mobileRechargeHistoryInfo.setMobileOperatorCodeInfo(moci);

                    lastMobileRechargeHistoryInfoList.add(mobileRechargeHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return lastMobileRechargeHistoryInfoList;
    }

    public List<MobileMoneyHistory> selectMobileMoneyWatingStatusInfo(Connection connection) {

        MobileMoneyHistory mobileMoneyHistoryInfo = null;
        List<MobileMoneyHistory> allMobileMoneyHistoryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectMobileMoneyWatingStatusInfo());
                rs = ps.executeQuery();

                allMobileMoneyHistoryInfoList = new ArrayList<MobileMoneyHistory>();

                while (rs.next()) {

                    mobileMoneyHistoryInfo = new MobileMoneyHistory();

                    mobileMoneyHistoryInfo.setMobileMoneyId(rs.getInt("mobile_money_id"));
                    mobileMoneyHistoryInfo.setReceiver(rs.getString("receiver"));
                    mobileMoneyHistoryInfo.setGivenBalance(rs.getDouble("given_balance"));
                    mobileMoneyHistoryInfo.setTrid(rs.getString("trid"));

                    allMobileMoneyHistoryInfoList.add(mobileMoneyHistoryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return allMobileMoneyHistoryInfoList;
    }

    public List<UserInfo> getResellerInfoByParent(Connection connection, String pID) {

        List<UserInfo> resellerByParentInfoList = null;

        for (int i = 0; i <= 5;) {

            UserGroup userGroupInfo = null;
            ManageRate manageRateInfo = null;
            UserInfo userInfo = null;
            MobileRechargeDebit mobileRechargeDebitinfo = null;
            MobileMoneyDebit mobileMoneyDebitInfo = null;

            PreparedStatement ps = null;
            ResultSet rs = null;

            if (connection != null) {

                try {

                    ps = connection.prepareStatement(QueryStatement.selectResellerInfoByParent(pID));
                    rs = ps.executeQuery();

                    resellerByParentInfoList = new ArrayList<UserInfo>();

                    while (rs.next()) {

                        userGroupInfo = new UserGroup();
                        manageRateInfo = new ManageRate();
                        userInfo = new UserInfo();
                        mobileRechargeDebitinfo = new MobileRechargeDebit();
                        mobileMoneyDebitInfo = new MobileMoneyDebit();

                        DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                        //
                        userInfo.setUserId(rs.getString("user_id") == null ? "" : rs.getString("user_id"));
                        userInfo.setParentId(rs.getString("parent_id"));
                        userInfo.setUserName(rs.getString("user_name"));
                        userInfo.setContactNumber1(rs.getString("contact_number1"));
                        userInfo.setEmailAddress(rs.getString("email_address"));
                        userInfo.setActiveStatus(rs.getString("active_status").charAt(0));
                        userInfo.setSuspendActivity(rs.getString("suspend_activity").charAt(0));
                        userInfo.setInsertBy(rs.getString("insert_by"));
                        userInfo.setPinConfirmation(rs.getString("pin_conf_status"));

                        Date cDate = rs.getTimestamp("insert_date");
                        String cfDate = formatter.format(cDate);
                        userInfo.setInsertDate(cfDate);

                        userInfo.setOtpStatus(rs.getString("otp_status").charAt(0));
                        userInfo.setEnablePinStatus(rs.getString("enable_pin_status"));
                        userInfo.setApiAccessStatus(rs.getString("api_access_status").charAt(0));
                        userInfo.setEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                        Date dbDate = (rs.getTimestamp("last_login") == null ? null : rs.getTimestamp("last_login"));
                        if (dbDate == null) {
                            userInfo.setLastLogin("");
                        } else {
                            String fmDate = formatter.format(dbDate);
                            userInfo.setLastLogin(fmDate);
                        }

                        userInfo.setServiceMRecharge(rs.getString("service_mrecharge"));
                        userInfo.setServiceMMoney(rs.getString("service_mmoney"));
                        //
                        userGroupInfo.setGroupId((rs.getInt("group_id") == 0 ? 0 : rs.getInt("group_id")));
                        userGroupInfo.setGroupName((rs.getString("group_name") == null ? "" : rs.getString("group_name").isEmpty() ? "" : rs.getString("group_name")));
                        //
                        manageRateInfo.setManageRateId((rs.getInt("manage_rate_id") == 0 ? 0 : rs.getInt("manage_rate_id")));
                        manageRateInfo.setRateName((rs.getString("rate_name") == null ? "" : rs.getString("rate_name").isEmpty() ? "" : rs.getString("rate_name")));
                        //
                        mobileRechargeDebitinfo.setMobileRechargeDebitId((rs.getInt("mr_debit_id") == 0 ? 0 : rs.getInt("mr_debit_id")));
                        mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal") == 0d ? 0d : rs.getDouble("mr_cur_bal")));
                        //  
                        mobileMoneyDebitInfo.setMobileMoneyDebitId((rs.getInt("mm_debit_id") == 0 ? 0 : rs.getInt("mm_debit_id")));
                        mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal") == 0d ? 0d : rs.getDouble("mm_cur_bal")));
                        //                    
                        userInfo.setUserGroupInfo(userGroupInfo);
                        //                    
                        userInfo.setManageRateInfo(manageRateInfo);
                        //
                        userInfo.setMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                        //    
                        userInfo.setMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                        //                    
//                    userInfo.setChildUserInfo(getResellerChldByParent(connection, rs.getString("user_id")));
                        //   

                        resellerByParentInfoList.add(userInfo);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println(e);
                    resellerByParentInfoList = null;
                } finally {
                    try {
                        if (rs != null) {
                            rs.close();
                        }
                        if (ps != null) {
                            ps.close();
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }

            System.out.println(resellerByParentInfoList);

//            Connection c = DBConnection.getMySqlConnection();
//            getResellerInfoByParent(c, pID);
        }

        return resellerByParentInfoList;
    }

    public List<ChildUserInfo> getResellerChldByParent(Connection connection, String chldID) {

        ChildUserInfo childUserInfo = null;
        UserGroup userGroupInfo = null;
        ManageRate manageRateInfo = null;
        MobileRechargeDebit mobileRechargeDebitinfo = null;
        MobileMoneyDebit mobileMoneyDebitInfo = null;
        List<ChildUserInfo> childUserInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectResellerByParent(chldID));
                rs = ps.executeQuery();

                childUserInfoList = new ArrayList<ChildUserInfo>();

                while (rs.next()) {

                    childUserInfo = new ChildUserInfo();
                    userGroupInfo = new UserGroup();
                    manageRateInfo = new ManageRate();
                    mobileRechargeDebitinfo = new MobileRechargeDebit();
                    mobileMoneyDebitInfo = new MobileMoneyDebit();

                    DateFormat formatter = new SimpleDateFormat("MMM dd,YYYY " + "|" + " HH:mm:ss");
                    //
                    childUserInfo.setChldUserId(rs.getString("user_id") == null ? "" : rs.getString("user_id"));
                    childUserInfo.setChldParentId(rs.getString("parent_id"));
                    childUserInfo.setChldUserName(rs.getString("user_name"));
                    childUserInfo.setChldContactNumber1(rs.getString("contact_number1"));
                    childUserInfo.setChldEmailAddress(rs.getString("email_address"));
                    childUserInfo.setChldActiveStatus(rs.getString("active_status").charAt(0));
                    childUserInfo.setChldSuspendActivity(rs.getString("suspend_activity").charAt(0));
                    childUserInfo.setChldInsertBy(rs.getString("insert_by"));
                    childUserInfo.setChldPinConfirmation(rs.getString("pin_conf_status"));

                    Date cDate = rs.getTimestamp("insert_date");
                    String cfDate = formatter.format(cDate);
                    childUserInfo.setChldInsertDate(cfDate);

                    childUserInfo.setChldOtpStatus(rs.getString("otp_status").charAt(0));
                    childUserInfo.setChldEnablePinStatus(rs.getString("enable_pin_status"));
                    childUserInfo.setChldApiAccessStatus(rs.getString("api_access_status").charAt(0));
                    childUserInfo.setChldEmailConfirmationStatus(rs.getString("email_conf_status").charAt(0));

                    Date dbDate = (rs.getTimestamp("last_login") == null ? null : rs.getTimestamp("last_login"));
                    if (dbDate == null) {
                        childUserInfo.setChldLastLogin("");
                    } else {
                        String fmDate = formatter.format(dbDate);
                        childUserInfo.setChldLastLogin(fmDate);
                    }

                    childUserInfo.setChldServiceMRecharge(rs.getString("service_mrecharge"));
                    childUserInfo.setChldServiceMMoney(rs.getString("service_mmoney"));
                    //
                    userGroupInfo.setGroupId((rs.getInt("group_id") == 0 ? 0 : rs.getInt("group_id")));
                    userGroupInfo.setGroupName((rs.getString("group_name") == null ? "" : rs.getString("group_name").isEmpty() ? "" : rs.getString("group_name")));
                    //
                    manageRateInfo.setManageRateId((rs.getInt("manage_rate_id") == 0 ? 0 : rs.getInt("manage_rate_id")));
                    manageRateInfo.setRateName((rs.getString("rate_name") == null ? "" : rs.getString("rate_name").isEmpty() ? "" : rs.getString("rate_name")));
                    //
                    mobileRechargeDebitinfo.setMobileRechargeDebitId((rs.getInt("mr_debit_id") == 0 ? 0 : rs.getInt("mr_debit_id")));
                    mobileRechargeDebitinfo.setMrCurrentBalance((rs.getDouble("mr_cur_bal") == 0d ? 0d : rs.getDouble("mr_cur_bal")));
                    //  
                    mobileMoneyDebitInfo.setMobileMoneyDebitId((rs.getInt("mm_debit_id") == 0 ? 0 : rs.getInt("mm_debit_id")));
                    mobileMoneyDebitInfo.setMmCurrentBalance((rs.getDouble("mm_cur_bal") == 0d ? 0d : rs.getDouble("mm_cur_bal")));
                    //                    
                    childUserInfo.setChldUserGroupInfo(userGroupInfo);
                    //                    
                    childUserInfo.setChldManageRateInfo(manageRateInfo);
                    //
                    childUserInfo.setChldMobileRechargeDebitInfo(mobileRechargeDebitinfo);
                    //    
                    childUserInfo.setChldMobileMoneyDebitInfo(mobileMoneyDebitInfo);
                    //                    
                    childUserInfoList.add(childUserInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                childUserInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return childUserInfoList;
    }

    public static void main(String[] args) {
        Connection c = DBConnection.getMySqlConnection();
        CommonList com = new CommonList();
        com.getResellerInfoByParent(c, "razu");
    }

}
