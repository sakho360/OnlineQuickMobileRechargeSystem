package com.common;

public interface ActionResult {

    public static final String ADMINUSERLOGINSUCCESS = "adminuserloginsuccess";
    public static final String RESELLERLOGINSUCCESS = "resellerloginsuccess";
    public static final String DBCONNECTIONFAILD = "dbconnectionfaild";

    public String execute() throws Exception;
}
