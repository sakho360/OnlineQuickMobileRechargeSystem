package com.model;

import com.common.QueryStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ManageRateDao {

    public int saveManageRateInfo(
            Connection connection,
            String rateName,
            Float grameenPhone,
            Float robi,
            Float banglalink,
            Float airtel,
            Float teletalk,
            Float bKash,
            Float dbbl,
            String uID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        try {

            ps = connection.prepareStatement(QueryStatement.insertManageRateStatement());

            ps.setString(1, rateName);
            ps.setFloat(2, grameenPhone);
            ps.setFloat(3, robi);
            ps.setFloat(4, banglalink);
            ps.setFloat(5, airtel);
            ps.setFloat(6, teletalk);
            ps.setFloat(7, bKash);
            ps.setFloat(8, dbbl);
            ps.setString(9, uID);
            ps.setString(10, insertBy);

            status = ps.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
            status = 0;
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return status;
    }

    public int updateManageRateInfo(
            Connection connection,
            String rateName,
            Float grameenPhone,
            Float robi,
            Float banglalink,
            Float airtel,
            Float teletalk,
            Float bKash,
            Float dbbl,
            String updateBy,
            String uID,
            Integer rateId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.updateManageRateStatement(uID, rateId));

                ps.setString(1, rateName);
                ps.setFloat(2, grameenPhone);
                ps.setFloat(3, robi);
                ps.setFloat(4, banglalink);
                ps.setFloat(5, airtel);
                ps.setFloat(6, teletalk);
                ps.setFloat(7, bKash);
                ps.setFloat(8, dbbl);
                ps.setString(9, updateBy);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int deleteManageRateInfo(
            Connection connection,
            String uID,
            Integer rateId) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.deleteManageRateStatement(uID, rateId));

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
