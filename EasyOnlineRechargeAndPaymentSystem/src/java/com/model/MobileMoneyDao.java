package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MobileMoneyDao {

    public int saveMobileMoney(
            Connection connection,
            String sender,
            String mobileNo,
            String type,
            Double amount,
            String operator,
            Double totalAmount,
            String uID) {
        
        int status = 0;

        PreparedStatement ps = null;
        
        if (connection != null) {

            String mmInsertStatement = " INSERT INTO "
                    + " mobile_money_history "
                    + " ( "
                    + " purchased_on, "
                    + " sender, "
                    + " receiver, "
                    + " type, "
                    + " given_balance, "
                    + " operator, "
                    + " total_amount, "
                    + " user_id,"
                    + " mmd_id "
                    + " ) VALUES ( "
                    + " CURRENT_TIMESTAMP ,?,?,?,?,?,?,?, (SELECT mm_debit_id FROM mobile_money_debit WHERE user_id = '" + uID + "') "
                    + " ) ";

            try {

                ps = connection.prepareStatement(mmInsertStatement);

                ps.setString(1, sender);
                ps.setString(2, mobileNo);
                ps.setString(3, type);
                ps.setDouble(4, amount);
                ps.setString(5, operator);
                ps.setDouble(6, totalAmount);
                ps.setString(7, uID);

                status = ps.executeUpdate();

            } catch (Exception e) {
                System.out.println(e);
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
