package com.persistance;

public class MobileRechargeHistory {

    private Integer mobileRechargeId;
    private String purchasedOn;
    private String sender;
    private String receiver;
    private String operator;
    private Integer type;
    private Double givenBalance;
    private Double operatorBalance;
    private String trid;
    private String tnxid;
    private String originator;
    private Character activeStatus;
    private UserInfo userInfo;
    private UserGroup userGroupInfo;
    private MobileRechargeDebit mobileRechargeDebitInfo;
    private MobileOperatorCodeInfo mobileOperatorCodeInfo;

    @Override
    public String toString() {
        return "MobileRechargeHistory{" + "mobileRechargeId=" + mobileRechargeId + ", purchasedOn=" + purchasedOn + ", sender=" + sender + ", receiver=" + receiver + ", operator=" + operator + ", type=" + type + ", givenBalance=" + givenBalance + ", operatorBalance=" + operatorBalance + ", trid=" + trid + ", tnxid=" + tnxid + ", originator=" + originator + ", activeStatus=" + activeStatus + ", userInfo=" + userInfo + ", userGroupInfo=" + userGroupInfo + ", mobileRechargeDebitInfo=" + mobileRechargeDebitInfo + ", mobileOperatorCodeInfo=" + mobileOperatorCodeInfo + '}';
    }

    /**
     * @return the mobileRechargeId
     */
    public Integer getMobileRechargeId() {
        return mobileRechargeId;
    }

    /**
     * @param mobileRechargeId the mobileRechargeId to set
     */
    public void setMobileRechargeId(Integer mobileRechargeId) {
        this.mobileRechargeId = mobileRechargeId;
    }

    /**
     * @return the purchasedOn
     */
    public String getPurchasedOn() {
        return purchasedOn;
    }

    /**
     * @param purchasedOn the purchasedOn to set
     */
    public void setPurchasedOn(String purchasedOn) {
        this.purchasedOn = purchasedOn;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return the receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the givenBalance
     */
    public Double getGivenBalance() {
        return givenBalance;
    }

    /**
     * @param givenBalance the givenBalance to set
     */
    public void setGivenBalance(Double givenBalance) {
        this.givenBalance = givenBalance;
    }

    /**
     * @return the operatorBalance
     */
    public Double getOperatorBalance() {
        return operatorBalance;
    }

    /**
     * @param operatorBalance the operatorBalance to set
     */
    public void setOperatorBalance(Double operatorBalance) {
        this.operatorBalance = operatorBalance;
    }

    /**
     * @return the trid
     */
    public String getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(String trid) {
        this.trid = trid;
    }

    /**
     * @return the tnxid
     */
    public String getTnxid() {
        return tnxid;
    }

    /**
     * @param tnxid the tnxid to set
     */
    public void setTnxid(String tnxid) {
        this.tnxid = tnxid;
    }

    /**
     * @return the originator
     */
    public String getOriginator() {
        return originator;
    }

    /**
     * @param originator the originator to set
     */
    public void setOriginator(String originator) {
        this.originator = originator;
    }

    /**
     * @return the activeStatus
     */
    public Character getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(Character activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the mobileRechargeDebitInfo
     */
    public MobileRechargeDebit getMobileRechargeDebitInfo() {
        return mobileRechargeDebitInfo;
    }

    /**
     * @param mobileRechargeDebitInfo the mobileRechargeDebitInfo to set
     */
    public void setMobileRechargeDebitInfo(MobileRechargeDebit mobileRechargeDebitInfo) {
        this.mobileRechargeDebitInfo = mobileRechargeDebitInfo;
    }

    /**
     * @return the mobileOperatorCodeInfo
     */
    public MobileOperatorCodeInfo getMobileOperatorCodeInfo() {
        return mobileOperatorCodeInfo;
    }

    /**
     * @param mobileOperatorCodeInfo the mobileOperatorCodeInfo to set
     */
    public void setMobileOperatorCodeInfo(MobileOperatorCodeInfo mobileOperatorCodeInfo) {
        this.mobileOperatorCodeInfo = mobileOperatorCodeInfo;
    }
}
