package com.persistance;

import java.util.Date;

public class UserLevelChildMenu {

    private Integer userLevelMenuIdChild;
    private String menuIdChild;
    private Integer levelIdChild;
    private Character levelMenuStatusChild;
    private String insertByChild;
    private Date insertDateChild;
    private String updateByChild;
    private Date updateDateChild;
    private String comCodeChild;
    private ParentMenu parentMenuInfo;

    /**
     * @return the userLevelMenuIdChild
     */
    public Integer getUserLevelMenuIdChild() {
        return userLevelMenuIdChild;
    }

    /**
     * @param userLevelMenuIdChild the userLevelMenuIdChild to set
     */
    public void setUserLevelMenuIdChild(Integer userLevelMenuIdChild) {
        this.userLevelMenuIdChild = userLevelMenuIdChild;
    }

    /**
     * @return the menuIdChild
     */
    public String getMenuIdChild() {
        return menuIdChild;
    }

    /**
     * @param menuIdChild the menuIdChild to set
     */
    public void setMenuIdChild(String menuIdChild) {
        this.menuIdChild = menuIdChild;
    }

    /**
     * @return the levelIdChild
     */
    public Integer getLevelIdChild() {
        return levelIdChild;
    }

    /**
     * @param levelIdChild the levelIdChild to set
     */
    public void setLevelIdChild(Integer levelIdChild) {
        this.levelIdChild = levelIdChild;
    }

    /**
     * @return the levelMenuStatusChild
     */
    public Character getLevelMenuStatusChild() {
        return levelMenuStatusChild;
    }

    /**
     * @param levelMenuStatusChild the levelMenuStatusChild to set
     */
    public void setLevelMenuStatusChild(Character levelMenuStatusChild) {
        this.levelMenuStatusChild = levelMenuStatusChild;
    }

    /**
     * @return the insertByChild
     */
    public String getInsertByChild() {
        return insertByChild;
    }

    /**
     * @param insertByChild the insertByChild to set
     */
    public void setInsertByChild(String insertByChild) {
        this.insertByChild = insertByChild;
    }

    /**
     * @return the insertDateChild
     */
    public Date getInsertDateChild() {
        return insertDateChild;
    }

    /**
     * @param insertDateChild the insertDateChild to set
     */
    public void setInsertDateChild(Date insertDateChild) {
        this.insertDateChild = insertDateChild;
    }

    /**
     * @return the updateByChild
     */
    public String getUpdateByChild() {
        return updateByChild;
    }

    /**
     * @param updateByChild the updateByChild to set
     */
    public void setUpdateByChild(String updateByChild) {
        this.updateByChild = updateByChild;
    }

    /**
     * @return the updateDateChild
     */
    public Date getUpdateDateChild() {
        return updateDateChild;
    }

    /**
     * @param updateDateChild the updateDateChild to set
     */
    public void setUpdateDateChild(Date updateDateChild) {
        this.updateDateChild = updateDateChild;
    }

    /**
     * @return the comCodeChild
     */
    public String getComCodeChild() {
        return comCodeChild;
    }

    /**
     * @param comCodeChild the comCodeChild to set
     */
    public void setComCodeChild(String comCodeChild) {
        this.comCodeChild = comCodeChild;
    }

    /**
     * @return the parentMenuInfo
     */
    public ParentMenu getParentMenuInfo() {
        return parentMenuInfo;
    }

    /**
     * @param parentMenuInfo the parentMenuInfo to set
     */
    public void setParentMenuInfo(ParentMenu parentMenuInfo) {
        this.parentMenuInfo = parentMenuInfo;
    }
}
