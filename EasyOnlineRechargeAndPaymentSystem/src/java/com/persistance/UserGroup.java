package com.persistance;

import java.util.Date;

public class UserGroup {

    private Integer groupId;
    private String groupName;
    private CompanyInfo companyInfo;
    private ManageRate manageRateInfo;
    private String groupDescription;
    private char activeStatus;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId the groupId to set
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    /**
     * @return the manageRateInfo
     */
    public ManageRate getManageRateInfo() {
        return manageRateInfo;
    }

    /**
     * @param manageRateInfo the manageRateInfo to set
     */
    public void setManageRateInfo(ManageRate manageRateInfo) {
        this.manageRateInfo = manageRateInfo;
    }

    /**
     * @return the groupDescription
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * @param groupDescription the groupDescription to set
     */
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * @return the activeStatus
     */
    public char getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(char activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
