package com.persistance;

public class BalanceRequestHistory {

    private Integer balanceRequestId;
    private Double amount;
    private String balanceType;
    private String requestSender;
    private String requestTime;
    private String requestSuccessTime;
    private Character requestStatus;
    private String requestResponseBy;
    private String requestReceiver;
    private String trid;
    private UserInfo userInfo;

    /**
     * @return the balanceRequestId
     */
    public Integer getBalanceRequestId() {
        return balanceRequestId;
    }

    /**
     * @param balanceRequestId the balanceRequestId to set
     */
    public void setBalanceRequestId(Integer balanceRequestId) {
        this.balanceRequestId = balanceRequestId;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the balanceType
     */
    public String getBalanceType() {
        return balanceType;
    }

    /**
     * @param balanceType the balanceType to set
     */
    public void setBalanceType(String balanceType) {
        this.balanceType = balanceType;
    }

    /**
     * @return the requestSender
     */
    public String getRequestSender() {
        return requestSender;
    }

    /**
     * @param requestSender the requestSender to set
     */
    public void setRequestSender(String requestSender) {
        this.requestSender = requestSender;
    }

    /**
     * @return the requestTime
     */
    public String getRequestTime() {
        return requestTime;
    }

    /**
     * @param requestTime the requestTime to set
     */
    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    /**
     * @return the requestSuccessTime
     */
    public String getRequestSuccessTime() {
        return requestSuccessTime;
    }

    /**
     * @param requestSuccessTime the requestSuccessTime to set
     */
    public void setRequestSuccessTime(String requestSuccessTime) {
        this.requestSuccessTime = requestSuccessTime;
    }

    /**
     * @return the requestStatus
     */
    public Character getRequestStatus() {
        return requestStatus;
    }

    /**
     * @param requestStatus the requestStatus to set
     */
    public void setRequestStatus(Character requestStatus) {
        this.requestStatus = requestStatus;
    }

    /**
     * @return the requestResponseBy
     */
    public String getRequestResponseBy() {
        return requestResponseBy;
    }

    /**
     * @param requestResponseBy the requestResponseBy to set
     */
    public void setRequestResponseBy(String requestResponseBy) {
        this.requestResponseBy = requestResponseBy;
    }

    /**
     * @return the requestReceiver
     */
    public String getRequestReceiver() {
        return requestReceiver;
    }

    /**
     * @param requestReceiver the requestReceiver to set
     */
    public void setRequestReceiver(String requestReceiver) {
        this.requestReceiver = requestReceiver;
    }

    /**
     * @return the trid
     */
    public String getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(String trid) {
        this.trid = trid;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
}
