package com.persistance;

public class CsvFileInfo {

    private String mobileNumber;
    private Integer type;
    private Integer amount;
    private Integer modemNumber;

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return the amount
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * @return the modemNumber
     */
    public Integer getModemNumber() {
        return modemNumber;
    }

    /**
     * @param modemNumber the modemNumber to set
     */
    public void setModemNumber(Integer modemNumber) {
        this.modemNumber = modemNumber;
    }
}
