package com.persistance;

public class AdminGroup {

    private Integer adminGroupId;
    private String adminGroupName;
    private Character adminGroupStatus;

    /**
     * @return the adminGroupId
     */
    public Integer getAdminGroupId() {
        return adminGroupId;
    }

    /**
     * @param adminGroupId the adminGroupId to set
     */
    public void setAdminGroupId(Integer adminGroupId) {
        this.adminGroupId = adminGroupId;
    }

    /**
     * @return the adminGroupName
     */
    public String getAdminGroupName() {
        return adminGroupName;
    }

    /**
     * @param adminGroupName the adminGroupName to set
     */
    public void setAdminGroupName(String adminGroupName) {
        this.adminGroupName = adminGroupName;
    }

    /**
     * @return the adminGroupStatus
     */
    public Character getAdminGroupStatus() {
        return adminGroupStatus;
    }

    /**
     * @param adminGroupStatus the adminGroupStatus to set
     */
    public void setAdminGroupStatus(Character adminGroupStatus) {
        this.adminGroupStatus = adminGroupStatus;
    }
}
