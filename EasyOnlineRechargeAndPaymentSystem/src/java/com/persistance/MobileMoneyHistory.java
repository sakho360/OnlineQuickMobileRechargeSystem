package com.persistance;

public class MobileMoneyHistory {

    private Integer mobileMoneyId;
    private String purchasedOn;
    private String sender;
    private String receiver;
    private String operator;
    private String type;
    private Double givenBalance;
    private Double operatorBalance;
    private Double totalAmount;
    private String trid;
    private String originator;
    private Character activeStatus;
    private UserInfo userInfo;
    private UserGroup userGroupInfo;
    private MobileMoneyDebit moneyDebitInfo;

    @Override
    public String toString() {
        return "MobileMoneyHistory{" + "mobileMoneyId=" + mobileMoneyId + ", purchasedOn=" + purchasedOn + ", sender=" + sender + ", receiver=" + receiver + ", operator=" + operator + ", type=" + type + ", givenBalance=" + givenBalance + ", operatorBalance=" + operatorBalance + ", totalAmount=" + totalAmount + ", trid=" + trid + ", originator=" + originator + ", activeStatus=" + activeStatus + ", userInfo=" + userInfo + ", userGroupInfo=" + userGroupInfo + ", moneyDebitInfo=" + moneyDebitInfo + '}';
    }

    /**
     * @return the mobileMoneyId
     */
    public Integer getMobileMoneyId() {
        return mobileMoneyId;
    }

    /**
     * @param mobileMoneyId the mobileMoneyId to set
     */
    public void setMobileMoneyId(Integer mobileMoneyId) {
        this.mobileMoneyId = mobileMoneyId;
    }

    /**
     * @return the purchasedOn
     */
    public String getPurchasedOn() {
        return purchasedOn;
    }

    /**
     * @param purchasedOn the purchasedOn to set
     */
    public void setPurchasedOn(String purchasedOn) {
        this.purchasedOn = purchasedOn;
    }

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * @return the receiver
     */
    public String getReceiver() {
        return receiver;
    }

    /**
     * @param receiver the receiver to set
     */
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    /**
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(String operator) {
        this.operator = operator;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the givenBalance
     */
    public Double getGivenBalance() {
        return givenBalance;
    }

    /**
     * @param givenBalance the givenBalance to set
     */
    public void setGivenBalance(Double givenBalance) {
        this.givenBalance = givenBalance;
    }

    /**
     * @return the operatorBalance
     */
    public Double getOperatorBalance() {
        return operatorBalance;
    }

    /**
     * @param operatorBalance the operatorBalance to set
     */
    public void setOperatorBalance(Double operatorBalance) {
        this.operatorBalance = operatorBalance;
    }

    /**
     * @return the totalAmount
     */
    public Double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the trid
     */
    public String getTrid() {
        return trid;
    }

    /**
     * @param trid the trid to set
     */
    public void setTrid(String trid) {
        this.trid = trid;
    }

    /**
     * @return the originator
     */
    public String getOriginator() {
        return originator;
    }

    /**
     * @param originator the originator to set
     */
    public void setOriginator(String originator) {
        this.originator = originator;
    }

    /**
     * @return the activeStatus
     */
    public Character getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(Character activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the moneyDebitInfo
     */
    public MobileMoneyDebit getMoneyDebitInfo() {
        return moneyDebitInfo;
    }

    /**
     * @param moneyDebitInfo the moneyDebitInfo to set
     */
    public void setMoneyDebitInfo(MobileMoneyDebit moneyDebitInfo) {
        this.moneyDebitInfo = moneyDebitInfo;
    }
}
