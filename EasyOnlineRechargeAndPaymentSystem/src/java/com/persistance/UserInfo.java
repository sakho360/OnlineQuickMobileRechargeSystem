package com.persistance;

import java.util.List;

public class UserInfo {

    private String userId;
    private String parentId;
    private String userName;
    private String userCompanyName;
    private String userPassword;
    private String contactNumber1;
    private String contactNumber2;
    private String emailAddress;
    private String address1;
    private String address2;
    private UserGroup userGroupInfo;
    private ManageRate manageRateInfo;
    private MobileRechargeDebit mobileRechargeDebitInfo;
    private MobileMoneyDebit mobileMoneyDebitInfo;
    private CompanyInfo companyInfo;
    private String groupDescription;
    private Character activeStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private String serviceMRecharge;
    private String serviceMMoney;
    private Character otpStatus;
    private String enablePinStatus;
    private Character apiAccessStatus;
    private Character emailConfirmationStatus;
    private String pinConfirmation;
    private String lastLogin;
    private String lastLogout;
    private Character suspendActivity;
    private List<ChildUserInfo> childUserInfo;

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userCompanyName
     */
    public String getUserCompanyName() {
        return userCompanyName;
    }

    /**
     * @param userCompanyName the userCompanyName to set
     */
    public void setUserCompanyName(String userCompanyName) {
        this.userCompanyName = userCompanyName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the contactNumber1
     */
    public String getContactNumber1() {
        return contactNumber1;
    }

    /**
     * @param contactNumber1 the contactNumber1 to set
     */
    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    /**
     * @return the contactNumber2
     */
    public String getContactNumber2() {
        return contactNumber2;
    }

    /**
     * @param contactNumber2 the contactNumber2 to set
     */
    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the manageRateInfo
     */
    public ManageRate getManageRateInfo() {
        return manageRateInfo;
    }

    /**
     * @param manageRateInfo the manageRateInfo to set
     */
    public void setManageRateInfo(ManageRate manageRateInfo) {
        this.manageRateInfo = manageRateInfo;
    }

    /**
     * @return the mobileRechargeDebitInfo
     */
    public MobileRechargeDebit getMobileRechargeDebitInfo() {
        return mobileRechargeDebitInfo;
    }

    /**
     * @param mobileRechargeDebitInfo the mobileRechargeDebitInfo to set
     */
    public void setMobileRechargeDebitInfo(MobileRechargeDebit mobileRechargeDebitInfo) {
        this.mobileRechargeDebitInfo = mobileRechargeDebitInfo;
    }

    /**
     * @return the mobileMoneyDebitInfo
     */
    public MobileMoneyDebit getMobileMoneyDebitInfo() {
        return mobileMoneyDebitInfo;
    }

    /**
     * @param mobileMoneyDebitInfo the mobileMoneyDebitInfo to set
     */
    public void setMobileMoneyDebitInfo(MobileMoneyDebit mobileMoneyDebitInfo) {
        this.mobileMoneyDebitInfo = mobileMoneyDebitInfo;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    /**
     * @return the groupDescription
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * @param groupDescription the groupDescription to set
     */
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * @return the activeStatus
     */
    public Character getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(Character activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the serviceMRecharge
     */
    public String getServiceMRecharge() {
        return serviceMRecharge;
    }

    /**
     * @param serviceMRecharge the serviceMRecharge to set
     */
    public void setServiceMRecharge(String serviceMRecharge) {
        this.serviceMRecharge = serviceMRecharge;
    }

    /**
     * @return the serviceMMoney
     */
    public String getServiceMMoney() {
        return serviceMMoney;
    }

    /**
     * @param serviceMMoney the serviceMMoney to set
     */
    public void setServiceMMoney(String serviceMMoney) {
        this.serviceMMoney = serviceMMoney;
    }

    /**
     * @return the otpStatus
     */
    public Character getOtpStatus() {
        return otpStatus;
    }

    /**
     * @param otpStatus the otpStatus to set
     */
    public void setOtpStatus(Character otpStatus) {
        this.otpStatus = otpStatus;
    }

    /**
     * @return the enablePinStatus
     */
    public String getEnablePinStatus() {
        return enablePinStatus;
    }

    /**
     * @param enablePinStatus the enablePinStatus to set
     */
    public void setEnablePinStatus(String enablePinStatus) {
        this.enablePinStatus = enablePinStatus;
    }

    /**
     * @return the apiAccessStatus
     */
    public Character getApiAccessStatus() {
        return apiAccessStatus;
    }

    /**
     * @param apiAccessStatus the apiAccessStatus to set
     */
    public void setApiAccessStatus(Character apiAccessStatus) {
        this.apiAccessStatus = apiAccessStatus;
    }

    /**
     * @return the emailConfirmationStatus
     */
    public Character getEmailConfirmationStatus() {
        return emailConfirmationStatus;
    }

    /**
     * @param emailConfirmationStatus the emailConfirmationStatus to set
     */
    public void setEmailConfirmationStatus(Character emailConfirmationStatus) {
        this.emailConfirmationStatus = emailConfirmationStatus;
    }

    /**
     * @return the pinConfirmation
     */
    public String getPinConfirmation() {
        return pinConfirmation;
    }

    /**
     * @param pinConfirmation the pinConfirmation to set
     */
    public void setPinConfirmation(String pinConfirmation) {
        this.pinConfirmation = pinConfirmation;
    }

    /**
     * @return the lastLogin
     */
    public String getLastLogin() {
        return lastLogin;
    }

    /**
     * @param lastLogin the lastLogin to set
     */
    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    /**
     * @return the lastLogout
     */
    public String getLastLogout() {
        return lastLogout;
    }

    /**
     * @param lastLogout the lastLogout to set
     */
    public void setLastLogout(String lastLogout) {
        this.lastLogout = lastLogout;
    }

    /**
     * @return the suspendActivity
     */
    public Character getSuspendActivity() {
        return suspendActivity;
    }

    /**
     * @param suspendActivity the suspendActivity to set
     */
    public void setSuspendActivity(Character suspendActivity) {
        this.suspendActivity = suspendActivity;
    }

    /**
     * @return the childUserInfo
     */
    public List<ChildUserInfo> getChildUserInfo() {
        return childUserInfo;
    }

    /**
     * @param childUserInfo the childUserInfo to set
     */
    public void setChildUserInfo(List<ChildUserInfo> childUserInfo) {
        this.childUserInfo = childUserInfo;
    }
}
