package com.persistance;

import java.util.Date;

public class MobileMoney {

    private Integer mobileCashId;
    private String simNumber;
    private Double maxLimit;
    private Double actualBalance;
    private Double givenBalance;
    private char activeStatus;
    private char bankingType;
    private UserInfo userInfo;
    private UserGroup userGroupInfo;
    private ManageRate manageRateInfo;
    private CompanyInfo companyInfo;
    private String groupDescription;
    private String insertBy;
    private Date insertDate;
    private String updateBy;
    private Date updateDate;

    /**
     * @return the mobileCashId
     */
    public Integer getMobileCashId() {
        return mobileCashId;
    }

    /**
     * @param mobileCashId the mobileCashId to set
     */
    public void setMobileCashId(Integer mobileCashId) {
        this.mobileCashId = mobileCashId;
    }

    /**
     * @return the simNumber
     */
    public String getSimNumber() {
        return simNumber;
    }

    /**
     * @param simNumber the simNumber to set
     */
    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    /**
     * @return the maxLimit
     */
    public Double getMaxLimit() {
        return maxLimit;
    }

    /**
     * @param maxLimit the maxLimit to set
     */
    public void setMaxLimit(Double maxLimit) {
        this.maxLimit = maxLimit;
    }

    /**
     * @return the actualBalance
     */
    public Double getActualBalance() {
        return actualBalance;
    }

    /**
     * @param actualBalance the actualBalance to set
     */
    public void setActualBalance(Double actualBalance) {
        this.actualBalance = actualBalance;
    }

    /**
     * @return the givenBalance
     */
    public Double getGivenBalance() {
        return givenBalance;
    }

    /**
     * @param givenBalance the givenBalance to set
     */
    public void setGivenBalance(Double givenBalance) {
        this.givenBalance = givenBalance;
    }

    /**
     * @return the activeStatus
     */
    public char getActiveStatus() {
        return activeStatus;
    }

    /**
     * @param activeStatus the activeStatus to set
     */
    public void setActiveStatus(char activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
     * @return the bankingType
     */
    public char getBankingType() {
        return bankingType;
    }

    /**
     * @param bankingType the bankingType to set
     */
    public void setBankingType(char bankingType) {
        this.bankingType = bankingType;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the manageRateInfo
     */
    public ManageRate getManageRateInfo() {
        return manageRateInfo;
    }

    /**
     * @param manageRateInfo the manageRateInfo to set
     */
    public void setManageRateInfo(ManageRate manageRateInfo) {
        this.manageRateInfo = manageRateInfo;
    }

    /**
     * @return the companyInfo
     */
    public CompanyInfo getCompanyInfo() {
        return companyInfo;
    }

    /**
     * @param companyInfo the companyInfo to set
     */
    public void setCompanyInfo(CompanyInfo companyInfo) {
        this.companyInfo = companyInfo;
    }

    /**
     * @return the groupDescription
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * @param groupDescription the groupDescription to set
     */
    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public Date getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
