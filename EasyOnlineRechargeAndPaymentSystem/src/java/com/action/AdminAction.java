package com.action;

import static com.common.ActionResult.ADMINUSERLOGINSUCCESS;
import com.common.CommonList;
import com.model.AdminDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ParentMenu;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import com.persistance.UserLevel;
import com.common.DBConnection;
import com.persistance.AdminGroup;
import com.persistance.AdminUser;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileRechargeHistory;
import com.persistance.MobileRechargeTransactionHistory;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class AdminAction extends ActionSupport {

    private Integer adminUserId;
    private String userRoleList;
    private String fullName;
    private String email;
    private String phone;
    private String searchPhone;
    private String userName;
    private String password;
    private String resellerId;
    private Integer mobileRechargeId;
    private Integer mobileMoneyId;
    //    
    private Integer levelId;
    private String groupFormString;
    private String levelName;
    //    
    private String messageString;
    private String messageColor;
    //    
    private CommonList commonList = null;
    private Connection connection = null;
    //    
    private Integer mrTransactionHistoryId;
    //    
    private double totalMRSale;
    private double totalResellerMrBal;
    private double totalResellerSubAdminMrBal;
    private double totalReseller4MrBal;
    private double totalReseller3MrBal;
    private double totalReseller2MrBal;
    private double totalReseller1MrBal;
    //    
    private double totalMMSale;
    private double totalResellerMmBal;
    private double totalResellerSubAdminMmBal;
    private double totalReseller4MmBal;
    private double totalReseller3MmBal;
    private double totalReseller2MmBal;
    private double totalReseller1MmBal;
    //    
    private MobileRechargeAction rechargeAction = null;
    private volatile boolean isMRThreadRunning;
    //    
    private List<AdminGroup> allAdminGroupInfoList = null;
    private List<AdminUser> allAdminUserInfoList = null;
    private List<AdminUser> viewOneAdminUserInfoList = null;
    private List<UserLevel> userLevelInfoList = null;
    private List<ParentMenu> parentMenuInfoList = null;
    private List<UserLevelMenu> userLevelMenuInfoList = null;
    //
    private List<MobileRechargeHistory> allMRNumber = null;
    private List<MobileRechargeHistory> allMRechargeHInfoList = null;
    private List<MobileRechargeHistory> singleMRHInfoList = null;
    //
    private List<MobileRechargeHistory> latestProcessingOrdersList = null;
    private List<MobileRechargeHistory> latestWaitingOrdersList = null;
    private List<MobileRechargeHistory> latestSuccessOrdersList = null;
    //
    private List<MobileMoneyHistory> allMMNumber = null;
    private List<MobileMoneyHistory> singleMMoneyHInfoList = null;
    private List<MobileMoneyHistory> allMMoneyHInfoList = null;
    //    
    private List<MobileRechargeTransactionHistory> mrTransactionHInfoList = null;
    private List<MobileRechargeTransactionHistory> singleMrTransactionHInfoList = null;
    //    
    private List<UserInfo> allResallersInfoList = null;
    private List<UserInfo> singleResallerInfoList = null;
    private List<UserInfo> onLineUserList = null;
    private List<UserInfo> paymentInfoList = null;
    private List<UserInfo> singlePaymentInfoList = null;
    //    
    private AdminDao adminDao = null;
    //  

    private List<UserInfo> allUserInfo = null;
    private List<UserGroup> userGroupInfoList = null;

    /**
     * *************************************************************************
     * ***************************** All List
     *
     ***************************************************************************
     * @return
     */
    public List<AdminGroup> getAllAdminGroup() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllAdminGroupInfoList(commonList.getAllAdminGroupInfo(connection));
        }

        return allAdminGroupInfoList;
    }

    public List<AdminUser> getAllAdminUser() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllAdminUserInfoList(commonList.getAllAdminUserInfo(connection));
        }

        return allAdminUserInfoList;
    }

    public List<UserLevel> getUserLevel() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setUserLevelInfoList(commonList.getAllUserLevelInfo(connection));
        }

        return userLevelInfoList;
    }

    public List<UserInfo> getAllResellers(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllResallersInfoList(commonList.getAllResellerInfo(connection, pId));
        }

        return allResallersInfoList;
    }

    public List<UserInfo> getAllOnlineUser() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setOnLineUserList(commonList.getOnlineUSer(connection));
        }

        return onLineUserList;
    }

    /**
     * *************************************************************************
     * *************** Admin User add,update,delete,view
     *
     ***************************************************************************
     * @return
     */
    public String showAdminUser() {

        getAllAdminUser();

        return SUCCESS;
    }

    public int checkDuplicateAdminUserName() {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT COUNT(user_name) "
                        + " TOTAL "
                        + " FROM "
                        + " admin_user "
                        + " WHERE "
                        + " user_name = '" + userName + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    public String addNewAdminUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (adminDao == null) {
            adminDao = new AdminDao();
        }

        if (connection != null) {

            if (checkDuplicateAdminUserName() == 0) {

                int checked = adminDao.saveUserAdminInfo(connection, userID, fullName, email, phone, userName, password, userRoleList, userID);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Admin User Added Successfully.";
                } else {
                    messageColor = "danger";
                    messageString = "Admin User Add Failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Admin UserName Already exist, Please use different UserName !";
            }
        }

        return SUCCESS;
    }

    public String adminUserDetailsView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setViewOneAdminUserInfoList(commonList.getOneAdminUserInfo(connection, adminUserId));
        }

        getAllAdminGroup();

        getAllAdminUser();

        return SUCCESS;
    }

    public String updateAdminUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (adminDao == null) {
            adminDao = new AdminDao();
        }

        if (connection != null) {

            int checked = adminDao.updateUserAdminInfo(connection, fullName, email, phone, password, userRoleList, userID, adminUserId);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Admin User Updated Successfully.";
            } else {
                messageColor = "danger";
                messageString = "Admin User Update Failed !";
            }
        }

        return SUCCESS;
    }

    public String deleteAdminUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (adminDao == null) {
            adminDao = new AdminDao();
        }

        if (connection != null) {

            int checked = adminDao.deleteUserAdminInfo(connection, adminUserId);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Admin User Deleted Successfully.";
            } else {
                messageColor = "danger";
                messageString = "Admin User Delete Failed !";
            }
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ***************************** Menu Controls
     *
     ***************************************************************************
     * @return
     */
    public List<ParentMenu> getAllMenu() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setParentMenuInfoList(commonList.getAllParentMenuInfo(connection));
        }

        return parentMenuInfoList;
    }

    public List<UserLevelMenu> getUserLevelMenu() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setUserLevelMenuInfoList(commonList.getUserLevelMenu(connection, levelId));
        }

        return userLevelMenuInfoList;
    }

    public String selectedGroupLevelName() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<UserLevelMenu> checked = commonList.getUserLevelMenu(connection, levelId);

            System.out.println(checked);

            if (checked.size() != 0) {
                getUserLevelMenu();
            } else {
                getAllMenu();
            }
        }

        return SUCCESS;
    }

    public String groupLevelSubMenuSave() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }
        //
        PreparedStatement ps = null;
        ResultSet rs = null;
        //        
        String[] menuArray = null;
        String[] childMenuArray = null;
        String menuId = "";
        String menuStatus = "";
        //
        String insertStatement = "";
        String updateStatement = "";
        int updateRecord = 0;
        //    
        String selectStatement = "";
        int levelMenuId = 0;
        //        
        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }
        //
        if (connection != null) {

            if ((groupFormString != null) && (getLevelId() != null)) {

                menuArray = groupFormString.split("/rd/");

                if (menuArray != null) {

                    try {

                        for (int i = 0; i < menuArray.length; i++) {

                            if (menuArray[i] != null) {

                                if (!menuArray[i].isEmpty()) {

                                    menuId = "";
                                    menuStatus = "";
                                    childMenuArray = menuArray[i].split("/fd/");

                                    if (childMenuArray != null) {

                                        menuId = childMenuArray[0];
                                        menuStatus = childMenuArray[1];

                                        if ((menuId != null) && (menuStatus != null)) {

                                            if ((!menuId.isEmpty()) && (!menuStatus.isEmpty())) {

                                                try {

                                                    updateRecord = 0;

                                                    updateStatement = " UPDATE "
                                                            + " user_level_menu "
                                                            + " SET "
                                                            + " level_menu_status = '" + menuStatus + "', "
                                                            + " update_by = '" + groupName + "', "
                                                            + " update_date = CURRENT_TIMESTAMP "
                                                            + " WHERE "
                                                            + " level_id = '" + levelId + "' "
                                                            + " AND "
                                                            + " menu_id = '" + menuId + "' ";

                                                    ps = connection.prepareStatement(updateStatement);
                                                    updateRecord = ps.executeUpdate();

                                                    if (updateRecord == 0) {

                                                        insertStatement = " INSERT INTO "
                                                                + " user_level_menu "
                                                                + " ( "
                                                                + " menu_id, "
                                                                + " level_id, "
                                                                + " level_menu_status, "
                                                                + " insert_by, "
                                                                + " insert_date "
                                                                + " ) VALUES ( "
                                                                + "'" + menuId + "', "
                                                                + "'" + levelId + "', "
                                                                + "'" + menuStatus + "', "
                                                                + "'" + groupName + "', "
                                                                + " CURRENT_TIMESTAMP "
                                                                + " ) ";

                                                        ps = connection.prepareStatement(insertStatement);
                                                        updateRecord = ps.executeUpdate();
                                                    }
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        }

        if (updateRecord > 0) {
            messageColor = "success";
            messageString = "Menu permission saved successfully.";
        } else {
            messageColor = "danger";
            messageString = "Menu permission save failed !";
        }

        return SUCCESS;
    }

    public static void main(String[] args) {

        try {

            PreparedStatement ps = null;
            Connection c = DBConnection.getMySqlConnection();

            String deleteStatement = " DELETE FROM "
                    + " user_level_menu ";
            ps = c.prepareStatement(deleteStatement);
//            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    /**
     * *************************************************************************
     * ***************************** Search Number
     *
     ***************************************************************************
     * @return
     */
    public List<MobileRechargeHistory> getMRNumber(String sId, String mrNumber) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMRNumber(commonList.getAllMRNumber(connection, sId, mrNumber));
        }

        return allMRNumber;
    }

    public List<MobileMoneyHistory> getMMNumber(String uId, String mmNumber) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMMNumber(commonList.getAllMMNumber(connection, uId, mmNumber));
        }

        return allMMNumber;
    }

    public String searchMobileNubmer() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getMRNumber(userID, searchPhone);

        getMMNumber(userID, searchPhone);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ***************************** Admin User Panel
     *
     ***************************************************************************
     * @return
     */
    public List<MobileRechargeHistory> allMRechargeHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMRechargeHInfoList(commonList.getAllMobileRechargeHistoryInfo(connection, uId));
        }

        return allMRechargeHInfoList;
    }

    //
    public double totalMobileRechargeSales(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String totalStatement = " SELECT "
                        + " SUM(given_balance) "
                        + " FROM "
                        + " mobile_recharge_history "
                        + " WHERE "
                        + " sender = '" + uId + "' "
                        + " AND "
                        + " active_status = 'Y' ";

                ps = connection.prepareStatement(totalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalMRSale(sum);

            } catch (Exception ex) {
                totalMRSale = 0.0;
                System.out.println(ex);
            }
        }

        return totalMRSale;
    }

    public double totalResellerMrBalByParent(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mrTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM "
                        + " user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

//                String totalStatement3 = " SELECT MMH.MM_TOTAL FROM user_info UI "
//                        + " INNER JOIN "
//                        + " ( SELECT user_id, SUM(given_balance) AS MR_TOTAL FROM mobile_recharge_history GROUP BY user_id )MRH ON UI.user_id =  MRH.user_id "
//                        + " INNER JOIN "
//                        + " ( SELECT user_id, SUM(given_balance) AS MM_TOTAL FROM mobile_money_history GROUP BY user_id )MMH ON UI.user_id =  MMH.user_id "
//                        + " WHERE UI.parent_id= '" + uId + "' ";
//                
                ps = connection.prepareStatement(mrTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalResellerMrBal(sum);

            } catch (Exception ex) {
                totalResellerMrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalResellerMrBal;
    }

    public double totalResellerSubAdminMrBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND "
                        + " UI.group_id = 1 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalResellerSubAdminMrBal(sum);

            } catch (Exception ex) {
                totalResellerSubAdminMrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalResellerSubAdminMrBal;
    }

    public double totalReseller4MrBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND "
                        + " UI.group_id = 2 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller4MrBal(sum);

            } catch (Exception ex) {
                totalReseller4MrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller4MrBal;
    }

    public double totalReseller3MrBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND "
                        + " UI.group_id = 3 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller3MrBal(sum);

            } catch (Exception ex) {
                totalReseller3MrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller3MrBal;
    }

    public double totalReseller2MrBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND "
                        + " UI.group_id = 4 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller2MrBal(sum);

            } catch (Exception ex) {
                totalReseller2MrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller2MrBal;
    }

    public double totalReseller1MrBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MRH.given_balance "
                        + " FROM user_info UI, mobile_recharge_history MRH "
                        + " WHERE "
                        + " UI.user_id =  MRH.user_id "
                        + " AND "
                        + " UI.group_id = 5 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MRH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller1MrBal(sum);

            } catch (Exception ex) {
                totalReseller1MrBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller1MrBal;
    }

    //
    public double totalMobileMoneySales(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String totalStatement = " SELECT "
                        + " SUM(given_balance) "
                        + " FROM "
                        + " mobile_money_history "
                        + " WHERE "
                        + " sender = '" + uId + "' "
                        + " AND "
                        + " active_status = 'Y' ";

                ps = connection.prepareStatement(totalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalMMSale(sum);

            } catch (Exception ex) {
                totalMMSale = 0.0;
                System.out.println(ex);
            }
        }

        return totalMMSale;
    }

    public double totalResellerMmBalByParent(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalResellerMmBal(sum);

            } catch (Exception ex) {
                totalResellerMmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalResellerMmBal;
    }

    public double totalResellerSubAdminMmBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND "
                        + " UI.group_id = 1 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalResellerSubAdminMmBal(sum);

            } catch (Exception ex) {
                totalResellerSubAdminMmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalResellerSubAdminMmBal;
    }

    public double totalReseller4MmBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND "
                        + " UI.group_id = 2 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller4MmBal(sum);

            } catch (Exception ex) {
                totalReseller4MmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller4MmBal;
    }

    public double totalReseller3MmBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND "
                        + " UI.group_id = 3 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller3MmBal(sum);

            } catch (Exception ex) {
                totalReseller3MmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller3MmBal;
    }

    public double totalReseller2MmBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND "
                        + " UI.group_id = 4 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller2MmBal(sum);

            } catch (Exception ex) {
                totalReseller2MmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller2MmBal;
    }

    public double totalReseller1MmBalance(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;
        Double sum = 0.0;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            try {

                String mmTotalStatement = " SELECT "
                        + " SUM(A.given_balance) "
                        + " FROM "
                        + " ( SELECT UI.user_id, UI.parent_id, UI.user_name, MMH.given_balance "
                        + " FROM user_info UI, mobile_money_history MMH "
                        + " WHERE "
                        + " UI.user_id =  MMH.user_id "
                        + " AND "
                        + " UI.group_id = 5 "
                        + " AND "
                        + " UI.parent_id= '" + uId + "' "
                        + " AND "
                        + " MMH.active_status = 'Y' )A ";

                ps = connection.prepareStatement(mmTotalStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    sum = rs.getDouble(1) == 0.0 ? 0.0 : rs.getDouble(1);
                }

                setTotalReseller1MmBal(sum);

            } catch (Exception ex) {
                totalReseller1MmBal = 0.0;
                System.out.println(ex);
                ex.printStackTrace();
            }
        }

        return totalReseller1MmBal;
    }

    //
    public List<MobileRechargeHistory> latestProcessingOrdersMRHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setLatestProcessingOrdersList(commonList.getLatestProcessingOrdersMRHInfo(connection, uId));
        }

        return latestProcessingOrdersList;
    }

    public List<MobileRechargeHistory> latestWaitingOrdersMRHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setLatestWaitingOrdersList(commonList.getLatestWaitingMRHInfo(connection, uId));
        }

        return latestWaitingOrdersList;
    }

    public List<MobileRechargeHistory> latestSuccessOrdersMRHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setLatestSuccessOrdersList(commonList.getLatestSuccessMRHInfo(connection, uId));
        }

        return getLatestSuccessOrdersList();
    }

    public List<MobileRechargeHistory> getSingleMRHInfo(String uId, Integer mrId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMRHInfoList(commonList.getSingleMobileRechargeHistoryInfo(connection, uId, mrId));
        }
        return singleMRHInfoList;
    }

    public List<MobileMoneyHistory> allMMoneyHInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllMMoneyHInfoList(commonList.getAllMobileMoneyHistoryInfo(connection, uId));
        }

        return allMMoneyHInfoList;
    }

    public List<MobileMoneyHistory> getSingleMMoneyHInfo(String uId, Integer mmId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMMoneyHInfoList(commonList.getSingleMobileMoneyHistoryInfo(connection, uId, mmId));
        }

        return singleMMoneyHInfoList;
    }

    public List<UserInfo> getPaymentInf(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setPaymentInfoList(commonList.getPaymentInfo(connection, pId));
        }

        return paymentInfoList;
    }

    public List<UserInfo> getSinglePaymentInf(String pId, String rId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSinglePaymentInfoList(commonList.getSinglePaymentInfo(connection, pId, rId));
        }

        return singlePaymentInfoList;
    }

    public List<MobileRechargeTransactionHistory> getTransactionHistory(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMrTransactionHInfoList(commonList.getTransaction(connection, pId));
        }

        return mrTransactionHInfoList;
    }

    public List<MobileRechargeTransactionHistory> singleTransactionHistory(String pId, Integer thId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleMrTransactionHInfoList(commonList.singleTransaction(connection, pId, thId));
        }

        return singleMrTransactionHInfoList;
    }

    public String viewTransactionsHistoryDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        singleTransactionHistory(userID, mrTransactionHistoryId);

        System.out.println(mrTransactionHistoryId);

        return SUCCESS;
    }

    public String goAdminUserLoginPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String adminUserLogin() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userName.isEmpty() || password.isEmpty()) {
            messageString = "Enter any User Name and password.";
            return INPUT;
        }

//        if (userID == null || userID.length() == 0) {
//            messageString = "Session.";
//            return LOGIN;
//        }
        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<AdminUser> adminUserList = commonList.adminUserLoginInfo(connection, userName, password);

            if ((adminUserList == null) || (adminUserList.isEmpty())) {
                messageString = "Invalid User Name or Password.";
                return ERROR;
            } else {
                for (AdminUser adminUser : adminUserList) {
                    session.put("adminUserId", adminUser.getAdminUserId());
                    session.put("adminUserPassword", adminUser.getPassword());
                    session.put("adminUserName", adminUser.getUserName());
                    session.put("adminUserGroupId", adminUser.getAdminGroupInfo().getAdminGroupId());
                    session.put("adminUserGroupName", adminUser.getAdminGroupInfo().getAdminGroupName());
                }
                messageString = "1";
                return ADMINUSERLOGINSUCCESS;
            }
        }

        return SUCCESS;
    }

    public String adminUserDashBoard() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

//        if (userID == null || userID.length() == 0) {
//            return LOGIN;
//        }
//        
        allMRechargeHInfo(userID);

        allMMoneyHInfo(userID);

        return SUCCESS;
    }

    public String adminUserMobileRecharge() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        allMRechargeHInfo(userID);

        return SUCCESS;
    }

    public String adminUserMobileMoney() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        allMMoneyHInfo(userID);

        return SUCCESS;
    }

    public String adminUserPayments() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        List l = getPaymentInf(userID);

        if (!l.isEmpty()) {
            messageString = getPaymentInf(userID).get(0).getParentId();
        } else {
            messageString = "no reseller found";
        }

        return SUCCESS;
    }

    public String adminUserTransactions() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        getTransactionHistory(userID);

        return SUCCESS;
    }

    public String adminUserMRechargeView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        getSingleMRHInfo(userID, mobileRechargeId);

        getMRNumber(userID, searchPhone);

        return SUCCESS;
    }

    public String adminUserMMoneyView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        getSingleMMoneyHInfo(userID, mobileMoneyId);

        getMMNumber(userID, searchPhone);

        return SUCCESS;
    }

    public String adminUserPaymentView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String useruame = (String) session.get("adminUserName");

        if (useruame == null || useruame.length() == 0) {
            return LOGIN;
        }

        getSinglePaymentInf(userID, resellerId);

        return SUCCESS;
    }

    public String adminUserLogout() {

        Map session = ActionContext.getContext().getSession();

        session.remove("adminUserId");
        session.remove("adminUserPassword");
        session.remove("adminUserName");
        session.remove("adminUserGroupId");
        session.remove("adminUserGroupName");
        session.clear();

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * *************************** Page Methods
     *
     * @return
     */
    public String goAdminDashboardPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        setAllMRNumber(allMRechargeHInfo(userID));

        getAllResellers(userID);

        setAllMMNumber(allMMoneyHInfo(userID));

        latestProcessingOrdersMRHInfo(userID);

        latestWaitingOrdersMRHInfo(userID);

        latestSuccessOrdersMRHInfo(userID);
        //
        totalMobileRechargeSales(userID);

        totalResellerMrBalByParent(userID);

        totalResellerSubAdminMrBalance(userID);

        totalReseller4MrBalance(userID);

        totalReseller3MrBalance(userID);

        totalReseller2MrBalance(userID);

        totalReseller1MrBalance(userID);
        //
        totalMobileMoneySales(userID);

        totalResellerMmBalByParent(userID);

        totalResellerSubAdminMmBalance(userID);

        totalReseller4MmBalance(userID);

        totalReseller3MmBalance(userID);

        totalReseller2MmBalance(userID);

        totalReseller1MmBalance(userID);

        return SUCCESS;
    }

    public String goAdminLiveChatPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goAdminUsersPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllAdminGroup();

        getAllAdminUser();

        return SUCCESS;
    }

    public String goProcessingManagementPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goWaitingManagementPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goActivityLogPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goSearchNumberPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goUserSummaryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllResellers(userID);

        return SUCCESS;
    }

    public String goOnlineUserPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllOnlineUser();

        return SUCCESS;
    }

    public String goModemsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goTransactionsHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getTransactionHistory(userID);

        return SUCCESS;
    }

    public String goOperatorSMSRepliesPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goSIMRechargeHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goSettingsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goBrandingPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goMenuControlsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllMenu();

        getUserLevel();

        if (rechargeAction == null) {
            rechargeAction = new MobileRechargeAction();
        }

        if (rechargeAction != null) {
            setIsMRThreadRunning(rechargeAction.isMobileRechargeThreadRunning());
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ********************** Setter and Getter Methods
     *
     * @return
     */
    /**
     * @return the allUserInfo
     */
    public List<UserInfo> getAllUserInfo() {
        return allUserInfo;
    }

    /**
     * @param allUserInfo the allUserInfo to set
     */
    public void setAllUserInfo(List<UserInfo> allUserInfo) {
        this.allUserInfo = allUserInfo;
    }

    /**
     * @return the userLevelInfoList
     */
    public List<UserLevel> getUserLevelInfoList() {
        return userLevelInfoList;
    }

    /**
     * @param userLevelInfoList the userLevelInfoList to set
     */
    public void setUserLevelInfoList(List<UserLevel> userLevelInfoList) {
        this.userLevelInfoList = userLevelInfoList;
    }

    /**
     * @return the parentMenuInfoList
     */
    public List<ParentMenu> getParentMenuInfoList() {
        return parentMenuInfoList;
    }

    /**
     * @param parentMenuInfoList the parentMenuInfoList to set
     */
    public void setParentMenuInfoList(List<ParentMenu> parentMenuInfoList) {
        this.parentMenuInfoList = parentMenuInfoList;
    }

    /**
     * @return the levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * @param levelName the levelName to set
     */
    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    /**
     * @return the groupFormString
     */
    public String getGroupFormString() {
        return groupFormString;
    }

    /**
     * @param groupFormString the groupFormString to set
     */
    public void setGroupFormString(String groupFormString) {
        this.groupFormString = groupFormString;
    }

    /**
     * @return the userGroupInfoList
     */
    public List<UserGroup> getUserGroupInfoList() {
        return userGroupInfoList;
    }

    /**
     * @param userGroupInfoList the userGroupInfoList to set
     */
    public void setUserGroupInfoList(List<UserGroup> userGroupInfoList) {
        this.userGroupInfoList = userGroupInfoList;
    }

    /**
     * @return the allAdminGroupInfoList
     */
    public List<AdminGroup> getAllAdminGroupInfoList() {
        return allAdminGroupInfoList;
    }

    /**
     * @param allAdminGroupInfoList the allAdminGroupInfoList to set
     */
    public void setAllAdminGroupInfoList(List<AdminGroup> allAdminGroupInfoList) {
        this.allAdminGroupInfoList = allAdminGroupInfoList;
    }

    /**
     * @return the allAdminUserInfoList
     */
    public List<AdminUser> getAllAdminUserInfoList() {
        return allAdminUserInfoList;
    }

    /**
     * @param allAdminUserInfoList the allAdminUserInfoList to set
     */
    public void setAllAdminUserInfoList(List<AdminUser> allAdminUserInfoList) {
        this.allAdminUserInfoList = allAdminUserInfoList;
    }

    /**
     * @return the adminUserId
     */
    public Integer getAdminUserId() {
        return adminUserId;
    }

    /**
     * @param adminUserId the adminUserId to set
     */
    public void setAdminUserId(Integer adminUserId) {
        this.adminUserId = adminUserId;
    }

    /**
     * @return the userRoleList
     */
    public String getUserRoleList() {
        return userRoleList;
    }

    /**
     * @param userRoleList the userRoleList to set
     */
    public void setUserRoleList(String userRoleList) {
        this.userRoleList = userRoleList;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the viewOneAdminUserInfoList
     */
    public List<AdminUser> getViewOneAdminUserInfoList() {
        return viewOneAdminUserInfoList;
    }

    /**
     * @param viewOneAdminUserInfoList the viewOneAdminUserInfoList to set
     */
    public void setViewOneAdminUserInfoList(List<AdminUser> viewOneAdminUserInfoList) {
        this.viewOneAdminUserInfoList = viewOneAdminUserInfoList;
    }

    /**
     * @return the levelId
     */
    public Integer getLevelId() {
        return levelId;
    }

    /**
     * @param levelId the levelId to set
     */
    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    /**
     * @return the userLevelMenuInfoList
     */
    public List<UserLevelMenu> getUserLevelMenuInfoList() {
        return userLevelMenuInfoList;
    }

    /**
     * @param userLevelMenuInfoList the userLevelMenuInfoList to set
     */
    public void setUserLevelMenuInfoList(List<UserLevelMenu> userLevelMenuInfoList) {
        this.userLevelMenuInfoList = userLevelMenuInfoList;
    }

    /**
     * @return the allResallersInfoList
     */
    public List<UserInfo> getAllResallersInfoList() {
        return allResallersInfoList;
    }

    /**
     * @param allResallersInfoList the allResallersInfoList to set
     */
    public void setAllResallersInfoList(List<UserInfo> allResallersInfoList) {
        this.allResallersInfoList = allResallersInfoList;
    }

    /**
     * @return the allMRNumber
     */
    public List<MobileRechargeHistory> getAllMRNumber() {
        return allMRNumber;
    }

    /**
     * @param allMRNumber the allMRNumber to set
     */
    public void setAllMRNumber(List<MobileRechargeHistory> allMRNumber) {
        this.allMRNumber = allMRNumber;
    }

    /**
     * @return the allMMNumber
     */
    public List<MobileMoneyHistory> getAllMMNumber() {
        return allMMNumber;
    }

    /**
     * @param allMMNumber the allMMNumber to set
     */
    public void setAllMMNumber(List<MobileMoneyHistory> allMMNumber) {
        this.allMMNumber = allMMNumber;
    }

    /**
     * @return the onLineUserList
     */
    public List<UserInfo> getOnLineUserList() {
        return onLineUserList;
    }

    /**
     * @param onLineUserList the onLineUserList to set
     */
    public void setOnLineUserList(List<UserInfo> onLineUserList) {
        this.onLineUserList = onLineUserList;
    }

    /**
     * @return the allMRechargeHInfoList
     */
    public List<MobileRechargeHistory> getAllMRechargeHInfoList() {
        return allMRechargeHInfoList;
    }

    /**
     * @param allMRechargeHInfoList the allMRechargeHInfoList to set
     */
    public void setAllMRechargeHInfoList(List<MobileRechargeHistory> allMRechargeHInfoList) {
        this.allMRechargeHInfoList = allMRechargeHInfoList;
    }

    /**
     * @return the singleMRHInfoList
     */
    public List<MobileRechargeHistory> getSingleMRHInfoList() {
        return singleMRHInfoList;
    }

    /**
     * @param singleMRHInfoList the singleMRHInfoList to set
     */
    public void setSingleMRHInfoList(List<MobileRechargeHistory> singleMRHInfoList) {
        this.singleMRHInfoList = singleMRHInfoList;
    }

    /**
     * @return the mobileRechargeId
     */
    public Integer getMobileRechargeId() {
        return mobileRechargeId;
    }

    /**
     * @param mobileRechargeId the mobileRechargeId to set
     */
    public void setMobileRechargeId(Integer mobileRechargeId) {
        this.mobileRechargeId = mobileRechargeId;
    }

    /**
     * @return the singleMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getSingleMMoneyHInfoList() {
        return singleMMoneyHInfoList;
    }

    /**
     * @param singleMMoneyHInfoList the singleMMoneyHInfoList to set
     */
    public void setSingleMMoneyHInfoList(List<MobileMoneyHistory> singleMMoneyHInfoList) {
        this.singleMMoneyHInfoList = singleMMoneyHInfoList;
    }

    /**
     * @return the mobileMoneyId
     */
    public Integer getMobileMoneyId() {
        return mobileMoneyId;
    }

    /**
     * @param mobileMoneyId the mobileMoneyId to set
     */
    public void setMobileMoneyId(Integer mobileMoneyId) {
        this.mobileMoneyId = mobileMoneyId;
    }

    /**
     * @return the allMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getAllMMoneyHInfoList() {
        return allMMoneyHInfoList;
    }

    /**
     * @param allMMoneyHInfoList the allMMoneyHInfoList to set
     */
    public void setAllMMoneyHInfoList(List<MobileMoneyHistory> allMMoneyHInfoList) {
        this.allMMoneyHInfoList = allMMoneyHInfoList;
    }

    /**
     * @return the paymentInfoList
     */
    public List<UserInfo> getPaymentInfoList() {
        return paymentInfoList;
    }

    /**
     * @param paymentInfoList the paymentInfoList to set
     */
    public void setPaymentInfoList(List<UserInfo> paymentInfoList) {
        this.paymentInfoList = paymentInfoList;
    }

    /**
     * @return the searchPhone
     */
    public String getSearchPhone() {
        return searchPhone;
    }

    /**
     * @param searchPhone the searchPhone to set
     */
    public void setSearchPhone(String searchPhone) {
        this.searchPhone = searchPhone;
    }

    /**
     * @return the resellerId
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * @param resellerId the resellerId to set
     */
    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    /**
     * @return the singlePaymentInfoList
     */
    public List<UserInfo> getSinglePaymentInfoList() {
        return singlePaymentInfoList;
    }

    /**
     * @param singlePaymentInfoList the singlePaymentInfoList to set
     */
    public void setSinglePaymentInfoList(List<UserInfo> singlePaymentInfoList) {
        this.singlePaymentInfoList = singlePaymentInfoList;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the mrTransactionHInfoList
     */
    public List<MobileRechargeTransactionHistory> getMrTransactionHInfoList() {
        return mrTransactionHInfoList;
    }

    /**
     * @param mrTransactionHInfoList the mrTransactionHInfoList to set
     */
    public void setMrTransactionHInfoList(List<MobileRechargeTransactionHistory> mrTransactionHInfoList) {
        this.mrTransactionHInfoList = mrTransactionHInfoList;
    }

    /**
     * @return the mrTransactionHistoryId
     */
    public Integer getMrTransactionHistoryId() {
        return mrTransactionHistoryId;
    }

    /**
     * @param mrTransactionHistoryId the mrTransactionHistoryId to set
     */
    public void setMrTransactionHistoryId(Integer mrTransactionHistoryId) {
        this.mrTransactionHistoryId = mrTransactionHistoryId;
    }

    /**
     * @return the singleMrTransactionHInfoList
     */
    public List<MobileRechargeTransactionHistory> getSingleMrTransactionHInfoList() {
        return singleMrTransactionHInfoList;
    }

    /**
     * @param singleMrTransactionHInfoList the singleMrTransactionHInfoList to
     * set
     */
    public void setSingleMrTransactionHInfoList(List<MobileRechargeTransactionHistory> singleMrTransactionHInfoList) {
        this.singleMrTransactionHInfoList = singleMrTransactionHInfoList;
    }

    /**
     * @return the latestProcessingOrdersList
     */
    public List<MobileRechargeHistory> getLatestProcessingOrdersList() {
        return latestProcessingOrdersList;
    }

    /**
     * @param latestProcessingOrdersList the latestProcessingOrdersList to set
     */
    public void setLatestProcessingOrdersList(List<MobileRechargeHistory> latestProcessingOrdersList) {
        this.latestProcessingOrdersList = latestProcessingOrdersList;
    }

    /**
     * @return the latestWaitingOrdersList
     */
    public List<MobileRechargeHistory> getLatestWaitingOrdersList() {
        return latestWaitingOrdersList;
    }

    /**
     * @param latestWaitingOrdersList the latestWaitingOrdersList to set
     */
    public void setLatestWaitingOrdersList(List<MobileRechargeHistory> latestWaitingOrdersList) {
        this.latestWaitingOrdersList = latestWaitingOrdersList;
    }

    /**
     * @return the latestSuccessOrdersList
     */
    public List<MobileRechargeHistory> getLatestSuccessOrdersList() {
        return latestSuccessOrdersList;
    }

    /**
     * @param latestSuccessOrdersList the latestSuccessOrdersList to set
     */
    public void setLatestSuccessOrdersList(List<MobileRechargeHistory> latestSuccessOrdersList) {
        this.latestSuccessOrdersList = latestSuccessOrdersList;
    }

    /**
     * @return the totalMRSale
     */
    public double getTotalMRSale() {
        return totalMRSale;
    }

    /**
     * @param totalMRSale the totalMRSale to set
     */
    public void setTotalMRSale(double totalMRSale) {
        this.totalMRSale = totalMRSale;
    }

    /**
     * @return the totalMMSale
     */
    public double getTotalMMSale() {
        return totalMMSale;
    }

    /**
     * @param totalMMSale the totalMMSale to set
     */
    public void setTotalMMSale(double totalMMSale) {
        this.totalMMSale = totalMMSale;
    }

    /**
     * @return the totalResellerMrBal
     */
    public double getTotalResellerMrBal() {
        return totalResellerMrBal;
    }

    /**
     * @param totalResellerMrBal the totalResellerMrBal to set
     */
    public void setTotalResellerMrBal(double totalResellerMrBal) {
        this.totalResellerMrBal = totalResellerMrBal;
    }

    /**
     * @return the totalResellerMmBal
     */
    public double getTotalResellerMmBal() {
        return totalResellerMmBal;
    }

    /**
     * @param totalResellerMmBal the totalResellerMmBal to set
     */
    public void setTotalResellerMmBal(double totalResellerMmBal) {
        this.totalResellerMmBal = totalResellerMmBal;
    }

    /**
     * @return the totalReseller4MmBal
     */
    public double getTotalReseller4MmBal() {
        return totalReseller4MmBal;
    }

    /**
     * @param totalReseller4MmBal the totalReseller4MmBal to set
     */
    public void setTotalReseller4MmBal(double totalReseller4MmBal) {
        this.totalReseller4MmBal = totalReseller4MmBal;
    }

    /**
     * @return the totalReseller3MmBal
     */
    public double getTotalReseller3MmBal() {
        return totalReseller3MmBal;
    }

    /**
     * @param totalReseller3MmBal the totalReseller3MmBal to set
     */
    public void setTotalReseller3MmBal(double totalReseller3MmBal) {
        this.totalReseller3MmBal = totalReseller3MmBal;
    }

    /**
     * @return the totalReseller2MmBal
     */
    public double getTotalReseller2MmBal() {
        return totalReseller2MmBal;
    }

    /**
     * @param totalReseller2MmBal the totalReseller2MmBal to set
     */
    public void setTotalReseller2MmBal(double totalReseller2MmBal) {
        this.totalReseller2MmBal = totalReseller2MmBal;
    }

    /**
     * @return the totalReseller1MmBal
     */
    public double getTotalReseller1MmBal() {
        return totalReseller1MmBal;
    }

    /**
     * @param totalReseller1MmBal the totalReseller1MmBal to set
     */
    public void setTotalReseller1MmBal(double totalReseller1MmBal) {
        this.totalReseller1MmBal = totalReseller1MmBal;
    }

    /**
     * @return the totalResellerSubAdminMmBal
     */
    public double getTotalResellerSubAdminMmBal() {
        return totalResellerSubAdminMmBal;
    }

    /**
     * @param totalResellerSubAdminMmBal the totalResellerSubAdminMmBal to set
     */
    public void setTotalResellerSubAdminMmBal(double totalResellerSubAdminMmBal) {
        this.totalResellerSubAdminMmBal = totalResellerSubAdminMmBal;
    }

    /**
     * @return the totalResellerSubAdminMrBal
     */
    public double getTotalResellerSubAdminMrBal() {
        return totalResellerSubAdminMrBal;
    }

    /**
     * @param totalResellerSubAdminMrBal the totalResellerSubAdminMrBal to set
     */
    public void setTotalResellerSubAdminMrBal(double totalResellerSubAdminMrBal) {
        this.totalResellerSubAdminMrBal = totalResellerSubAdminMrBal;
    }

    /**
     * @return the totalReseller4MrBal
     */
    public double getTotalReseller4MrBal() {
        return totalReseller4MrBal;
    }

    /**
     * @param totalReseller4MrBal the totalReseller4MrBal to set
     */
    public void setTotalReseller4MrBal(double totalReseller4MrBal) {
        this.totalReseller4MrBal = totalReseller4MrBal;
    }

    /**
     * @return the totalReseller3MrBal
     */
    public double getTotalReseller3MrBal() {
        return totalReseller3MrBal;
    }

    /**
     * @param totalReseller3MrBal the totalReseller3MrBal to set
     */
    public void setTotalReseller3MrBal(double totalReseller3MrBal) {
        this.totalReseller3MrBal = totalReseller3MrBal;
    }

    /**
     * @return the totalReseller2MrBal
     */
    public double getTotalReseller2MrBal() {
        return totalReseller2MrBal;
    }

    /**
     * @param totalReseller2MrBal the totalReseller2MrBal to set
     */
    public void setTotalReseller2MrBal(double totalReseller2MrBal) {
        this.totalReseller2MrBal = totalReseller2MrBal;
    }

    /**
     * @return the totalReseller1MrBal
     */
    public double getTotalReseller1MrBal() {
        return totalReseller1MrBal;
    }

    /**
     * @param totalReseller1MrBal the totalReseller1MrBal to set
     */
    public void setTotalReseller1MrBal(double totalReseller1MrBal) {
        this.totalReseller1MrBal = totalReseller1MrBal;
    }

    /**
     * @return the isMRThreadRunning
     */
    public boolean isIsMRThreadRunning() {
        return isMRThreadRunning;
    }

    /**
     * @param isMRThreadRunning the isMRThreadRunning to set
     */
    public void setIsMRThreadRunning(boolean isMRThreadRunning) {
        this.isMRThreadRunning = isMRThreadRunning;
    }
}
