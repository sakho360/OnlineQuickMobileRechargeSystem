package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.ManageRateDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ManageRate;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class ManageRatesAction extends ActionSupport {

    private Integer manageRateId;
    private String rateName;
    private Float grameenPhone;
    private Float robi;
    private Float banglalink;
    private Float airtel;
    private Float teletalk;
    private Float bkash;
    private Float dbbl;
    //    
    private String messageString;
    private String messageColor;
    //
    private CommonList commonList = null;
    private Connection connection = null;
    //
    private List<ManageRate> allManageRateInfoList = null;
    private List<ManageRate> singleManageRateInfoList = null;
    //
    private ManageRateDao manageRateDao = null;

    //
    public List<ManageRate> getAllManageRate(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllManageRateInfoList(commonList.getAllManageRateInfo(connection, pId));
        }

        return allManageRateInfoList;
    }

    public List<ManageRate> getSingleManageRate(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleManageRateInfoList(commonList.getSingleManageRateInfo(connection, uId, manageRateId));
        }

        return singleManageRateInfoList;
    }

    public String showAllManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllManageRate(userID);

        return SUCCESS;
    }

    public String addNewManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int checked = manageRateDao.saveManageRateInfo(connection, rateName, grameenPhone, robi, banglalink, airtel, teletalk, bkash, dbbl, userID, userID);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Rate Plan created successfully.";
            } else {
                messageColor = "danger";
                messageString = "Rate Plan create failed !";
            }
        }

        return SUCCESS;
    }

    public String manageRateDetailsView() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getSingleManageRate(userID);

        getAllManageRate(userID);

        return SUCCESS;
    }

    public String updateManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int checked = manageRateDao.updateManageRateInfo(connection, rateName, grameenPhone, robi, banglalink, airtel, teletalk, bkash, dbbl, userID, userID, manageRateId);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Rate Plan updated successfully.";
            } else {
                messageColor = "danger";
                messageString = "Rate Plan update failed !";
            }
        }

        return SUCCESS;
    }

    public String deleteManageRate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (manageRateDao == null) {
            manageRateDao = new ManageRateDao();
        }

        if (connection != null) {

            int checked = manageRateDao.deleteManageRateInfo(connection, userID, manageRateId);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Rate Plan deleted successfully.";
            } else {
                messageColor = "danger";
                messageString = "Rate Plan delete failed !";
            }
        }

        return SUCCESS;
    }

    public String goManageRatesPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllManageRate(userID);

        return SUCCESS;
    }

    /**
     * @return the grameenPhone
     */
    public Float getGrameenPhone() {
        return grameenPhone;
    }

    /**
     * @param grameenPhone the grameenPhone to set
     */
    public void setGrameenPhone(Float grameenPhone) {
        this.grameenPhone = grameenPhone;
    }

    /**
     * @return the robi
     */
    public Float getRobi() {
        return robi;
    }

    /**
     * @param robi the robi to set
     */
    public void setRobi(Float robi) {
        this.robi = robi;
    }

    /**
     * @return the banglalink
     */
    public Float getBanglalink() {
        return banglalink;
    }

    /**
     * @param banglalink the banglalink to set
     */
    public void setBanglalink(Float banglalink) {
        this.banglalink = banglalink;
    }

    /**
     * @return the airtel
     */
    public Float getAirtel() {
        return airtel;
    }

    /**
     * @param airtel the airtel to set
     */
    public void setAirtel(Float airtel) {
        this.airtel = airtel;
    }

    /**
     * @return the teletalk
     */
    public Float getTeletalk() {
        return teletalk;
    }

    /**
     * @param teletalk the teletalk to set
     */
    public void setTeletalk(Float teletalk) {
        this.teletalk = teletalk;
    }

    /**
     * @return the bkash
     */
    public Float getBkash() {
        return bkash;
    }

    /**
     * @param bkash the bkash to set
     */
    public void setBkash(Float bkash) {
        this.bkash = bkash;
    }

    /**
     * @return the dbbl
     */
    public Float getDbbl() {
        return dbbl;
    }

    /**
     * @param dbbl the dbbl to set
     */
    public void setDbbl(Float dbbl) {
        this.dbbl = dbbl;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the allManageRateInfoList
     */
    public List<ManageRate> getAllManageRateInfoList() {
        return allManageRateInfoList;
    }

    /**
     * @param allManageRateInfoList the allManageRateInfoList to set
     */
    public void setAllManageRateInfoList(List<ManageRate> allManageRateInfoList) {
        this.allManageRateInfoList = allManageRateInfoList;
    }

    /**
     * @return the singleManageRateInfoList
     */
    public List<ManageRate> getSingleManageRateInfoList() {
        return singleManageRateInfoList;
    }

    /**
     * @param singleManageRateInfoList the singleManageRateInfoList to set
     */
    public void setSingleManageRateInfoList(List<ManageRate> singleManageRateInfoList) {
        this.singleManageRateInfoList = singleManageRateInfoList;
    }

    /**
     * @return the manageRateId
     */
    public Integer getManageRateId() {
        return manageRateId;
    }

    /**
     * @param manageRateId the manageRateId to set
     */
    public void setManageRateId(Integer manageRateId) {
        this.manageRateId = manageRateId;
    }

    /**
     * @return the rateName
     */
    public String getRateName() {
        return rateName;
    }

    /**
     * @param rateName the rateName to set
     */
    public void setRateName(String rateName) {
        this.rateName = rateName;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }
}
