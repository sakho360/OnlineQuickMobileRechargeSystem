package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.MyAccountDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.BalanceRequestHistory;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class MyAccountAction extends ActionSupport {

    private String resellerId;
    private String curPassword;
    private String newPassword;
    private String transactionPin;
    private String pinStatus;
    //    
    private String fullName;
    private String email;
    private String phone;
    private String otpStatus;
    private String pinEnabledStatus;
    private String pinConfirmStatus;
    private String apiStatus;
    private String currentPin;
    private String parentId;
    private String balType;
    private Double amount;
    private Integer balanceRequestId;
    //    
    private String fieldName;
    private String fieldValue;
    //    
    private String loginMessage;
    private String messageString;
    private String messageColor;
    //
    private CommonList commonList = null;
    private Connection connection = null;
    //
    private List<UserInfo> profileInfoList = null;
    private List<BalanceRequestHistory> balanceRequestReceiveInfoList = null;
    private List<BalanceRequestHistory> singleBalanceRequestReceiveInfoList = null;
    //
    private MyAccountDao myAccountDao = null;
    private ResellerAction resellerAction = null;

    public List<UserInfo> getProfile(String id) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setProfileInfoList(commonList.getMyProfileInfo(connection, id));
        }

        return profileInfoList;
    }

    public List<BalanceRequestHistory> balanceRequestReceiveHistory(String resellerId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setBalanceRequestReceiveInfoList(commonList.getBalanceRequestReceive(connection, resellerId));
        }

        return balanceRequestReceiveInfoList;
    }

    public List<BalanceRequestHistory> singleBalanceRequestReceiveHistory(String resellerId, Integer brhId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleBalanceRequestReceiveInfoList(commonList.getSingleBalanceRequestReceive(connection, resellerId, brhId));
        }

        return singleBalanceRequestReceiveInfoList;
    }

    public String updateResellerProfile() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (myAccountDao == null) {
            myAccountDao = new MyAccountDao();
        }

        if (connection != null) {

            List<UserInfo> cPassword = commonList.getCurrentPassword(connection, userID, curPassword);

            if (cPassword.size() != 0) {

                int checked = myAccountDao.updateProfile(connection, fullName, phone, email, otpStatus, pinEnabledStatus, apiStatus, pinConfirmStatus, userID, resellerId);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Your profile updated successfully.";
                } else {
                    messageColor = "danger";
                    messageString = "Your profile update failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Please provide correct current password";
            }
        }

        return SUCCESS;
    }

    public String updatePassword() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (myAccountDao == null) {
            myAccountDao = new MyAccountDao();
        }

        if (connection != null) {

            List<UserInfo> cPassword = commonList.getCurrentPassword(connection, userID, curPassword);

            if (!cPassword.isEmpty()) {

                int checked = myAccountDao.updateResellerPassword(connection, newPassword, userID, userID);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Password changed successfully.";
                } else {
                    messageColor = "danger";
                    messageString = "Password change failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Please provide correct current password";
            }
        }

        return SUCCESS;
    }

    public String requestForBalance() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (myAccountDao == null) {
            myAccountDao = new MyAccountDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, currentPin);

            if (cPin.size() != 0) {

                String trid = "";
                if (trIDGenerator() != null) {
                    trid = trIDGenerator();
                } else {
                    trid = "10000001";
                }

                int checked = myAccountDao.requestBalance(connection, amount, balType, resellerId, parentId, trid);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Balance request successfully send to " + parentId + ". ";
                } else {
                    messageColor = "danger";
                    messageString = "Balance request send failed to " + parentId + ". ";
                }
            } else {
                messageColor = "danger";
                messageString = "Invalid pin !";
            }
        }

        return SUCCESS;
    }

    private String trIDGenerator() {

        String trID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.TR_ID)+1),8,'0') "
                        + " AS BAL_REQ_TR_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(trid, 1,8),UNSIGNED INTEGER) "
                        + " AS TR_ID FROM bal_request_history)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();
                while (rs.next()) {
                    trID = rs.getString("BAL_REQ_TR_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return trID;
    }

    public String viewBalanceRequestHistoryDetails() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        singleBalanceRequestReceiveHistory(userID, balanceRequestId);

        System.out.println(balanceRequestId);

        return SUCCESS;
    }

    public String searchBalanceRequestHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setBalanceRequestReceiveInfoList(commonList.searchBalanceRequestHistoryInfo(connection, userID, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String goMyProfilePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (resellerAction == null) {
            resellerAction = new ResellerAction();
        }

        setPinStatus(resellerAction.checkPinStatus(userID));

        getProfile(userID);

        return SUCCESS;
    }

    public String goBalanceRequestHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        balanceRequestReceiveHistory(userID);

        return SUCCESS;
    }

    public String goEditProfilePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getProfile(userID);

        return SUCCESS;
    }

    public String goChangePasswordPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        String username = (String) session.get("userName");
        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goApiInfoPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String username = (String) session.get("userName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goContactSupportPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String username = (String) session.get("userName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goEnablePinPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String setTransactionPin() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (myAccountDao == null) {
            myAccountDao = new MyAccountDao();
        }

        if (connection != null) {

            List<UserInfo> cPassword = commonList.getCurrentPassword(connection, userID, curPassword);

            if (!cPassword.isEmpty()) {

                int checked = myAccountDao.setTransactionPin(connection, transactionPin, userID, userID);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Transaction pin set successfully.";
                    return SUCCESS;
                } else {
                    messageColor = "danger";
                    messageString = "Transaction pin set failed !";
                    return ERROR;
                }
            } else {
                messageColor = "danger";
                messageString = "Please provide correct current password !";
                return ERROR;
            }
        }

        return SUCCESS;
    }

    /**
     * @return the profileInfoList
     */
    public List<UserInfo> getProfileInfoList() {
        return profileInfoList;
    }

    /**
     * @param profileInfoList the profileInfoList to set
     */
    public void setProfileInfoList(List<UserInfo> profileInfoList) {
        this.profileInfoList = profileInfoList;
    }

    /**
     * @return the loginMessage
     */
    public String getLoginMessage() {
        return loginMessage;
    }

    /**
     * @param loginMessage the loginMessage to set
     */
    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the otpStatus
     */
    public String getOtpStatus() {
        return otpStatus;
    }

    /**
     * @param otpStatus the otpStatus to set
     */
    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    /**
     * @return the pinEnabledStatus
     */
    public String getPinEnabledStatus() {
        return pinEnabledStatus;
    }

    /**
     * @param pinEnabledStatus the pinEnabledStatus to set
     */
    public void setPinEnabledStatus(String pinEnabledStatus) {
        this.pinEnabledStatus = pinEnabledStatus;
    }

    /**
     * @return the apiStatus
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * @param apiStatus the apiStatus to set
     */
    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    /**
     * @return the resellerId
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * @param resellerId the resellerId to set
     */
    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the currentPin
     */
    public String getCurrentPin() {
        return currentPin;
    }

    /**
     * @param currentPin the currentPin to set
     */
    public void setCurrentPin(String currentPin) {
        this.currentPin = currentPin;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the balType
     */
    public String getBalType() {
        return balType;
    }

    /**
     * @param balType the balType to set
     */
    public void setBalType(String balType) {
        this.balType = balType;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the balanceRequestReceiveInfoList
     */
    public List<BalanceRequestHistory> getBalanceRequestReceiveInfoList() {
        return balanceRequestReceiveInfoList;
    }

    /**
     * @param balanceRequestReceiveInfoList the balanceRequestReceiveInfoList to
     * set
     */
    public void setBalanceRequestReceiveInfoList(List<BalanceRequestHistory> balanceRequestReceiveInfoList) {
        this.balanceRequestReceiveInfoList = balanceRequestReceiveInfoList;
    }

    /**
     * @return the singleBalanceRequestReceiveInfoList
     */
    public List<BalanceRequestHistory> getSingleBalanceRequestReceiveInfoList() {
        return singleBalanceRequestReceiveInfoList;
    }

    /**
     * @param singleBalanceRequestReceiveInfoList the
     * singleBalanceRequestReceiveInfoList to set
     */
    public void setSingleBalanceRequestReceiveInfoList(List<BalanceRequestHistory> singleBalanceRequestReceiveInfoList) {
        this.singleBalanceRequestReceiveInfoList = singleBalanceRequestReceiveInfoList;
    }

    /**
     * @return the balanceRequestId
     */
    public Integer getBalanceRequestId() {
        return balanceRequestId;
    }

    /**
     * @param balanceRequestId the balanceRequestId to set
     */
    public void setBalanceRequestId(Integer balanceRequestId) {
        this.balanceRequestId = balanceRequestId;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the curPassword
     */
    public String getCurPassword() {
        return curPassword;
    }

    /**
     * @param curPassword the curPassword to set
     */
    public void setCurPassword(String curPassword) {
        this.curPassword = curPassword;
    }

    /**
     * @return the pinConfirmStatus
     */
    public String getPinConfirmStatus() {
        return pinConfirmStatus;
    }

    /**
     * @param pinConfirmStatus the pinConfirmStatus to set
     */
    public void setPinConfirmStatus(String pinConfirmStatus) {
        this.pinConfirmStatus = pinConfirmStatus;
    }

    /**
     * @return the transactionPin
     */
    public String getTransactionPin() {
        return transactionPin;
    }

    /**
     * @param transactionPin the transactionPin to set
     */
    public void setTransactionPin(String transactionPin) {
        this.transactionPin = transactionPin;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }
}
