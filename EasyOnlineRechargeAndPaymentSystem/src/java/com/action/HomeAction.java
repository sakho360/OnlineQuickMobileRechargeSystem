package com.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.MobileMoneyDebit;
import com.persistance.MobileRechargeHistory;
import com.persistance.MobileMoneyHistory;
import com.persistance.MobileRechargeDebit;
import java.util.List;
import java.util.Map;

public class HomeAction extends ActionSupport {

    private List<MobileRechargeHistory> lastMRechargeHInfoList = null;
    private List<MobileMoneyHistory> lastMMoneyHInfoList = null;
    //    
    private AdminAction adminActionInfo = null;
    //
    private MobileRechargeAction mobileRechargeActionInfo = null;
    private MobileMoneyAction mobileMoneyActionInfo = null;
    private List<MobileRechargeDebit> currentMrBalInfoList = null;
    private List<MobileMoneyDebit> currentMmBalInfoList = null;
    private ResellerAction resellerAction = null;
    //    
    private double totalResellerMrBal;
    private double totalResellerMmBal;
    private String pinStatus;

    //
    public String goHomePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        mobileRechargeActionInfo = new MobileRechargeAction();
        mobileMoneyActionInfo = new MobileMoneyAction();
        adminActionInfo = new AdminAction();
        resellerAction = new ResellerAction();

        setLastMRechargeHInfoList(mobileRechargeActionInfo.lastMRechargeHInfo());
        setCurrentMrBalInfoList(mobileRechargeActionInfo.currentMRBalance(userID));
        setLastMMoneyHInfoList(mobileMoneyActionInfo.lastMMoneyHInfo());
        setCurrentMmBalInfoList(mobileMoneyActionInfo.currentMMBalance(userID));
        setTotalResellerMrBal(adminActionInfo.totalResellerMrBalByParent(userID));
        setTotalResellerMmBal(adminActionInfo.totalResellerMmBalByParent(userID));
        setPinStatus(resellerAction.checkPinStatus(userID));

        return SUCCESS;
    }

    /**
     * @return the lastMRechargeHInfoList
     */
    public List<MobileRechargeHistory> getLastMRechargeHInfoList() {
        return lastMRechargeHInfoList;
    }

    /**
     * @param lastMRechargeHInfoList the lastMRechargeHInfoList to set
     */
    public void setLastMRechargeHInfoList(List<MobileRechargeHistory> lastMRechargeHInfoList) {
        this.lastMRechargeHInfoList = lastMRechargeHInfoList;
    }

    /**
     * @return the lastMMoneyHInfoList
     */
    public List<MobileMoneyHistory> getLastMMoneyHInfoList() {
        return lastMMoneyHInfoList;
    }

    /**
     * @param lastMMoneyHInfoList the lastMMoneyHInfoList to set
     */
    public void setLastMMoneyHInfoList(List<MobileMoneyHistory> lastMMoneyHInfoList) {
        this.lastMMoneyHInfoList = lastMMoneyHInfoList;
    }

    /**
     * @return the currentMrBalInfoList
     */
    public List<MobileRechargeDebit> getCurrentMrBalInfoList() {
        return currentMrBalInfoList;
    }

    /**
     * @param currentMrBalInfoList the currentMrBalInfoList to set
     */
    public void setCurrentMrBalInfoList(List<MobileRechargeDebit> currentMrBalInfoList) {
        this.currentMrBalInfoList = currentMrBalInfoList;
    }

    /**
     * @return the currentMmBalInfoList
     */
    public List<MobileMoneyDebit> getCurrentMmBalInfoList() {
        return currentMmBalInfoList;
    }

    /**
     * @param currentMmBalInfoList the currentMmBalInfoList to set
     */
    public void setCurrentMmBalInfoList(List<MobileMoneyDebit> currentMmBalInfoList) {
        this.currentMmBalInfoList = currentMmBalInfoList;
    }

    /**
     * @return the totalResellerMrBal
     */
    public double getTotalResellerMrBal() {
        return totalResellerMrBal;
    }

    /**
     * @param totalResellerMrBal the totalResellerMrBal to set
     */
    public void setTotalResellerMrBal(double totalResellerMrBal) {
        this.totalResellerMrBal = totalResellerMrBal;
    }

    /**
     * @return the totalResellerMmBal
     */
    public double getTotalResellerMmBal() {
        return totalResellerMmBal;
    }

    /**
     * @param totalResellerMmBal the totalResellerMmBal to set
     */
    public void setTotalResellerMmBal(double totalResellerMmBal) {
        this.totalResellerMmBal = totalResellerMmBal;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }
}
