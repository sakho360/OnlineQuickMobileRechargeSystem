package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.ResellerDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ManageRate;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ResellerAction extends ActionSupport {

    private String userLevelList;
    private String mobileRecharge;
    private String mobileMoney;
    private String resellerId;
    private String password;
    private String fullName;
    private String email;
    private String phone;
    private String rateList;
    private String otpStatus;
    private String pinEnabledStatus;
    private String apiStatus;
    private String sendEmailStatus;
    private String pinConfirmStatus;
    private String currentUPin;
    private String pinStatus;
    //
    private String messageString;
    private String messageColor;
    private String fundButton;
    private String suspendButton;
    private String deleteButton;
    private String messageEditResellerButtonClicked;
    private String messageViewResellerButtonClicked;
    //
    private CommonList commonList = null;
    private Connection connection = null;
    //
    private ResellerDao resellerDao = null;
    //    
    private List<UserGroup> userGroupInfoList = null;
    private List<ManageRate> allManageRateInfoList = null;
    //    
    private List<UserInfo> resallersSubAdminInfoList = null;
    private List<UserInfo> resallersFourInfoList = null;
    private List<UserInfo> resallersThreeInfoList = null;
    private List<UserInfo> resallersTwoInfoList = null;
    private List<UserInfo> resallersOneInfoList = null;
    private List<UserInfo> allResallersInfoList = null;
    private List<UserInfo> allResallersByAdminInfoList = null;
    //    
    private List<UserInfo> singleResallerInfoList = null;

    //
    public List<UserGroup> userGroupInfo() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setUserGroupInfoList(commonList.getAllUserGroupInfo(connection));
        }

        return userGroupInfoList;
    }

    public List<ManageRate> getManageRateInfo(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllManageRateInfoList(commonList.getAllManageRateInfo(connection, uId));
        }

        return allManageRateInfoList;
    }

    public List<UserInfo> getSingleReseller(String uId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleResallerInfoList(commonList.getSingleResellerInfo(connection, uId, resellerId));
        }

        return singleResallerInfoList;
    }

    /**
     * *************************************************************************
     ***************Reseller Add,View,Update,Delete Methods
     *
     * @return
     */
    public int checkDuplicateUserId() {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT COUNT(user_id) "
                        + " TOTAL "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " user_id = '" + resellerId + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    public int checkDuplicateEmail() {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = "";

                if ((email != null) && (!email.isEmpty())) {

                    selectQuery = " SELECT COUNT(email_address) TOTAL "
                            + " FROM "
                            + " user_info "
                            + " WHERE "
                            + " email_address = '" + email + "' ";
                } else {

                    selectQuery = " SELECT COUNT(email_address)*0 TOTAL "
                            + " FROM "
                            + " user_info ";
                }

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return count;
    }

    public String checkPinStatus(String uId) {

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT enable_pin_status "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " user_id = '" + uId + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    pinStatus = rs.getString("enable_pin_status");
                }
            } catch (SQLException ex) {
                System.out.println(ex);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex);
                }
            }
        }

        return pinStatus;
    }

    public String addNewReseller() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (resellerDao == null) {
            resellerDao = new ResellerDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, currentUPin);

            if (checkDuplicateUserId() == 0) {

                if (checkDuplicateEmail() == 0) {

                    if (cPin.size() != 0) {

                        int checked = resellerDao.addNewResellerInfo(connection, resellerId, userID, fullName, password, phone, email, userLevelList, mobileRecharge, mobileMoney, rateList, otpStatus, pinEnabledStatus, apiStatus, sendEmailStatus, pinConfirmStatus, userID);

                        if (checked > 0) {
                            messageString = "1";
                            return SUCCESS;
                        } else {
                            messageColor = "danger";
                            messageString = "User Created failed.";
                            return ERROR;
                        }
                    } else {
                        messageColor = "danger";
                        messageString = "Invalid Pin !";
                        return ERROR;
                    }
                } else {
                    messageColor = "danger";
                    messageString = "Email Address Already exist, Please use different Email address !";
                    return ERROR;
                }
            } else {
                messageColor = "danger";
                messageString = "User ID Already exist, Please use different User ID !";
                return ERROR;
            }
        }

        return ERROR;
    }

    public String confirmAddNewReseller() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        getManageRateInfo(userID);

        getSingleReseller(userID);

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<UserInfo> checkParent = commonList.getParentId(connection, userID, resellerId);

            List<UserInfo> checkAdmin = commonList.getAdmin(connection, userID);

            if (checkParent.size() != 0) {
                fundButton = "fund";
                suspendButton = "buttonSpan";
            } else {
                fundButton = "fundHide";
                suspendButton = "buttonSpanHide";
            }

            if (checkAdmin.size() != 0) {
                suspendButton = "buttonSpan";
                deleteButton = "del";
            } else {
                deleteButton = "delHide";
            }

            messageColor = "mesShow";
            messageString = "User Created Successfully.";
        }

        messageViewResellerButtonClicked = "active";

        return SUCCESS;
    }

    public String viewResellerButtonClicked() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getManageRateInfo(userID);

        getSingleReseller(userID);

        checkPinStatus(userID);

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<UserInfo> checkParent = commonList.getParentId(connection, userID, resellerId);

            List<UserInfo> checkAdmin = commonList.getAdmin(connection, userID);

            if (checkParent.size() != 0) {
                fundButton = "fund";
                suspendButton = "buttonSpan";
            } else {
                fundButton = "fundHide";
                suspendButton = "buttonSpanHide";
            }

            if (checkAdmin.size() != 0) {
                suspendButton = "buttonSpan";
                deleteButton = "del";
            } else {
                deleteButton = "delHide";
            }
        }

        messageViewResellerButtonClicked = "active";

        return SUCCESS;
    }

    public String editResellerButtonClicked() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getManageRateInfo(userID);

        getSingleReseller(userID);

        checkPinStatus(userID);

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<UserInfo> checkParent = commonList.getParentId(connection, userID, resellerId);

            List<UserInfo> checkAdmin = commonList.getAdmin(connection, userID);

            if (checkParent.size() != 0) {
                fundButton = "fund";
                suspendButton = "buttonSpan";
            } else {
                fundButton = "fundHide";
                suspendButton = "buttonSpanHide";
            }

            if (checkAdmin.size() != 0) {
                suspendButton = "buttonSpan";
                deleteButton = "del";
            } else {
                deleteButton = "delHide";
            }
        }

        messageEditResellerButtonClicked = "active";

        return SUCCESS;
    }

    public String updateReseller() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (resellerDao == null) {
            resellerDao = new ResellerDao();
        }

        if (connection != null) {

            List<UserInfo> cPin = commonList.getCurrentPin(connection, userID, currentUPin);

            if (checkDuplicateEmail() == 0) {

                if (cPin.size() != 0) {

                    int checked = resellerDao.updateResellerInfo(connection, fullName, phone, email, mobileRecharge, mobileMoney, rateList, otpStatus, pinEnabledStatus, apiStatus, sendEmailStatus, pinConfirmStatus, userID, resellerId);

                    if (checked > 0) {
                        messageColor = "success";
                        messageString = "Reseller Updated Successfully.";
                    } else {
                        messageColor = "danger";
                        messageString = "Reseller Update Failed !";
                    }
                } else {
                    messageColor = "danger";
                    messageString = "Invalid Pin!";
                }
            } else {
                messageColor = "danger";
                messageString = "Email Address Already exist, Please use different Email address !";
            }
        }

        return SUCCESS;
    }

    public String resellerSuspend() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (resellerDao == null) {
            resellerDao = new ResellerDao();
        }

        if (connection != null) {

            int checked = resellerDao.updateSuspendStatus(connection, resellerId);

            if (checked > 0) {
                messageColor = "danger";
                messageString = "Reseller -" + resellerId + " has been suspended";
            } else {
                messageColor = "success";
                messageString = "Reseller -" + resellerId + " suspend failed !";
            }
        }

        return SUCCESS;
    }

    public String changeButton() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        getSingleReseller(userID);

        return SUCCESS;
    }

    public String statusUpdate() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        getSingleReseller(userID);

        return SUCCESS;
    }

    public String resellerUnSuspend() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");
        String groupName = (String) session.get("groupName");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (resellerDao == null) {
            resellerDao = new ResellerDao();
        }

        if (connection != null) {

            int checked = resellerDao.updateUnSuspendStatus(connection, resellerId);

            if (checked > 0) {
                messageColor = "success";
                messageString = "Reseller -" + resellerId + " has been activated";
            } else {
                messageColor = "danger";
                messageString = "Reseller -" + resellerId + " activation failed !";
            }
        }

        return SUCCESS;
    }

    public String deleteReseller() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (resellerDao == null) {
            resellerDao = new ResellerDao();
        }

        if (connection != null) {

            List<UserInfo> checkAdmin = commonList.getAdmin(connection, userID);

            if (checkAdmin.size() != 0) {

                int checked = resellerDao.deleteResellerInfo(connection, resellerId);

                if (checked > 0) {
                    messageColor = "success";
                    messageString = "Reseller Deleted Successfully.";
                } else {
                    messageColor = "danger";
                    messageString = "Reseller Delete Failed !";
                }
            } else {
                messageColor = "danger";
                messageString = "Need permission of Admin.";
            }
        }

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * *********************Add Reseller Page Methods
     *
     * @return
     */
    public String goAddResellerSubAdminPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        userGroupInfo();

        getManageRateInfo(userID);

        checkPinStatus(userID);

        return SUCCESS;
    }

    public String goAddResellerFourPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        userGroupInfo();

        getManageRateInfo(userID);

        checkPinStatus(userID);

        return SUCCESS;
    }

    public String goAddResellerThreePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        userGroupInfo();

        getManageRateInfo(userID);

        checkPinStatus(userID);

        return SUCCESS;
    }

    public String goAddResellerTwoPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        userGroupInfo();

        getManageRateInfo(userID);

        checkPinStatus(userID);

        return SUCCESS;
    }

    public String goAddResellerOnePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        userGroupInfo();

        getManageRateInfo(userID);

        checkPinStatus(userID);

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ********************Get Reseller Methods
     *
     * @return
     */
    public List<UserInfo> getResellersSubadmin(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResallersSubAdminInfoList(commonList.getResellerSubAdminInfo(connection, pId));
        }

        return resallersSubAdminInfoList;
    }

    public List<UserInfo> getResellersFour(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResallersFourInfoList(commonList.getResellerFourInfo(connection, pId));
        }

        return resallersFourInfoList;
    }

    public List<UserInfo> getResellersThree(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResallersThreeInfoList(commonList.getResellerThreeInfo(connection, pId));
        }

        return resallersThreeInfoList;
    }

    public List<UserInfo> getResellersTwo(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResallersTwoInfoList(commonList.getResellerTwoInfo(connection, pId));
        }

        return resallersTwoInfoList;
    }

    public List<UserInfo> getResellersOne(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setResallersOneInfoList(commonList.getResellerOneInfo(connection, pId));
        }

        return resallersOneInfoList;
    }

    public List<UserInfo> getAllResellers(String pId) {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllResallersInfoList(commonList.getAllResellerInfo(connection, pId));
        }

        return allResallersInfoList;
    }

    public List<UserInfo> getAllResellersByAdmin() {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAllResallersByAdminInfoList(commonList.getAllResellerInfoByAdmin(connection));
        }

        return allResallersByAdminInfoList;
    }

    /**
     * *************************************************************************
     * ********************Show Reseller Page Methods
     *
     * @return
     */
    public String goShowResellersSubadminPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getResellersSubadmin(userID);

        return SUCCESS;
    }

    public String goShowResellersFourPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getResellersFour(userID);

        return SUCCESS;
    }

    public String goShowResellersThreePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getResellersThree(userID);

        return SUCCESS;
    }

    public String goShowResellersTwoPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getResellersTwo(userID);

        return SUCCESS;
    }

    public String goShowResellersOnePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getResellersOne(userID);

        return SUCCESS;
    }

    public String goShowAllResellersPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllResellers(userID);

        return SUCCESS;
    }

    public String goAdminShowAllResellers() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("userId");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        getAllResellersByAdmin();

        return SUCCESS;
    }

    /**
     * *************************************************************************
     * ********************Getter and Setter Methods
     *
     * @return
     */
    /**
     * @return the userLevelList
     */
    public String getUserLevelList() {
        return userLevelList;
    }

    /**
     * @param userLevelList the userLevelList to set
     */
    public void setUserLevelList(String userLevelList) {
        this.userLevelList = userLevelList;
    }

    /**
     * @return the mobileRecharge
     */
    public String getMobileRecharge() {
        return mobileRecharge;
    }

    /**
     * @param mobileRecharge the mobileRecharge to set
     */
    public void setMobileRecharge(String mobileRecharge) {
        this.mobileRecharge = mobileRecharge;
    }

    /**
     * @return the mobileMoney
     */
    public String getMobileMoney() {
        return mobileMoney;
    }

    /**
     * @param mobileMoney the mobileMoney to set
     */
    public void setMobileMoney(String mobileMoney) {
        this.mobileMoney = mobileMoney;
    }

    /**
     * @return the resellerId
     */
    public String getResellerId() {
        return resellerId;
    }

    /**
     * @param resellerId the resellerId to set
     */
    public void setResellerId(String resellerId) {
        this.resellerId = resellerId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the rateList
     */
    public String getRateList() {
        return rateList;
    }

    /**
     * @param rateList the rateList to set
     */
    public void setRateList(String rateList) {
        this.rateList = rateList;
    }

    /**
     * @return the otpStatus
     */
    public String getOtpStatus() {
        return otpStatus;
    }

    /**
     * @param otpStatus the otpStatus to set
     */
    public void setOtpStatus(String otpStatus) {
        this.otpStatus = otpStatus;
    }

    /**
     * @return the pinEnabledStatus
     */
    public String getPinEnabledStatus() {
        return pinEnabledStatus;
    }

    /**
     * @param pinEnabledStatus the pinEnabledStatus to set
     */
    public void setPinEnabledStatus(String pinEnabledStatus) {
        this.pinEnabledStatus = pinEnabledStatus;
    }

    /**
     * @return the apiStatus
     */
    public String getApiStatus() {
        return apiStatus;
    }

    /**
     * @param apiStatus the apiStatus to set
     */
    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    /**
     * @return the sendEmailStatus
     */
    public String getSendEmailStatus() {
        return sendEmailStatus;
    }

    /**
     * @param sendEmailStatus the sendEmailStatus to set
     */
    public void setSendEmailStatus(String sendEmailStatus) {
        this.sendEmailStatus = sendEmailStatus;
    }

    /**
     * @return the pinConfirmStatus
     */
    public String getPinConfirmStatus() {
        return pinConfirmStatus;
    }

    /**
     * @param pinConfirmStatus the pinConfirmStatus to set
     */
    public void setPinConfirmStatus(String pinConfirmStatus) {
        this.pinConfirmStatus = pinConfirmStatus;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the userGroupInfoList
     */
    public List<UserGroup> getUserGroupInfoList() {
        return userGroupInfoList;
    }

    /**
     * @param userGroupInfoList the userGroupInfoList to set
     */
    public void setUserGroupInfoList(List<UserGroup> userGroupInfoList) {
        this.userGroupInfoList = userGroupInfoList;
    }

    /**
     * @return the resallersSubAdminInfoList
     */
    public List<UserInfo> getResallersSubAdminInfoList() {
        return resallersSubAdminInfoList;
    }

    /**
     * @param resallersSubAdminInfoList the resallersSubAdminInfoList to set
     */
    public void setResallersSubAdminInfoList(List<UserInfo> resallersSubAdminInfoList) {
        this.resallersSubAdminInfoList = resallersSubAdminInfoList;
    }

    /**
     * @return the resallersFourInfoList
     */
    public List<UserInfo> getResallersFourInfoList() {
        return resallersFourInfoList;
    }

    /**
     * @param resallersFourInfoList the resallersFourInfoList to set
     */
    public void setResallersFourInfoList(List<UserInfo> resallersFourInfoList) {
        this.resallersFourInfoList = resallersFourInfoList;
    }

    /**
     * @return the resallersThreeInfoList
     */
    public List<UserInfo> getResallersThreeInfoList() {
        return resallersThreeInfoList;
    }

    /**
     * @param resallersThreeInfoList the resallersThreeInfoList to set
     */
    public void setResallersThreeInfoList(List<UserInfo> resallersThreeInfoList) {
        this.resallersThreeInfoList = resallersThreeInfoList;
    }

    /**
     * @return the resallersTwoInfoList
     */
    public List<UserInfo> getResallersTwoInfoList() {
        return resallersTwoInfoList;
    }

    /**
     * @param resallersTwoInfoList the resallersTwoInfoList to set
     */
    public void setResallersTwoInfoList(List<UserInfo> resallersTwoInfoList) {
        this.resallersTwoInfoList = resallersTwoInfoList;
    }

    /**
     * @return the resallersOneInfoList
     */
    public List<UserInfo> getResallersOneInfoList() {
        return resallersOneInfoList;
    }

    /**
     * @param resallersOneInfoList the resallersOneInfoList to set
     */
    public void setResallersOneInfoList(List<UserInfo> resallersOneInfoList) {
        this.resallersOneInfoList = resallersOneInfoList;
    }

    /**
     * @return the allResallersInfoList
     */
    public List<UserInfo> getAllResallersInfoList() {
        return allResallersInfoList;
    }

    /**
     * @param allResallersInfoList the allResallersInfoList to set
     */
    public void setAllResallersInfoList(List<UserInfo> allResallersInfoList) {
        this.allResallersInfoList = allResallersInfoList;
    }

    /**
     * @return the singleResallerInfoList
     */
    public List<UserInfo> getSingleResallerInfoList() {
        return singleResallerInfoList;
    }

    /**
     * @param singleResallerInfoList the singleResallerInfoList to set
     */
    public void setSingleResallerInfoList(List<UserInfo> singleResallerInfoList) {
        this.singleResallerInfoList = singleResallerInfoList;
    }

    /**
     * @return the messageEditResellerButtonClicked
     */
    public String getMessageEditResellerButtonClicked() {
        return messageEditResellerButtonClicked;
    }

    /**
     * @param messageEditResellerButtonClicked the
     * messageEditResellerButtonClicked to set
     */
    public void setMessageEditResellerButtonClicked(String messageEditResellerButtonClicked) {
        this.messageEditResellerButtonClicked = messageEditResellerButtonClicked;
    }

    /**
     * @return the messageViewResellerButtonClicked
     */
    public String getMessageViewResellerButtonClicked() {
        return messageViewResellerButtonClicked;
    }

    /**
     * @param messageViewResellerButtonClicked the
     * messageViewResellerButtonClicked to set
     */
    public void setMessageViewResellerButtonClicked(String messageViewResellerButtonClicked) {
        this.messageViewResellerButtonClicked = messageViewResellerButtonClicked;
    }

    /**
     * @return the allManageRateInfoList
     */
    public List<ManageRate> getAllManageRateInfoList() {
        return allManageRateInfoList;
    }

    /**
     * @param allManageRateInfoList the allManageRateInfoList to set
     */
    public void setAllManageRateInfoList(List<ManageRate> allManageRateInfoList) {
        this.allManageRateInfoList = allManageRateInfoList;
    }

    /**
     * @return the fundButton
     */
    public String getFundButton() {
        return fundButton;
    }

    /**
     * @param fundButton the fundButton to set
     */
    public void setFundButton(String fundButton) {
        this.fundButton = fundButton;
    }

    /**
     * @return the suspendButton
     */
    public String getSuspendButton() {
        return suspendButton;
    }

    /**
     * @param suspendButton the suspendButton to set
     */
    public void setSuspendButton(String suspendButton) {
        this.suspendButton = suspendButton;
    }

    /**
     * @return the deleteButton
     */
    public String getDeleteButton() {
        return deleteButton;
    }

    /**
     * @param deleteButton the deleteButton to set
     */
    public void setDeleteButton(String deleteButton) {
        this.deleteButton = deleteButton;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the allResallersByAdminInfoList
     */
    public List<UserInfo> getAllResallersByAdminInfoList() {
        return allResallersByAdminInfoList;
    }

    /**
     * @param allResallersByAdminInfoList the allResallersByAdminInfoList to set
     */
    public void setAllResallersByAdminInfoList(List<UserInfo> allResallersByAdminInfoList) {
        this.allResallersByAdminInfoList = allResallersByAdminInfoList;
    }

    /**
     * @return the currentUPin
     */
    public String getCurrentUPin() {
        return currentUPin;
    }

    /**
     * @param currentUPin the currentUPin to set
     */
    public void setCurrentUPin(String currentUPin) {
        this.currentUPin = currentUPin;
    }

    /**
     * @return the pinStatus
     */
    public String getPinStatus() {
        return pinStatus;
    }

    /**
     * @param pinStatus the pinStatus to set
     */
    public void setPinStatus(String pinStatus) {
        this.pinStatus = pinStatus;
    }
}
